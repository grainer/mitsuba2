import os
import numpy as np
import enoki as ek
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter

import mitsuba
# Set the desired mitsuba variant (MUST BE THE SAME IN ALL PYTHON FILES !!!)
import nets

mitsuba.set_variant('gpu_rgb')
from mitsuba.core import Float, UInt32, Vector2f, Vector3f, Thread, Frame3f, xml, ScalarTransform4f, Bitmap, Struct
from mitsuba.core.xml import load_file, load_dict
from mitsuba.render import (BSDF, BSDFContext, BSDFFlags,
                            BSDFSample3f, SurfaceInteraction3f, register_bsdf,
                            DirectionSample3f, Emitter, ImageBlock,
                            SamplingIntegrator, has_flag,
                            register_integrator, reflect)
import renderhelpers as sgr
import matplotlib.pyplot as plt

# Input paths and parameters ===========================================================================================
scenepath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/03-03-glossybunny-staticlight' #'C:/Users/grainer/Documents/GitHub/mitsuba2/learning'

outputpath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/results/learning-bunny/bunnyscene-2'
savename_render = 'savedscene.exr'#

scenepath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/bunny-plane'
    # 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/03-03-glossybunny-staticlight-alienware' #'C:/Users/grainer/Documents/GitHub/mitsuba2/learning'
outputpath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/results/learning-bunny/bunnyscene-alienware'
scenename_train = 'glossy-sg.xml'
scenename_gt = 'glossy-gt.xml'

# Ennis map:
lightSG = [[19.77183, 18.899605, 28.09268, 124.06032, 0.13278571, 0.39354134, 0.90966654], [13.951765, 15.265006, 23.527641, 120.74434, -0.15289421, 0.5411309, 0.8269225], [0.4071942, 0.22668266, 0.10997803, 0.89142746, -0.0812242, -0.6204905, 0.7799963], [64.97454, 42.578075, 16.2094, 154.95787, -0.20876692, 0.07599607, 0.97500813], [5.1314754, 4.024345, 3.1723998, 91.700905, -0.88628745, -0.11917673, -0.44753936], [26.087568, 35.68813, 47.75076, 40.86883, -0.18703045, 0.25262922, 0.94931453], [11.181716, 15.722419, 25.156954, 210.88081, 0.10982957, 0.5572564, 0.82304484], [1.8714151, 1.836281, 1.6746786, 35.524567, 0.34814197, -0.37467614, 0.8593107], [51.333977, 24.03316, 0.3976881, 90.55722, 0.124854945, 0.100686945, 0.98705286], [4.564536, 3.5192788, 2.8387885, 131.00095, 0.9625086, -0.11077466, -0.24760067], [0.1044002, 24.66059, 42.970016, 91.73856, 0.13887106, 0.15844153, 0.97755367], [103.84179, 74.74271, 46.163086, 1047.5914, -0.42761734, 0.07404973, 0.90092176]]


# Other parameters (hardcoded)
spp = 8#  16
hybridanis = False#
anisowarp = True# False #
sharpness_thresh = 5 #for the anisotropic NDF warp
cos_sharpness = 2.13
cos_amplitude = 1.17

if not os.path.exists(outputpath):
    os.makedirs(outputpath)
# Load the scene XMLs
Thread.thread().file_resolver().append(scenepath)
scene_train = load_file(scenepath+'/'+scenename_train, spp=spp)
scene_gt = load_file(scenepath+'/'+scenename_gt, spp=spp)


    # scene_train.integrator().render(scene_train, scene_train.sensors()[0])
    # groundtruth = torch.from_numpy(np.array(scene_train.sensors()[0].film().bitmap(raw=True).convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False))).cuda() # this conversion to torch doesn't work
    # groundtruthshape = groundtruth.shape
    # print(groundtruthshape)
    # film.bitmap(raw=True).convert(Bitmap.PixelFormat.RGB, Struct.Type.UInt8, srgb_gamma=True)

# Custom rendering pipeline in Python (from Mitsuba2 doc)
film = scene_train.sensors()[0].film()
sampler = scene_train.sensors()[0].sampler()
film_size = film.crop_size()
print('film crop size ', film_size)
total_sample_count = ek.hprod(film_size)
if sampler.wavefront_size() != total_sample_count:
    sampler.seed(0, total_sample_count) #deactivate all the random samples ???

# Enumerate discrete sample & pixel indices, and uniformly sample positions within each pixel.
pos = ek.arange(UInt32, total_sample_count)
scale = Vector2f(1.0 / film_size[0], 1.0 / film_size[1])
pos = Vector2f(Float(pos % int(film_size[0])), Float(pos // int(film_size[0])))

# Sample rays starting from the camera sensor
rays, weights = scene_train.sensors()[0].sample_ray_differential(time=0, sample1=sampler.next_1d(), sample2=pos*scale, sample3=0)

# Intersect rays with the scene geometry
si = scene_train.ray_intersect(rays)
active_b = si.is_valid() & (Frame3f.cos_theta(si.wi) > 0.0)
ctx = BSDFContext()
bsdf = si.bsdf(rays)

# world positions of intersections
mask = active_b.torch().bool()
pos3d = si.p.torch()[mask,:].unsqueeze_(1).view(-1,3)

sglights = torch.tensor(lightSG).unsqueeze(0).expand(pos3d.shape[0], -1, -1).cuda()


roughness = BSDF.get_alpha_vec(bsdf, si).torch()
mask = mask * ~((roughness != roughness).any())

# # # BSDF without NDF term, only terms out of the integral. Evaluate in reflected direction
bsdf_val = BSDF.eval_vec(bsdf, ctx, si, reflect(si.wi), active_b).torch()

m2 = BSDF.get_alpha_vec(bsdf, si)**2
ndf_amp = ek.select(active_b, Vector3f(1.0) / (m2 * np.pi), Vector3f(0)).torch()[mask,:]
ndf_sharp = ek.select(active_b, Float(2.0) / m2, Float(0)).torch()[mask].unsqueeze_(1).expand(-1,3)
normals = si.to_world(Vector3f(0,0,1)).torch()[mask,:]  # si.n.torch()[mask,:]
wi = si.to_world(si.wi).torch()[mask,:]

ndf_val = sgr.shadeSGs(sglights, normals, wi, ndf_amp, ndf_sharp, hybridanis=hybridanis, anisowarp=anisowarp, sharpness_thresh=sharpness_thresh)
pred_render = torch.nn.functional.relu(bsdf_val[mask, :] * ndf_val)

scene_gt.integrator().render(scene_gt, scene_gt.sensors()[0])
gt = torch.from_numpy(np.array(scene_gt.sensors()[0].film().bitmap(raw=True).convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)))

pred = torch.zeros(film_size[0]*film_size[1], 3)
pred[mask.cpu(),:] = pred_render.cpu()
pred = pred.view(film_size[0], film_size[1], 3)

multiplier = 2.0
pred = pred**(1.0/2.2)
gt = gt**(1.0/2.2)
pred = torch.clip(pred / gt.max(), 0.0, 1.0)
gt = torch.clip(gt / gt.max(), 0.0, 1.0)
diff = torch.abs(pred - gt)
fig, axes = plt.subplots(1, 3, figsize=(18, 6))
axes[0].imshow(multiplier* pred)
axes[0].title.set_text('Pytorch approximation')
axes[1].imshow(multiplier* gt)
axes[1].title.set_text('Mitsuba2 render')
axes[2].imshow(multiplier* diff)
axes[2].title.set_text('Abs Difference')
plt.show()