# import os, sys
# import numpy as np
# import imageio as im
# import cv2  # resize images with float support
# from scipy import ndimage  # gaussian blur
# import time
# import matplotlib.pyplot as plt
# from matplotlib.colors import LinearSegmentedColormap
# import imageio
# import cv2
# import torch
# import csv
#
# # direct_renders_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/kitchen/path-direct/'
# # total_renders_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/refpaths/kitchen/path/'
# # nn_renders_dir = 'F:/gilles/paperresults/kitchen/0499-splitnets-withbuffers--bats-3-lat-64-layer-2-width-172-lr-0.0001/'
# # f = open('./direct-indirect-error-values-kitchen.csv', 'w', newline='')
#
# # direct_renders_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/atelier/path-direct/'
# # total_renders_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/refpaths/atelier/path/'
# # nn_renders_dir = 'F:/gilles/paperresults/atelier/0499-splitnets-withbuffers--bats-3-lat-64-layer-2-width-172-lr-0.0001/'
# # f = open('./direct-indirect-error-values-atelier.csv', 'w', newline='')
#
# direct_renders_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/bedroom/path-direct/'
# total_renders_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/refpaths/bedroom/path/'
# nn_renders_dir = 'F:/gilles/paperresults/bedroom/0499-splitnets-withbuffers--bats-3-lat-64-layer-2-width-172-lr-0.0001/'
# f = open('./direct-indirect-error-values-bedroom.csv', 'w', newline='')
#
# num_ims = 600
# num_samples_perim = 16
#
# writer = csv.writer(f)
# for i in range(num_ims):
#   directim = torch.from_numpy(cv2.cvtColor(cv2.imread(direct_renders_dir + str(i).zfill(5) + '.AccumulatePass.output.0.exr', cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
#   totalim = torch.from_numpy(cv2.cvtColor(cv2.imread(total_renders_dir + str(i).zfill(5) + '.AccumulatePass.output.0.exr', cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
#   nnim = torch.from_numpy(cv2.cvtColor(cv2.imread(nn_renders_dir + str(i).zfill(8) + '.exr', cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
#   indirectim = totalim - directim
#
#   # print(totalim.shape)
#   # plt.imshow(directim)
#   # plt.show()
#   # plt.imshow(indirectim)
#   # plt.show()
#   # plt.imshow(nnim)
#   # plt.show()
#
#   # compute random subset of pixels
#   directim = directim.view(-1,3)
#   indirectim = indirectim.view(-1,3)
#   totalim = totalim.view(-1,3)
#   nnim = nnim.view(-1,3)
#   randidx = torch.randint(512*512, (num_samples_perim,1)).squeeze()
#
#   direct = torch.sum(directim[randidx, :] / totalim[randidx, :], dim=1) / 3.0
#   indirect = torch.sum(indirectim[randidx, :] / totalim[randidx, :], dim=1) / 3.0
#   nn = torch.sqrt(torch.sum((nnim[randidx, :] - totalim[randidx, :])**2, dim=1))
#
#   valid = (indirect > 0.0) # remove envmap / background from samples
#   direct = direct[valid]
#   indirect = indirect[valid]
#   nn = nn[valid]
#
#   print(direct[0].item(), indirect[0].item(), nn[0].item())
#   print("===========================================")
#
#   for r in range(direct.shape[0]):
#     writer.writerow([direct[r].item(), indirect[r].item(), nn[r].item()])
#
#
# # f = open('./diffuse-specular-error-values-atelier.csv', 'w', newline='')
# # f = open('./diffuse-specular-error-values-kitchen.csv', 'w', newline='')
# f = open('./diffuse-specular-error-values-bedroom.csv', 'w', newline='')
#
# writer = csv.writer(f)
# for i in range(num_ims):
#   diffuseim = torch.from_numpy(cv2.cvtColor(cv2.imread(direct_renders_dir + str(i).zfill(5) + '.GBufferRT.diffuseOpacity.0.exr', cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
#   specularim = torch.from_numpy(cv2.cvtColor(cv2.imread(direct_renders_dir + str(i).zfill(5) + '.GBufferRT.specRough.0.exr', cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
#   totalim = torch.from_numpy(cv2.cvtColor(cv2.imread(total_renders_dir + str(i).zfill(5) + '.AccumulatePass.output.0.exr', cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
#   nnim = torch.from_numpy(cv2.cvtColor(cv2.imread(nn_renders_dir + str(i).zfill(8) + '.exr', cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
#
#   # print(totalim.shape)
#   # plt.imshow(directim)
#   # plt.show()
#   # plt.imshow(indirectim)
#   # plt.show()
#   # plt.imshow(nnim)
#   # plt.show()
#
#   # compute random subset of pixels
#   diffuseim = diffuseim.view(-1,3)
#   specularim = specularim.view(-1,3)
#   totalim = totalim.view(-1,3)
#   nnim = nnim.view(-1,3)
#
#   valid = torch.logical_or((torch.sum(diffuseim, dim=1) > 0.0), (torch.sum(specularim, dim=1) > 0.0))  # remove envmap / background from samples
#   diffuseim = diffuseim[valid]
#   specularim = specularim[valid]
#   totalim = totalim[valid]
#   nnim = nnim[valid]
#
#   randidx = torch.randint(totalim.shape[0], (num_samples_perim, 1)).squeeze()
#
#   diffuse = torch.sum(diffuseim[randidx, :], dim=1) / 3.0
#   specular = torch.sum(specularim[randidx, :], dim=1) / 3.0
#   nn = torch.sqrt(torch.sum((nnim[randidx, :] - totalim[randidx, :])**2, dim=1))
#
#
#   print(diffuse[0].item(), specular[0].item(), nn[0].item())
#   print("===========================================")
#
#   for r in range(diffuse.shape[0]):
#     writer.writerow([diffuse[r].item(), specular[r].item(), nn[r].item()])
#



import os, sys
import numpy as np
import imageio as im
import cv2  # resize images with float support
from scipy import ndimage  # gaussian blur
import time
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import imageio
import cv2
import torch
import csv

data_dir = 'F:/gilles/paperresults/deepshading/atelier/'
reference_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/refpaths/atelier/path/'
data_dir = 'F:/gilles/paperresults/deepshading/bedroom/'
reference_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/refpaths/bedroom/path/'

for i in range(600):

  # Read the mask and the GT and replace the envmap in the reconstruction


  gtim = torch.from_numpy(cv2.cvtColor(cv2.imread(reference_dir + str(i).zfill(5) + '.AccumulatePass.output.0.exr', cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))

  maskim = torch.from_numpy(cv2.cvtColor(cv2.imread(reference_dir + str(i).zfill(5) + '.GBufferRT.normW.0.exr', cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))

  nnim = torch.from_numpy(cv2.cvtColor(cv2.imread(data_dir + str(i).zfill(8) + '.exr', cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))

  # print(totalim.shape)
  # plt.imshow(directim)
  # plt.show()
  # plt.imshow(indirectim)
  # plt.show()
  # plt.imshow(nnim)
  # plt.show()

  mask = (torch.sum(maskim**2, dim=2, keepdim=True) < 0.1).expand(-1,-1,3)

  nnim[mask] = gtim[mask]

  # plt.imshow(nnim)
  # plt.show()

  imageio.imwrite(data_dir + str(i).zfill(8) + '.exr', nnim.cpu().numpy())
  print(str(i), "===========================================")
