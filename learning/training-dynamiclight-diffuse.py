import os
import numpy as np
import enoki as ek
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader

import mitsuba
# Set the desired mitsuba variant (MUST BE THE SAME IN ALL PYTHON FILES !!!)
import nets
mitsuba.set_variant('gpu_rgb') #'gpu_autodiff_rgb')
from mitsuba.core import Float, UInt32, Vector2f, Vector3f, Thread, Frame3f, xml, ScalarTransform4f, Bitmap, Struct
from mitsuba.core.xml import load_file, load_dict
from mitsuba.render import (BSDF, BSDFContext, BSDFFlags,
                            BSDFSample3f, SurfaceInteraction3f, register_bsdf,
                            DirectionSample3f, Emitter, ImageBlock,
                            SamplingIntegrator, has_flag,
                            register_integrator, reflect)
from mitsuba.python.util import traverse
import renderhelpers as sgr
import matplotlib.pyplot as plt
torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True

# Input paths and parameters ===========================================================================================
scenepath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/dynamic-envmap-bunny'
outputpath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/results/dynamic-bunny/firsttests'
envmap_dir = 'F:/gilles/data/envmaps/IndoorHDRDataset2018-256x128'
# envmap_dir = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/render-assets' #
scenename_train = 'diffuse-sg.xml'
scenename_gt = 'diffuse-gt.xml'

# Training parameters ==================================================================================================
max_epochs = 1000# 10000

num_freqs = 0 # 0 means no positional encoding

latentdim = 32#    128# 72#  350# #128#   512#

numSGs = 12#  25#      50#

cnndim = 256# 96# 512# 2048#  4096#
netwidth = 64# 128#  400# 250#      64#
batch_size = 0
num_layers = 1# 4# 3#  4#    1#     5# 3#
learning_rate = 0.00001#   0.0001#   0.005#   0.01# 0.5#
logloss = True# False#
L2loss = False# True#
exponentiate = False#
normalize = False#  True#
rgbprior = False# True#
kldiv = False#  True
n2ntonemap = False# True#
pos_encoding = True#  False#

experiment_name = 'latelightinject-logenvmap-avgpool-'

experiment_name = experiment_name +'-batchsize-' +str(batch_size) +'-layers-' +str(num_layers) +'-width-' +str(netwidth) \
                  +'-numSG-'+str(numSGs) +'-lr-'+str(learning_rate) +'-freq-' + str(num_freqs)\
                  +'-log-'+str(int(logloss)) +'-L2-'+str(int(L2loss)) + '-latentdim-' + str(latentdim)
                  # +'-kldiv-'+str(int(kldiv)) +'-priors-'+str(int(rgbprior)) +

# Other parameters (hardcoded)
num_workers = 0
spp = 16 # 32#
hybridanis = False#
anisowarp = True#   False
sharpness_thresh = 5 #for the anisotropic NDF warp
cos_sharpness = 2.13
cos_amplitude = 1.17
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
if not os.path.exists(outputpath):
    os.makedirs(outputpath)
# Load the scene XMLs
Thread.thread().file_resolver().append(scenepath)
scene_gt = load_file(scenepath+'/'+scenename_gt, spp=spp)
scene_train = load_file(scenepath+'/'+scenename_train, spp=spp)

#Load envmaps, DO the train validation data split on the filenames
exr_files = []
for r, d, f in os.walk(envmap_dir):  # r=root, d=directories, f = files
    for file in f:
        if '.exr' in file:
            exr_files.append(os.path.join(r, file))
print(len(exr_files), ' EXR files found')
idxs = np.arange(len(exr_files))
np.random.shuffle(idxs)
cutoff = int(0.8 * len(exr_files))
exr_files = np.array(exr_files)
files_train = exr_files[idxs[0:cutoff]]
files_val = exr_files[idxs[cutoff:]]
print(len(files_train), ' training files')
print(len(files_val), ' testing files')
print('Load training data.')
train_dataset = sgr.EnvmapDataset(files_train, False)
print('Load validation data.')
val_dataset = sgr.EnvmapDataset(files_val, False)
print('Training data loaded.')
train_dataloader = DataLoader(train_dataset, batch_size=1, shuffle=True, num_workers=num_workers)#, pin_memory=True, drop_last=True)
val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=False, num_workers=num_workers)#, pin_memory=True, drop_last=True)

# Find differentiable scene parameters
params = traverse(scene_gt)
param_res = params['dynamic_envmap.resolution']
param_ref = params['dynamic_envmap.data']
params.keep(['dynamic_envmap.data'])

# Setup NN, optimizer, tensorboard etc. ===================================================================================
net = nets.RenderingNet(cnndim, latentdim, numSGs, netwidth, num_layers, num_freqs).to(device=gpudevice)
sg_centers = sgr.initialiseSGcenters(numSGs).to(device=gpudevice)
# optimizer = torch.optim.SGD(net.parameters(), lr=learning_rate)
optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
writer = SummaryWriter(outputpath +'/runs/' + experiment_name) # logloss-l1-adam-5e-3-testFaceNormals')

klloss = torch.nn.KLDivLoss()
iteration = 0
envmap_mitsuba = torch.ones(param_res[1], param_res[0], 4).to(gpudevice)

params['dynamic_envmap.data'] = Float(envmap_mitsuba.view(-1))
params.update()
# scene_gt.integrator().render(scene_gt, scene_gt.sensors()[0])
# ambient_groundtruth = torch.from_numpy(np.array(scene_gt.sensors()[0].film().bitmap(raw=True).convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False))).to(device=gpudevice) # this conversion to torch doesn't work



for epoch in range(max_epochs):
    epoch_loss = 0.0
    for envmap, envmap_m, envmap_std in train_dataloader:
        envmap_mitsuba[:,:,0:3] = (envmap.squeeze()).permute(1, 2, 0).to(gpudevice) # + envmap_m * envmap_std
        optimizer.zero_grad()
        running_loss = 0.0

        # plt.imshow(torch.clip(2.0 * envmap_mitsuba[:,:,0:3].cpu(), 0.0, 1.0)**(1.0/2.2)) #
        # plt.show()
        # print(envmap_mitsuba.min(), envmap_mitsuba.max())


        # Randomly set the camera position and render the GT scene
        randomsensor = sgr.generate_random_sensor(spp)
        if iteration % 251 == 0: #once in a while render the original view, useful for comparisons for meetings/summaries
            randomsensor = scene_gt.sensors()[0]

        params['dynamic_envmap.data'] = Float(envmap_mitsuba.view(-1))
        params.update()
        scene_gt.integrator().render(scene_gt, randomsensor)
        groundtruth = torch.from_numpy(np.array(randomsensor.film().bitmap(raw=True).convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False))).to(device=gpudevice) # this conversion to torch doesn't work
        groundtruthshape = groundtruth.shape


        # plt.imshow(torch.clip(15.0* groundtruth.cpu(), 0.0, 1.0)**(1.0/2.2))
        # plt.show()

        # Custom rendering pipeline in Python (from Mitsuba2 doc)
        film = randomsensor.film()
        sampler = randomsensor.sampler()
        film_size = film.crop_size()
        total_sample_count = ek.hprod(film_size)
        if sampler.wavefront_size() != total_sample_count:
            sampler.seed(0, total_sample_count) #deactivate all the random samples ???

        # Enumerate discrete sample & pixel indices, and uniformly sample positions within each pixel.
        pos = ek.arange(UInt32, total_sample_count)
        # pos //= spp
        scale = Vector2f(1.0 / film_size[0], 1.0 / film_size[1])
        pos = Vector2f(Float(pos % int(film_size[0])), Float(pos // int(film_size[0])))
        # pos += sampler.next_2d() # try deactivating this (no random position within the pixel but only center...)

        # Sample rays starting from the camera sensor
        rays, weights = randomsensor.sample_ray_differential(time=0, sample1=sampler.next_1d(), sample2=pos*scale, sample3=0)

        # Intersect rays with the scene geometry
        si = scene_train.ray_intersect(rays)
        active_b = si.is_valid() & (Frame3f.cos_theta(si.wi) > 0.0)
        ctx = BSDFContext()
        bsdf = si.bsdf(rays)

        # world positions of intersections
        mask = active_b.torch().bool()
        pos3d = torch.cat([si.p.torch()[mask, :], si.to_world(Vector3f(0, 0, 1)).torch()[mask, :]], 1)
        print('position ', pos3d.min(), pos3d.max())

        net_out = net( torch.log(envmap.to(gpudevice) +0.0001), pos3d)
        # net_out = net(envmap.to(gpudevice), pos3d)
        sglights = sgr.deparametrize_torch(net_out.unsqueeze(2).view(-1,numSGs,6), sg_centers, exponentiate)

        normals = si.to_world(Vector3f(0, 0, 1)).torch()[mask, :]
        bsdf_val = BSDF.eval_vec(bsdf, ctx, si, Vector3f([0.0, 0.0, 1.0]), active_b).torch()[mask, :]
        irr = torch.zeros(pos3d.shape[0], 3).to(device=gpudevice)
        for l in range(numSGs):
            irr = irr + sgr.exact_cosSGinnerProduct(normals, sglights[:, l, 0:3], sglights[:, l, 3], sglights[:, l, 4:7])
            # irr = irr + sgr.stephen_hill_irradiance(normals, sglights[:, l, 0:3], sglights[:, l, 3], sglights[:, l, 4:7])


        pred_render = torch.nn.functional.relu(bsdf_val * irr )
        # groundtruth = groundtruth / envmap_std.to(device=gpudevice) # - ambient_groundtruth * envmap_m.to(device=gpudevice)
        groundtruth = groundtruth.view(1, groundtruth.shape[0] * groundtruth.shape[1], 3)

        # DEBUG FOR NaNs ===================================================================================================
        assert (mask != mask).any() == False, "Damn! mask contains NaNs"
        assert (sglights != sglights).any() == False, "Damn! sglights contains NaNs"
        assert (pos3d != pos3d).any() == False, "Damn! pos3d contains NaNs"
        assert (irr != irr).any() == False, "Damn! ndf_val contains NaNs"
        assert (pred_render != pred_render).any() == False, "Damn! pred_render contains NaNs"
        # assert (bsdf_val[mask, :] != bsdf_val[mask, :]).any() == False, "Damn! bsdf_val contains NaNs"
        # ==================================================================================================================
        # if kldiv:
        #     std = groundtruth.squeeze()[mask, :].std()
        #     # normalize the KL div but not the L1 loss
        #
        #     gtprob = groundtruth.squeeze()[mask, :]
        #     m = gtprob.min()
        #     s = torch.sum(gtprob)
        #     gtprob = gtprob - m
        #     gtprob = gtprob /s
        #
        #     loss = klloss(torch.log(torch.nn.functional.relu( (pred_render-m)/s ) + 1.0), gtprob) + torch.abs(pred_render - groundtruth.squeeze()[mask, :]).mean()
            # loss = klloss(torch.log(torch.nn.functional.relu(pred_render/ std) + 1.0), groundtruth.squeeze()[mask, :] / std) + ((pred_render/std - groundtruth.squeeze()[mask, :]/std) ** 2).mean()
            # loss = klloss(torch.log(torch.nn.functional.relu(pred_render) + 1.0), groundtruth.squeeze()[mask, :]) + ((pred_render - groundtruth.squeeze()[mask, :]) ** 2).mean()
            #+ torch.abs(pred_render - groundtruth.squeeze()[mask, :]).mean()/std

        if logloss:
            if L2loss:
                if normalize:
                    std = torch.log(groundtruth.squeeze()[mask, :] + 1.0).std()
                    loss = ((torch.log(torch.nn.functional.relu(pred_render) + 1.0) / std - torch.log(
                        groundtruth.squeeze()[mask, :] + 1.0) / std) ** 2).mean()
                else:
                    loss = ((torch.log(torch.nn.functional.relu(pred_render) + 1.0) - torch.log(
                        groundtruth.squeeze()[mask, :] + 1.0)) ** 2).mean()
            else:
                if normalize:
                    std = torch.log(groundtruth.squeeze()[mask, :] + 1.0).std()
                    loss = torch.abs(torch.log(torch.nn.functional.relu(pred_render) + 1.0) - torch.log(
                        groundtruth.squeeze()[mask, :] + 1.0)).mean() / std
                else:
                    loss = torch.abs(torch.log(torch.nn.functional.relu(pred_render) +1.0) - torch.log(groundtruth.squeeze()[mask, :] +1.0)).mean()
        else:
            if n2ntonemap:
                if L2loss:
                    # loss = (( pred_render/(pred_render+1.0) - groundtruth.squeeze()[mask, :]/(groundtruth.squeeze()[mask, :]+1.0) ) ** 2).mean()
                    # loss = ( (pred_render - groundtruth.squeeze()[mask, :])**2 / (pred_render + 1.0) ).mean()
                    loss = (((pred_render/(pred_render+1.0))**(1.0/2.2) - (groundtruth.squeeze()[mask, :]/(groundtruth.squeeze()[mask, :]+1.0))**(1.0/2.2))**2).mean()
                    # loss = torch.abs((pred_render / (pred_render + 1.0)) ** (1.0 / 2.2) - (
                    #             groundtruth.squeeze()[mask, :] / (groundtruth.squeeze()[mask, :] + 1.0)) ** (
                    #                              1.0 / 2.2)).mean()
                else:
                    loss = torch.abs( pred_render/(pred_render+1.0) - groundtruth.squeeze()[mask, :]/(groundtruth.squeeze()[mask, :]+1.0) ).mean()
                    # loss = torch.abs( (pred_render / (pred_render + 1.0))**2 - (groundtruth.squeeze()[mask, :] / (
                    #             groundtruth.squeeze()[mask, :] + 1.0))**2 ).mean()
                    # loss = torch.abs( torch.log(1.0 + pred_render / (pred_render + 1.0)) - torch.log(1.0 + groundtruth.squeeze()[mask, :] / (
                    #             groundtruth.squeeze()[mask, :] + 1.0)) ).mean()
            else:
                if L2loss:
                    loss = ((pred_render - groundtruth.squeeze()[mask, :]) ** 2).mean()
                else:
                    loss = torch.abs(pred_render - groundtruth.squeeze()[mask, :]).mean()


        if rgbprior:
            loss = loss + 0.1 * ( (1.0 - sglights[:, :, 0:3]/(1.0+sglights[:, :, 0:3])).mean() + (sglights[:, :, 3]/(1.0+sglights[:, :, 3])).mean())
            # loss = loss + 0.1 * (torch.log(1.0 + torch.log(1.0 + sglights[:, :, 3])).mean() - torch.log(
            #     1.0 + torch.log(1.0 + sglights[:, :, 0:3])).mean())

        if kldiv:
            # normalize the KL div but not the loss
            gtprob = groundtruth.squeeze()[mask, :]
            m = gtprob.min()
            s = torch.sum(gtprob)
            gtprob = gtprob - m
            gtprob = gtprob /s
            loss = loss + klloss(torch.log(torch.nn.functional.relu( (pred_render-m)/s ) + 1.0), gtprob)

        running_loss += loss.item()
        loss.backward()
        optimizer.step()

        writer.add_scalar('training loss', running_loss, iteration)
        iteration += 1
        print('loss  ', running_loss, 'for iteration  ', iteration)

        epoch_loss += running_loss / len(train_dataloader)

        if iteration % 10 == 0:
            with torch.no_grad():

                pred = torch.zeros(groundtruthshape[0]*groundtruthshape[1], 3)
                gt = (groundtruth*mask.unsqueeze(1).expand(-1,3)).view(groundtruthshape).cpu()
                gt = gt.permute(2,0,1)

                pred[mask.cpu(),:] = pred_render.cpu()
                pred = pred.view(groundtruthshape)
                pred = pred.permute(2,0,1)
                diff = torch.abs(pred - gt)

                # print(gt.is_cuda, pred.is_cuda, diff.is_cuda)
                print(gt.shape, pred.shape, diff.shape)
                print('maxs and mins  ', gt.max(), pred.max(), diff.max(), gt.min(), pred.min(), diff.min())
                img_grid = torchvision.utils.make_grid([pred, gt, diff])
                img_grid = img_grid**(1/2.2)
                img_grid = img_grid / img_grid.max()
                img_grid = torch.clip(img_grid, 0.0, 1.0)
                writer.add_image('predictions vs. GT', img_grid, global_step=iteration)



print('Finished Training')


