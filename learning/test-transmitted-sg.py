import os
import numpy as np
import enoki as ek
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader

import mitsuba
# Set the desired mitsuba variant (MUST BE THE SAME IN ALL PYTHON FILES !!!)
import nets
mitsuba.set_variant('gpu_rgb') #'gpu_autodiff_rgb')
import enoki
from mitsuba.core import Float, UInt32, Vector2f, Vector3f, Thread, Frame3f, xml, ScalarTransform4f, Bitmap, Struct
from mitsuba.render import MicrofacetDistribution, MicrofacetType
from mitsuba.core.xml import load_file, load_dict
from mitsuba.render import (BSDF, BSDFContext, BSDFFlags,
                            BSDFSample3f, SurfaceInteraction3f, register_bsdf,
                            DirectionSample3f, Emitter, ImageBlock,
                            SamplingIntegrator, has_flag,
                            register_integrator, reflect)
from mitsuba.python.util import traverse
import renderhelpers as sgr
import matplotlib.pyplot as plt
torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True
import cv2

# ===========================================================================================
# PISA SG lights
lightSG = [[0.9433718, 0.37715107, 0.030528598, 35.069885, -0.84324276, 0.2121104, 0.4939137], [0.70375603, 0.3655845, 0.15851796, 23.585962, 0.52402025, 0.7886307, -0.32165867], [0.08567178, 0.06427135, 0.044218015, 1.1091332, -0.2356962, -0.66532904, -0.70836747], [0.6558805, 0.31723064, 0.11005787, 21.001013, -0.60743445, 0.73271066, -0.30685267], [0.86364007, 0.35598388, 0.11529839, 22.24271, 0.46149436, 0.2683993, -0.8455677], [1.1314801, 1.2558013, 1.2232834, 41.10061, 0.053204477, 0.256633, 0.9650434], [1.0217043, 0.4285945, 0.13063939, 15.990939, -0.047010783, 0.8666322, -0.49672785], [0.20844299, 0.3255447, 0.49083838, 5.3000503, -0.44954064, 0.51849866, 0.72737354], [1.158383, 1.5155687, 1.7711425, 46.52204, 0.38086146, 0.38595143, 0.84022975], [2.3821747, 2.6763604, 2.6895938, 227.89229, 0.6516599, 0.27348915, 0.7074907], [0.66085684, 0.31836376, 0.1304988, 49.41547, 0.9093498, 0.18544276, -0.3724163], [0.7925673, 1.0842797, 1.393003, 14.961501, 0.7191419, 0.51414, 0.46743447]]
numSGs = 12

# # Ennis map:
# lightSG = [[19.77183, 18.899605, 28.09268, 124.06032, 0.13278571, 0.39354134, 0.90966654], [13.951765, 15.265006, 23.527641, 120.74434, -0.15289421, 0.5411309, 0.8269225], [0.4071942, 0.22668266, 0.10997803, 0.89142746, -0.0812242, -0.6204905, 0.7799963], [64.97454, 42.578075, 16.2094, 154.95787, -0.20876692, 0.07599607, 0.97500813], [5.1314754, 4.024345, 3.1723998, 91.700905, -0.88628745, -0.11917673, -0.44753936], [26.087568, 35.68813, 47.75076, 40.86883, -0.18703045, 0.25262922, 0.94931453], [11.181716, 15.722419, 25.156954, 210.88081, 0.10982957, 0.5572564, 0.82304484], [1.8714151, 1.836281, 1.6746786, 35.524567, 0.34814197, -0.37467614, 0.8593107], [51.333977, 24.03316, 0.3976881, 90.55722, 0.124854945, 0.100686945, 0.98705286], [4.564536, 3.5192788, 2.8387885, 131.00095, 0.9625086, -0.11077466, -0.24760067], [0.1044002, 24.66059, 42.970016, 91.73856, 0.13887106, 0.15844153, 0.97755367], [103.84179, 74.74271, 46.163086, 1047.5914, -0.42761734, 0.07404973, 0.90092176]]


num_MC_samples = 500#00
shade_diffuse = False
shade_specular = True
shade_transmitted = True
ggx = True # False # True
EPSILON = 1.0e-8

# TODO : try normalizing the NDF SG ??
# TODO : GGX alpha is wrong (needs to be squared !!!!) or the SG alpha needs to be square rooted
# TODO : for the SGs we want "linear roughness



buffer_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/assets/testbunny"
# READ IN BUFFERS
alpha = torch.from_numpy(cv2.cvtColor(cv2.imread(os.path.join(buffer_dir, "GBufferRT.alpha.0.exr"), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
pos3d = torch.from_numpy(cv2.cvtColor(cv2.imread(os.path.join(buffer_dir, "GBufferRT.posW.0.exr"), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
diffuse = torch.from_numpy(cv2.cvtColor(cv2.imread(os.path.join(buffer_dir, "GBufferRT.diffuseOpacity.0.exr"), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
normals = torch.from_numpy(cv2.cvtColor(cv2.imread(os.path.join(buffer_dir, "GBufferRT.normW.0.exr"), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
specular = torch.from_numpy(cv2.cvtColor(cv2.imread(os.path.join(buffer_dir, "GBufferRT.specShading.0.exr"), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
transmitted = torch.from_numpy(cv2.cvtColor(cv2.imread(os.path.join(buffer_dir, "GBufferRT.transShading.0.exr"), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
viewdir = torch.from_numpy(cv2.cvtColor(cv2.imread(os.path.join(buffer_dir, "GBufferRT.viewW.0.exr"), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))

gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
gtshape = viewdir.shape
full_mask_trans = (torch.sum(transmitted * transmitted, dim=-1, keepdim=False) > 0.0)

# plt.imshow(normals)
# plt.show()
# plt.imshow(alpha[:,:,0])
# plt.show()

# flatten everything
mask = (torch.sqrt(torch.sum(normals**2, dim=-1, keepdim=True)).expand(-1,-1,3) > 0.0)  # to make sure, remove background
mask = mask[:,:,0].view(-1)

pos3d = pos3d.view(-1, 3)
normals = normals.view(-1, 3)
diffuse_bsdf_val = diffuse.view(-1, 3)
specular_bsdf_val = specular.view(-1, 3)
trans_bsdf_val = transmitted.view(-1, 3)

ior = alpha[:, :, 1].view(-1).unsqueeze(1).expand(-1,3)
alpha = alpha[:, :, 0].view(-1)
wi = viewdir.view(-1, 3)


# isolate pixels that need shading
# mask = (alpha > 0.0)
pos3d = pos3d[mask, :]
pred = torch.zeros(pos3d.shape).to(device=gpudevice)
normals = normals[mask, :]
ior = ior[mask, :]
diffuse_bsdf_val = diffuse_bsdf_val[mask, :]
specular_bsdf_val = specular_bsdf_val[mask, :]
trans_bsdf_val = trans_bsdf_val[mask, :]
wi = wi[mask, :]
alpha = alpha[mask]

sglights = torch.from_numpy(np.array(lightSG, dtype=np.float32)).unsqueeze(0).expand(alpha.shape[0], -1,-1).to(device=gpudevice)


# ior = ior*2
# alpha = alpha * 3.0
print("alpha: ", alpha.min(), alpha.mean(), alpha.max())



if shade_diffuse: # # diffuse shading
    mask_diffuse = (torch.sum(diffuse_bsdf_val * diffuse_bsdf_val, dim=-1, keepdim=False) > 0.0)
    irr = torch.zeros(torch.sum(mask_diffuse), 3).to(device=gpudevice)
    for l in range(sglights.shape[1]):
        irr = irr + sgr.exact_cosSGinnerProduct(normals[mask_diffuse, :].to(device=gpudevice),
                                                sglights[mask_diffuse, l, 0:3],
                                                sglights[mask_diffuse, l, 3],
                                                sglights[mask_diffuse, l, 4:7])
    pred[mask_diffuse, :] = pred[mask_diffuse, :] + irr   # * diffuse_bsdf_val[mask_diffuse, :].to(device=gpudevice)
    assert (irr != irr).any() == False, "Damn! irr contains NaNs"
#
mask_specular = (torch.sum(specular_bsdf_val * specular_bsdf_val, dim=-1, keepdim=False) > 0.0)
if ggx:
    r2 = alpha[mask_specular]
else:
    r2 = alpha[mask_specular]** 2
if shade_specular: # # # specular shading
    ndf_sharp = 2.0 / r2
    ndf_amp = 1.0 / (r2 * np.pi)
    ndf_amp = ndf_amp.unsqueeze(1).expand(-1, 3)
    ndf_sharp = ndf_sharp.unsqueeze(1).expand(-1, 3)
    ndf_val = torch.zeros(ndf_sharp.shape, device=gpudevice)
    sg_amp, sg_sharp, sg_axi = sgr.warpBRDF_SG(ndf_amp.to(gpudevice), ndf_sharp.to(gpudevice), normals[mask_specular, :].to(gpudevice),
                                                                wi[mask_specular, :].to(gpudevice))
    for l in range(sglights.size()[1]):
        ndf_val = ndf_val + sgr.SGinnerProduct(sglights[mask_specular, l, 0:3], sglights[mask_specular, l, 3].unsqueeze(1).expand(-1, 3),
                                           sglights[mask_specular, l, 4:7], sg_amp, sg_sharp, sg_axi)
    pred[mask_specular, :] = pred[mask_specular, :] + ndf_val * specular_bsdf_val[mask_specular, :].to(gpudevice)

# # # transmitted shading
mask_trans = (torch.sum(trans_bsdf_val * trans_bsdf_val, dim=-1, keepdim=False) > 0.0)
if shade_transmitted:
    if ggx:
        r2 = alpha[mask_trans]
    else:
        r2 = alpha[mask_trans] ** 2
    ndf_sharp = 2.0 / r2
    ndf_amp = 1.0 / (r2 * np.pi * (1.0 - torch.exp(-2.0 * ndf_sharp)))
    ndf_amp = ndf_amp.unsqueeze(1).expand(-1, 3)
    ndf_sharp = ndf_sharp.unsqueeze(1).expand(-1, 3)
    ndf_val = torch.zeros(ndf_sharp.shape, device=gpudevice)
    print("ndf_amp  ", ndf_amp.min(), ndf_amp.mean(), ndf_amp.max())
    print("ndf_sharp  ", ndf_sharp.min(), ndf_sharp.mean(), ndf_sharp.max())

    sg_amp, sg_sharp, sg_axi = sgr.warpBTDF_SG(ndf_amp.to(gpudevice), ndf_sharp.to(gpudevice), normals[mask_trans, :].to(gpudevice),
                                                                wi[mask_trans, :].to(gpudevice), ior[mask_trans, :].to(gpudevice))
    print("sg_amp  ", sg_amp.min(), sg_amp.mean(), sg_amp.max())
    print("sg_sharp  ", sg_sharp.min(), sg_sharp.mean(), sg_sharp.max())
    print("ior  ", ior.min(), ior.mean(), ior.max())
    for l in range(sglights.size()[1]):
        ndf_val = ndf_val + sgr.SGinnerProduct(sglights[mask_trans, l, 0:3], sglights[mask_trans, l, 3].unsqueeze(1).expand(-1, 3),
                                           sglights[mask_trans, l, 4:7], sg_amp, sg_sharp, sg_axi)

    print("ndfval  ", ndf_val.min(), ndf_val.mean(), ndf_val.max())
    assert (ndf_val != ndf_val).any() == False, "Damn! ndf_val contains NaNs"
    pred[mask_trans, :] = pred[mask_trans, :] + ndf_val * trans_bsdf_val[mask_trans, :].to(gpudevice)
print("====================")
print("====================")
print("====================")
#
# debug = torch.zeros(gtshape)
# debug[full_mask_trans, :] = -sg_axi.cpu() *0.5 + 0.5
# plt.imshow(debug)
# plt.show()


# MC diffuse
mcpred = torch.zeros(mask_specular.shape[0], 3).to(device=gpudevice)
if shade_diffuse:
    for i in range(num_MC_samples):
        lightdir = torch.from_numpy(np.float32(2 * np.random.rand(mask_diffuse.shape[0], 3) - 1.0)).to(device=gpudevice)
        lightdir = sgr.normalize(lightdir)
        widotn = torch.clamp(sgr.dot(lightdir.to(device=gpudevice), normals.to(device=gpudevice)), 0.0)
        lightSGsatrandomsample = torch.zeros( widotn[mask_diffuse,:].shape).cuda()
        for l in range(numSGs):
            lightSGsatrandomsample += sglights[:, l, 0:3] * torch.exp(sglights[:, l, 3].unsqueeze(1).expand(-1,3)
                                                                      * (sgr.dot(lightdir, sglights[:, l, 4:7]) - 1.0))
        mcpred[mask_diffuse,:] += widotn[mask_diffuse,:] * lightSGsatrandomsample

# MC specular
brdf = torch.zeros(mask_specular.shape[0], 3).to(device=gpudevice)
r2 = alpha[mask_specular] ** 2
r2 = r2.unsqueeze(1).expand(-1,3).cuda()

if shade_specular:
    for i in range(num_MC_samples):
        lightdir = torch.from_numpy(np.float32(2 * np.random.rand(mask_specular.shape[0], 3) - 1.0)).to(device=gpudevice)
        lightdir = sgr.normalize(lightdir)
        # # # # reflection
        halfdir = sgr.normalize(wi[mask_specular, :].to(device=gpudevice) + lightdir[mask_specular, :])
        widotn = sgr.dot(lightdir.to(device=gpudevice), normals.to(device=gpudevice))
        cos_theta = sgr.dot(halfdir, normals[mask_specular,:].clone().to(device=gpudevice))
        if ggx:# # # Falcor GGX:
            d = ((cos_theta * r2  - cos_theta) * cos_theta + 1)
            brdf[mask_specular, :] = r2  / (d * d * np.pi)
        else: # # # Beckman
            tana = (- cos_theta**2 + 1.0 ) / (cos_theta**2)
            # tana[torch.isinf(tana)] = 1.0e+12
            # print(tana.min(), tana.max())
            brdf[mask_specular, :] = torch.exp(-(tana / r2)) / (np.pi * r2 * (cos_theta**4))
            brdf[(widotn <= 0.0)] = 0.0

        brdf = brdf * specular_bsdf_val.to(device=gpudevice) * (widotn > 0.0)
        lightSGsatrandomsample = torch.zeros(mask_specular.shape[0], 3).cuda()
        for l in range(numSGs):
            lightSGsatrandomsample += sglights[:, l, 0:3] * torch.exp(sglights[:, l, 3].unsqueeze(1).expand(-1,3)
                                                                      * (sgr.dot(lightdir, sglights[:, l, 4:7]) - 1.0))
        mcpred[mask_specular, :] += lightSGsatrandomsample[mask_specular, :] * brdf[mask_specular, :]


# # do MC for the transparent shading from behind estimating DxL
r2 = (alpha[mask_trans] ** 2).unsqueeze(1).expand(-1,3).cuda()

if shade_transmitted:
    # print(mask_trans.shape, ior[mask_trans, :].shape, wi[mask_trans, :].shape)
    for i in range(num_MC_samples):
        lightdir = torch.from_numpy(np.float32(2 * np.random.rand(mask_trans.shape[0], 3) - 1.0)).to(device=gpudevice)
        lightdir = sgr.normalize(lightdir)
        widotn = sgr.dot(lightdir.to(device=gpudevice), normals.to(device=gpudevice))
        # # refraction
        halfdir = - 1.0 * sgr.normalize(wi[mask_trans, :].to(device=gpudevice) + ior[mask_trans, :].to(device=gpudevice) * lightdir[mask_trans, :])
        cos_theta = sgr.dot(halfdir, normals[mask_trans,:].to(device=gpudevice))
        if ggx:# # # Falcor GGX:
            d = ((cos_theta * r2  - cos_theta) * cos_theta + 1.0)
            brdf[mask_trans, :] = r2  / (d * d * np.pi)
        else: # # # Beckman
            tana = (- cos_theta**2 + 1.0 ) / (cos_theta**2)
            D = torch.exp(-(tana / r2)) / (np.pi * r2 * (cos_theta**4))
            D[(cos_theta == 0.0)] = 0.0
            brdf[mask_trans, :] = D

        brdf[(widotn >= 0.0)] = 0.0
        brdf = brdf * trans_bsdf_val.to(device=gpudevice)
        lightSGsatrandomsample = torch.zeros(mask_trans.shape[0], 3).cuda()
        for l in range(numSGs):
            lightSGsatrandomsample += sglights[:, l, 0:3] * torch.exp(sglights[:, l, 3].unsqueeze(1).expand(-1,3)
                                                                      * (sgr.dot(lightdir, sglights[:, l, 4:7]) - 1.0))
        mcpred[mask_trans, :] += lightSGsatrandomsample[mask_trans, :] * brdf[mask_trans, :]
        # print('lightSGsatrandomsample ', lightSGsatrandomsample.min(), lightSGsatrandomsample.mean(), lightSGsatrandomsample.max())


# for refraction multiply by 2pi (because only sampling behind normal), for reflection by 4pi (sampling full sphere)
mcpred = mcpred * 4.0 * np.pi / num_MC_samples
mcim = torch.zeros(gtshape).view(-1,3)
mcim[mask, :] = mcpred.cpu()
mcim = mcim.view(gtshape)
print(mcim.min(), mcim.mean(), mcim.max())

pr = torch.zeros(gtshape)
pr = pr.view(-1, 3)
pr[mask, :] = pred.cpu()
pr = pr.unsqueeze(0).view(gtshape)

# plt.imshow((pr/pr.max())**(1.0/2.2))
# plt.show()
# plt.imshow((mcim/mcim.max())**(1.0/2.2))
# plt.show()

print("average ratio ", (pr[full_mask_trans,:]/ mcim[full_mask_trans,:]).mean(), (pr[full_mask_trans,:] / mcim[full_mask_trans,:] ).std())


print("render statistics : ", pr.min(), mcim.min())
print("render statistics : ", pr.mean(), mcim.mean())
print("render statistics : ", pr.max(), mcim.max())


pr = pr.permute(2,0,1)
mcim = mcim.permute(2,0,1)


img_grid = torchvision.utils.make_grid([ pr, mcim ], nrow=2)
plt.imshow(1.0 * ((img_grid/img_grid.max())**(1.0/2.2)).permute(1,2,0))
plt.show()





# test with different ior
# test with different roughness



