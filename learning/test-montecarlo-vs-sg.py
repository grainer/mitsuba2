import os
import numpy as np
import enoki as ek
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader

import mitsuba
# Set the desired mitsuba variant (MUST BE THE SAME IN ALL PYTHON FILES !!!)
import nets
mitsuba.set_variant('gpu_rgb') #'gpu_autodiff_rgb')
import enoki
from mitsuba.core import Float, UInt32, Vector2f, Vector3f, Thread, Frame3f, xml, ScalarTransform4f, Bitmap, Struct
from mitsuba.render import MicrofacetDistribution, MicrofacetType
from mitsuba.core.xml import load_file, load_dict
from mitsuba.render import (BSDF, BSDFContext, BSDFFlags,
                            BSDFSample3f, SurfaceInteraction3f, register_bsdf,
                            DirectionSample3f, Emitter, ImageBlock,
                            SamplingIntegrator, has_flag,
                            register_integrator, reflect)
from mitsuba.python.util import traverse
import renderhelpers as sgr
import matplotlib.pyplot as plt
torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True

# Input paths and parameters ===========================================================================================
scenepath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/dynamic-envmap-bunny'
outputpath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/results/dynamic-allmaterials/alien'
envmap_dir = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/envmaps/IndoorHDRDataset2018-256x128'
# envmap_dir = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/render-assets' #
scenename_gt = 'aovs-all.xml'

# Training parameters ==================================================================================================
max_epochs = 50000# 10000
num_freqs = 6# 0 # 0 means no positional encoding
latentdim = 128#  72# 72#    128# 72#  350# #128#   512#
numSGs = 50#  25#      50#
cnndim = 2048# 96# 512# 2048#  4096#
netwidth = 256# 256#    64# 400#      250#      64#
batch_size = 4#  10#0# 60#  5# 5# 24
num_layers = 3# 4# 3#  4#    1#     5# 3#
learning_rate = 0.0002#   0.0001#   0.005#   0.01# 0.5#
logloss = True# False#
L2loss = False# True#
imsize = 100#  400# 256#
exponentiate = True #  False#  True#
withnormals = True#  False
insize = 3 + 2 * 3 * num_freqs + 3*withnormals


experiment_name = 'catcherror-alias-25SGs-fixcambatch-avgpool-nerfinit-nonorm-exprgbscaled-expscaledsharp-tanhangles-nobias'
# experiment_name = experiment_name +'-batchsize-' +str(batch_size) +'lats-'+str(latentdim) +'-layers-' +str(num_layers) +'-width-' +str(netwidth) \
#                   +'-numSG-'+str(numSGs) +'-lr-'+str(learning_rate) +'-freq-' + str(num_freqs)\
#                   +'-log-'+str(int(logloss)) +'-L2-'+str(int(L2loss))+'-withnormals-' + str(int(withnormals))
use_manual_SGs = False#
# alpharough = 0.2



num_MC_samples = 100000

# # # # Pisa:
# use_manual_SGs = True#
# lightSG = [[0.9433718, 0.37715107, 0.030528598, 35.069885, -0.84324276, 0.2121104, 0.4939137], [0.70375603, 0.3655845, 0.15851796, 23.585962, 0.52402025, 0.7886307, -0.32165867], [0.08567178, 0.06427135, 0.044218015, 1.1091332, -0.2356962, -0.66532904, -0.70836747], [0.6558805, 0.31723064, 0.11005787, 21.001013, -0.60743445, 0.73271066, -0.30685267], [0.86364007, 0.35598388, 0.11529839, 22.24271, 0.46149436, 0.2683993, -0.8455677], [1.1314801, 1.2558013, 1.2232834, 41.10061, 0.053204477, 0.256633, 0.9650434], [1.0217043, 0.4285945, 0.13063939, 15.990939, -0.047010783, 0.8666322, -0.49672785], [0.20844299, 0.3255447, 0.49083838, 5.3000503, -0.44954064, 0.51849866, 0.72737354], [1.158383, 1.5155687, 1.7711425, 46.52204, 0.38086146, 0.38595143, 0.84022975], [2.3821747, 2.6763604, 2.6895938, 227.89229, 0.6516599, 0.27348915, 0.7074907], [0.66085684, 0.31836376, 0.1304988, 49.41547, 0.9093498, 0.18544276, -0.3724163], [0.7925673, 1.0842797, 1.393003, 14.961501, 0.7191419, 0.51414, 0.46743447]]
# numSGs = 12

# use_manual_SGs = True#
# lightSG = [[1.0, 1.0, 1.0, 1000, 0.0, 1.0, 0.0], [1.0, 1.0, 1.0, 1000, 0.0, 1.0, 0.0]]
# numSGs = 2

# # Ennis map:
# lightSG = [[19.77183, 18.899605, 28.09268, 124.06032, 0.13278571, 0.39354134, 0.90966654], [13.951765, 15.265006, 23.527641, 120.74434, -0.15289421, 0.5411309, 0.8269225], [0.4071942, 0.22668266, 0.10997803, 0.89142746, -0.0812242, -0.6204905, 0.7799963], [64.97454, 42.578075, 16.2094, 154.95787, -0.20876692, 0.07599607, 0.97500813], [5.1314754, 4.024345, 3.1723998, 91.700905, -0.88628745, -0.11917673, -0.44753936], [26.087568, 35.68813, 47.75076, 40.86883, -0.18703045, 0.25262922, 0.94931453], [11.181716, 15.722419, 25.156954, 210.88081, 0.10982957, 0.5572564, 0.82304484], [1.8714151, 1.836281, 1.6746786, 35.524567, 0.34814197, -0.37467614, 0.8593107], [51.333977, 24.03316, 0.3976881, 90.55722, 0.124854945, 0.100686945, 0.98705286], [4.564536, 3.5192788, 2.8387885, 131.00095, 0.9625086, -0.11077466, -0.24760067], [0.1044002, 24.66059, 42.970016, 91.73856, 0.13887106, 0.15844153, 0.97755367], [103.84179, 74.74271, 46.163086, 1047.5914, -0.42761734, 0.07404973, 0.90092176]]


# £ TODO: try MC integration of warped SG NDF. That should be pixel accurate with SGint


# Other parameters (hardcoded)
num_workers = 0
spp = 32 # 32#
hybridanis = False#
anisowarp = True#   False
sharpness_thresh = 5 #for the anisotropic NDF warp
cos_sharpness = 2.13
cos_amplitude = 1.17
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
if not os.path.exists(outputpath):
    os.makedirs(outputpath)
# Load the scene XMLs
Thread.thread().file_resolver().append(scenepath)
scene_gt = load_file(scenepath+'/'+scenename_gt)

if not use_manual_SGs:
    #Load envmaps, DO the train validation data split on the filenames
    exr_files = []
    for r, d, f in os.walk(envmap_dir):  # r=root, d=directories, f = files
        for file in f:
            if '.exr' in file:
                exr_files.append(os.path.join(r, file))
    print(len(exr_files), ' EXR files found')
    idxs = np.arange(len(exr_files))
    np.random.shuffle(idxs)
    cutoff = int(0.8 * len(exr_files))
    exr_files = np.array(exr_files)
    files_train = exr_files[idxs[0:cutoff]]
    files_val = exr_files[idxs[cutoff:]]
    print(len(files_train), ' training files')
    print(len(files_val), ' testing files')
    print('Load validation data.')
    val_dataset = sgr.EnvmapDataset(files_val, False, False)
    val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=True, num_workers=num_workers)#, pin_memory=True, drop_last=True)

# Find differentiable scene parameters
params = traverse(scene_gt)
param_res = params['dynamic_envmap.resolution']
param_ref = params['dynamic_envmap.data']
params.keep(['dynamic_envmap.data'])

if not use_manual_SGs:
    net = nets.RenderingNet(cnndim, latentdim, numSGs, insize, netwidth, num_layers, num_freqs).to(device=gpudevice)
    net.load_state_dict(torch.load(outputpath +'/runs/' + experiment_name +'-net.pth'))
    net.eval()
sg_centers = sgr.initialiseSGcenters(numSGs).to(device=gpudevice)
envmap_mitsuba = torch.ones(param_res[1], param_res[0], 4).to(gpudevice)

with torch.no_grad():
    if not use_manual_SGs:
        envmap, envmap_m, envmap_std = next(iter(val_dataloader))
        envmap_mitsuba[:, :, 0:3] = (torch.exp(envmap.squeeze() * envmap_std + envmap_m)).permute(1, 2, 0).to(gpudevice)
        params['dynamic_envmap.data'] = Float(envmap_mitsuba.view(-1))
        params.update()

    randomsensor, campos = sgr.generate_random_sensor(32, 400, 400)  # fix the camera in the batch
    # gtcpu, pr, img_grid2 = sgr.render_gt_and_sg(scene_gt, randomsensor, campos, net, envmap[0],
    #                               envmap_m[0], envmap_std[0],
    #                               num_freqs, numSGs, withnormals, 32,
    #                               sg_centers, exponentiate, 400,
    #                                    gpudevice, True)
    scene_gt.integrator().render(scene_gt, randomsensor)
    components = randomsensor.film().bitmap(raw=False).split()
    for i in range(len(components)):
        if 'position' in components[i][0]:
            buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
            pos3d = torch.from_numpy(buffer.data_np())
        elif 'normals' in components[i][0]:
            buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
            normals = torch.from_numpy(buffer.data_np())
        elif 'bsdf' in components[i][0]:
            buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
            bsdf_val = torch.from_numpy(buffer.data_np())
        elif 'gt' in components[i][0]:
            buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
            gtcpu = torch.from_numpy(buffer.data_np())
        elif 'alpha' in components[i][0]:
            buffer = components[i][1].convert(Bitmap.PixelFormat.Y, Struct.Type.Float32, srgb_gamma=False)
            alpha = torch.from_numpy(buffer.data_np())
            print('min alpha: ', alpha.min())

    del scene_gt, randomsensor, components
    enoki.cuda_malloc_trim()

    assert (pos3d != pos3d).any() == False, "Damn! pos3d contains NaNs"
    assert (normals != normals).any() == False, "Damn! normals contains NaNs"
    assert (bsdf_val != bsdf_val).any() == False, "Damn! bsdf_val contains NaNs"
    assert (gtcpu != gtcpu).any() == False, "Damn! gtcpu contains NaNs"
    assert (alpha != alpha).any() == False, "Damn! alpha contains NaNs"
    # to make sure, remove background
    gtcpu = gtcpu * (bsdf_val > 0.0)
    # flatten everything
    pos3d = pos3d.view(-1, 3)
    normals = normals.view(-1, 3)
    bsdf_val = bsdf_val.view(-1, 3)
    alpha = alpha.view(-1)

    # isolate pixels that need shading
    mask = (torch.sum(bsdf_val * bsdf_val, dim=-1, keepdim=False) > 0.0)
    pos3d = pos3d[mask, :]
    pred = torch.zeros(pos3d.shape).to(device=gpudevice)
    normals = normals[mask, :]
    bsdf_val = bsdf_val[mask, :]
    alpha = alpha[mask]
    netin = pos3d

    if not use_manual_SGs:
        if num_freqs > 0:
            netin = nets.positional_encoding(pos3d, num_encoding_functions=num_freqs)
        if withnormals:
            netin = torch.cat((netin, normals), dim=1)
        assert (envmap != envmap).any() == False, "Damn! envmap contains NaNs"
        net_out = net(envmap.to(device=gpudevice), netin.to(device=gpudevice))

        # Undo the normalization of light intensities
        net_out = net_out.unsqueeze(2).view(-1, numSGs, 6)
        assert (net_out != net_out).any() == False, "Damn! net_out contains NaNs"
        net_out[:, :, 0:3] = net_out[:, :, 0:3] * envmap_std.to(device=gpudevice) + envmap_m.to(device=gpudevice)
        sglights = sgr.deparametrize_torch(net_out, sg_centers, exponentiate)
        sglights[:, :, 0:3] = sglights[:, :, 0:3]
        sglights[:, :, 3] = sglights[:, :, 3] * numSGs / np.pi
        print('minmax RGB prediction: ', sglights[:, :, 0:3].min(), sglights[:, :, 0:3].max())
        print('minmax sharpness: ', sglights[:, :, 3].min(), sglights[:, :, 3].max())
        print('minmax thetaphi: ', net_out[:, :, 3:4].min(), net_out[:, :, 3:4].max())
        assert (sglights != sglights).any() == False, "Damn! net_out contains NaNs after denormalizing"

    else:
        sglights = torch.tensor(lightSG).unsqueeze(0).expand(pos3d.shape[0], -1,-1).cuda()


    # diffuse shading
    mask_diffuse = (alpha == 1.0)
    irr = torch.zeros(torch.sum(mask_diffuse), 3).to(device=gpudevice)
    for l in range(numSGs):
        irr = irr + sgr.exact_cosSGinnerProduct(normals[mask_diffuse, :].to(device=gpudevice),
                                            sglights[mask_diffuse, l, 0:3], sglights[mask_diffuse, l, 3],
                                            sglights[mask_diffuse, l, 4:7])
    pred[mask_diffuse, :] = pred[mask_diffuse, :] + irr * bsdf_val[mask_diffuse, :].to(device=gpudevice)
    assert (irr != irr).any() == False, "Damn! irr contains NaNs"

    # specular shading (anisotropic)
    mask_specular = (alpha < 1.0)
    wi = sgr.normalize(campos.unsqueeze(0).expand(torch.sum(mask_specular), -1) - pos3d[mask_specular, :])
    r2 = alpha[mask_specular] ** 2
    ndf_amp = 1.0 / (r2 * np.pi)
    ndf_amp = ndf_amp.unsqueeze(1).expand(-1, 3)
    ndf_sharp = 2.0 / r2
    ndf_sharp = ndf_sharp.unsqueeze(1).expand(-1, 3)
    amp, warpX, warpY, warpZ, sharpX, sharpY = sgr.warpBRDF_ASG(ndf_amp.to(gpudevice), ndf_sharp.to(gpudevice),
                                                            normals[mask_specular, :].to(gpudevice),
                                                            wi.to(gpudevice))
    ndf_val = torch.zeros(wi.shape, device=gpudevice)
    for l in range(numSGs):
        ndf_val = ndf_val + sgr.convolveASG_SG(amp, warpX, warpY, warpZ, sharpX, sharpY,
                                           sglights[mask_specular, l, 0:3],
                                           sglights[mask_specular, l, 3].unsqueeze(1).expand(-1, 3),
                                           sglights[mask_specular, l, 4:7])
    assert (ndf_val != ndf_val).any() == False, "Damn! ndf_val contains NaNs"
    pred[mask_specular, :] = pred[mask_specular, :] + torch.nn.functional.relu(
        ndf_val * bsdf_val[mask_specular, :].to(gpudevice))

    pr = torch.zeros(gtcpu.shape[0]*gtcpu.shape[1],3)
    pr[mask.cpu(),:] = pred.cpu()
    pr = pr.view(gtcpu.shape)

    # specular shading (isotropic)
    pred[mask_specular, :] = 0.0
    ndf_val = torch.zeros(wi.shape, device=gpudevice)
    sg_amp, sg_sharp, sg_axi = sgr.warpBRDF_SG(ndf_amp.to(gpudevice), ndf_sharp.to(gpudevice),
                                                            normals[mask_specular, :].to(gpudevice),
                                                            wi.to(gpudevice))
    for l in range(numSGs):
        ndf_val = ndf_val + sgr.SGinnerProduct(sglights[mask_specular, l, 0:3], sglights[mask_specular, l, 3].unsqueeze(1).expand(-1, 3),
                                           sglights[mask_specular, l, 4:7], sg_amp, sg_sharp, sg_axi)
    pred[mask_specular, :] = torch.nn.functional.relu(ndf_val * bsdf_val[mask_specular, :].to(gpudevice))
    priso = torch.zeros(gtcpu.shape[0] * gtcpu.shape[1], 3)
    priso[mask.cpu(), :] = pred.cpu()
    priso = priso.view(gtcpu.shape)



    # Monte carlo integration of SGs
    mcpred = torch.zeros(pos3d.shape).cuda()  # .to(device=gpudevice)
    mcsgpred = torch.zeros(pos3d.shape).cuda()
    randomlightdirs = torch.from_numpy(np.float32(2 * np.random.rand(num_MC_samples, 3) - 1.0))
    randomlightdirs = sgr.normalize(randomlightdirs).cuda()
    print(randomlightdirs)
    viewdir = sgr.normalize(campos.unsqueeze(0).expand(torch.sum(mask_specular), -1) - pos3d[mask_specular, :]).cuda()
    r2 = r2.unsqueeze(1).expand(-1,3).cuda()

    for i in range(num_MC_samples):
        lightdir = randomlightdirs[i, :].unsqueeze(0).expand(pos3d.shape)

        brdf = torch.zeros(lightdir.shape).cuda()
        brdf[mask_diffuse,:] = torch.nn.functional.relu(sgr.dot(lightdir, normals.cuda())[mask_diffuse,:])

        halfdir = sgr.normalize(viewdir + lightdir[mask_specular,:])
        cos_theta = sgr.dot(halfdir, normals[mask_specular,:].cuda())
        tana = (1.0 - cos_theta**2) / (cos_theta**2)
        # Beckmann:
        brdf[mask_specular,:] = torch.exp(-(tana / r2)) / (np.pi * r2 * (cos_theta**4))

        sgbrdf = torch.zeros(lightdir.shape).cuda()
        sgbrdf[mask_diffuse,:] = 1.17 * torch.exp(2.133 * (sgr.dot(lightdir, normals.cuda())[mask_diffuse,:] - 1.0))

        sgbrdf[mask_specular, :] = torch.exp(2.0 / r2 * (sgr.dot(halfdir, normals[mask_specular,:].cuda()) - 1.0)) \
                                   / (np.pi * r2)
        # sgbrdf[mask_specular, :] = sg_amp * torch.exp(sg_sharp * (sgr.dot(lightdir[mask_specular,:], sg_axi) - 1.0))



        lightSGsatrandomsample = torch.zeros(lightdir.shape).cuda()
        for l in range(numSGs):
            lightSGsatrandomsample += sglights[:, l, 0:3] * torch.exp(sglights[:, l, 3].unsqueeze(1).expand(-1,3)
                                                                      * (sgr.dot(lightdir, sglights[:, l, 4:7]) - 1.0))
        mcpred += lightSGsatrandomsample * brdf
        mcsgpred += lightSGsatrandomsample * sgbrdf

    mcpred = mcpred *4.0*np.pi / num_MC_samples
    mcim = torch.zeros(gtcpu.shape[0] * gtcpu.shape[1], 3)
    mcim[mask.cpu(), :] = mcpred.cpu() * bsdf_val
    mcim = mcim.view(gtcpu.shape)

    mcsgpred = mcsgpred * 4.0 * np.pi / num_MC_samples
    mcsgim = torch.zeros(gtcpu.shape[0] * gtcpu.shape[1], 3)
    mcsgim[mask.cpu(), :] = mcsgpred.cpu() * bsdf_val
    mcsgim = mcsgim.view(gtcpu.shape)


    # plt.imshow(mcim)
    # plt.show()
    # plt.imshow(pr)
    # plt.show()
    # plt.imshow(gtcpu)
    # plt.show()

    gtcpu = gtcpu**(1.0 / 2.2)
    pr = pr ** (1.0 / 2.2)
    mcim = mcim ** (1.0 / 2.2)
    mcsgim = mcsgim ** (1.0 / 2.2)
    priso = priso ** (1.0 / 2.2)
    gtmax = mcsgim.max() #  / 3.0


    gtcpu = gtcpu.permute(2, 0, 1) / gtmax
    pr = pr.permute(2, 0, 1) / gtmax
    mcim = mcim.permute(2, 0, 1) / gtmax
    mcsgim = mcsgim.permute(2, 0, 1) / gtmax
    priso = priso.permute(2, 0, 1) / gtmax

    ref = mcim
    multiplier = 2.0
    mcim_diff = torch.abs(mcim - ref) * multiplier
    mcsgim_diff = torch.abs(mcsgim - ref) * multiplier
    priso_diff = torch.abs(priso - ref) * multiplier
    pr_diff = torch.abs(pr - ref) * multiplier
    gtcpu_diff = torch.abs(gtcpu - ref) * multiplier

    img_grid = torchvision.utils.make_grid([mcim, mcsgim, priso, pr, gtcpu,
                                            mcim_diff, mcsgim_diff, priso_diff, pr_diff, gtcpu_diff], nrow=5)
    plt.imshow(1.5 * img_grid.permute(1,2,0))
    plt.title('MC BSDFxSG lights,                      MC SG_BSDFxSG lights,                      SGint (isotropic warp),                      SGint (anisotropic warp),                      GT pathtraced (max_depth 2)')
    plt.show()

    # diff = torch.abs(pr - gtcpu)
    # # print(gtcpu.shape, pr.shape, diff.shape)
    # print('maxs and mins  ', gtcpu.max(), pr.max(), diff.max(), gtcpu.min(), pr.min(), diff.min())
    # img_grid = torchvision.utils.make_grid([pr, gtcpu, diff])
    # img_grid = img_grid / gtcpu.max()
    # img_grid = img_grid**(1/2.2)