import os
import torch
import cv2
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
import nets
from collections import OrderedDict

import renderhelpers as sgr

# data_dir = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/envmaps/IndoorHDRDataset2018-256x128'
data_dir = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/envmaps/OutdoorHDR-256x128'

outputpath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/results/AEnetworks/'

modelname = '25lat-long-AE-nocoordconv-leaky.pth'

# data_train = 'F:/gilles/data/envmaps/laval-preview/train'
# data_val = 'F:/gilles/data/envmaps/laval-preview/test'
num_epochs = 50000    #    2500# 50#  100
imwidth = 128
logtransf = True# False#

lossL2 = False# True#

resize_ims = False
num_workers = 0# 2
batch_size = 12# 24
learning_rate = 0.000025
# Best test loss was  0.002839172903424383

latentdim = 25#  128#  72# 512#      # 512#

cnndim = 2048# 256#  2048#  4096#

# TODO : Modularize the convolutional model so the sizes can be input as lists for maximum flexibility and control from the hard code.
# TODO : Tensorboard viz. Make CNN encoder a function of the number of CNN layers I want so that I can experiment with several depths

# # DO the train validation data split on the filenames
exr_files = []
for r, d, f in os.walk(data_dir):  # r=root, d=directories, f = files
    for file in f:
        if '.exr' in file:
            exr_files.append(os.path.join(r, file))
print(len(exr_files), ' EXR files found')
idxs = np.arange(len(exr_files))
np.random.shuffle(idxs)
cutoff = int(0.8 * len(exr_files))
exr_files = np.array(exr_files)
files_train = exr_files[idxs[0:cutoff]]
files_val = exr_files[idxs[cutoff:]]
print(len(files_train), ' training files')
print(len(files_val), ' testing files')

class AEDataset(Dataset):
    def __init__(self, filenames, augment=True, transform=None, max=False):
        self.transform = transform
        self.files = filenames
        self.images = []
        self.means = []
        self.stds = []
        self.width = 0
        self.augment = augment
        self.maxmin = max

        self.minvalue = 0.00001
        c = 0
        thresh = torch.nn.Threshold(self.minvalue, self.minvalue, inplace=True)

        mmin = 1.0
        mmax = 0.0

        for f in self.files:
            if c%50 == 0:
                print('Read ', c, ' EXR files.')
            im = torch.clip(torch.from_numpy(
                cv2.cvtColor(cv2.imread(f, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH),
                             cv2.COLOR_RGB2BGR)).permute(2, 0, 1), 0.0)
            if self.transform:
                im = self.transform(im)
            # if logtransf:

            im = im / im.mean() # (torch.sum(im) / im.numel())

            mmin = np.minimum(mmin, im.min().item())
            mmax = np.maximum(mmax, im.max().item())

            im = torch.log(1.0 + thresh(im))
            im_m = 0
            im_std = 1

            if self.maxmin:
                im_m = 0 # im.min()
                im_std = im.max() - im_m
                im = (im - im_m) / im_std

            self.images.append(im)
            self.means.append(im_m)
            self.stds.append(im_std)
            c += 1
            self.width = im.shape[2]
        print('minmax of envmaps, ', mmin, mmax)
    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        # print(idx)
        if self.augment:
            rota = int(self.width * np.random.rand(1))
            rgbperm = torch.randperm(3)
            t = self.images[idx]
            ims = torch.zeros(t.shape)
            ims[:,:, 0:self.width-rota] = t[rgbperm,:,rota:self.width]
            ims[:,:, self.width-rota:self.width] = t[rgbperm,:, 0:rota]

            return ims, self.means[idx], self.stds[idx]
        else:
            return self.images[idx], self.means[idx], self.stds[idx]

transf = None
if resize_ims:
    transf = transforms.Resize(imwidth)
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('Load training data.')
train_dataset = AEDataset(files_train, True, transf, max = False)
print('Load validation data.')
val_dataset = AEDataset(files_val, False, transf, max = False)
print('Training data loaded.')
train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=num_workers)#, pin_memory=True, drop_last=True)
val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=False, num_workers=num_workers)#, pin_memory=True, drop_last=True)

model = nets.EnvmapAE(cnndim, latentdim).to(device)
model.train()
print('start training on '+ str(len(train_dataloader)) +' envmap batches.')
if lossL2:
    criterion = nn.SmoothL1Loss(reduce=False, reduction='none') # nn.MSELoss()#
else:
    criterion = nn.L1Loss(reduce=False, reduction='none')#
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

best_testloss = 10000.0
for epoch in range(num_epochs):
    running_loss = 0.0
    for batch, _, _ in train_dataloader:
        if not logtransf:
            batch = torch.exp(batch)

        batch = batch.to(device)
        outputs = model(batch)
        # print(outputs.shape)
        optimizer.zero_grad()
        # loss = criterion(outputs, batch)
        loss = (torch.abs(outputs - batch) * torch.exp(batch)).mean()
        # loss = criterion( torch.exp(outputs), torch.exp(batch) ).mean()
        loss.backward()
        optimizer.step()
        running_loss += loss.item()
        # scheduler.step()
    epoch_loss = running_loss / len(train_dataloader)
    print('Epoch:{} Loss: {:.4f} '.format(epoch, epoch_loss))

    with torch.no_grad():
        test_loss = 0.0
        for inp, m, std in val_dataloader:
            if not logtransf:
                inp = torch.exp(inp)
            inp = inp.to(device)
            out = model(inp)
            # loss = criterion(out, inp)
            tloss = (torch.abs(out - inp) * torch.exp(inp)).mean()
            # loss = criterion(torch.exp(out), torch.exp(inp))
            test_loss += tloss.item()
        test_loss = test_loss / len(val_dataloader)
        print('      Test Loss: {:.4f} '.format(test_loss))
        if test_loss < best_testloss:
            best_testloss = test_loss
            best_model_state_dict = {k: v.to('cpu') for k, v in model.state_dict().items()}
            best_model_state_dict = OrderedDict(best_model_state_dict)

print('Best test loss was ', best_testloss)
model.load_state_dict(best_model_state_dict)
model.eval()

torch.save(model, outputpath + modelname)

with torch.no_grad():
    test_loss = 0.0
    for inp, m, std in val_dataloader:
        inp = inp.to(device)
        out = model(inp)

        plt.figure(figsize=(10, 2))
        plt.subplot(1, 2, 1)
        gt = inp.cpu().squeeze_().permute(1,2,0)

        # gt = 0.5 * gt + 0.5
        gt = gt * std + m
        gt = torch.exp(gt) - 1.0

        gtmax = gt.max()
        plt.imshow((gt/gtmax).numpy()**(1.0/2.2) * 3.0)
        plt.subplot(1, 2, 2)
        pred = out.cpu().squeeze_().permute(1, 2, 0)

        # pred= 0.5* pred + 0.5
        pred = pred * std + m
        if logtransf:
            pred = torch.exp(pred) - 1.0
        plt.imshow((pred/gtmax).numpy()**(1.0/2.2) * 3.0)
        plt.show()

# class EnvmapDatasetProgressive(Dataset):
#     def __init__(self, maps_dir, transform=None):
#         self.maps_dir = maps_dir
#         self.transform = transform
#         self.files = []
#         for r, d, f in os.walk(maps_dir):# r=root, d=directories, f = files
#             for file in f:
#                 if '.exr' in file:
#                     self.files.append(os.path.join(r, file))
#         # print('EXR dataset found:')
#         # print(self.files)
#
#     def __len__(self):
#         return len(self.files)
#
#     def __getitem__(self, idx):
#         im = torch.clip(torch.from_numpy(cv2.cvtColor(cv2.imread(self.files[idx], cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)).permute(2,0,1), 0.0)
#         if self.transform:
#             im = self.transform(im)
#         im = torch.log(1.0 + im)
#         im_m = im.mean()
#         im_std = im.std()
#         im = (im - im_m) / im_std
#         return im, im_m, im_std