import os
import numpy as np
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader
import sys
import os
import cv2
import nets
from torchvision.utils import save_image

import renderhelpers as sgr
import matplotlib.pyplot as plt

import einops
import siren_pytorch as siren

torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True

dropbox_path = 'F:/gilles/Dropbox/'
# Input paths and parameters ===========================================================================================
train_path = dropbox_path + 'INRIA/projects/nprt/data/scenes/atelier/train'
test_path = dropbox_path + 'INRIA/projects/nprt/data/scenes/atelier/test'

aabb = [[-146.994873, -59.336391, -170.382034], [146.993851, 85.836891, 113.362228]] # for atelier

envmap_trainpath = dropbox_path + 'INRIA/projects/nprt/data/scenes/indoorenvs/train'
envmap_testpath = dropbox_path + 'INRIA/projects/nprt/data/scenes/indoorenvs/test'

highres_test_path = dropbox_path + 'INRIA/projects/nprt/data/scenes/atelier/highres'
highres_envmap_testpath = dropbox_path + 'INRIA/projects/nprt/data/scenes/indoorenvs/highres'

outputpath = dropbox_path + 'INRIA/projects/nprt/res/pixelbasis'

experiment_name = 'hybridpixel-nobandlimitbrdf-' #
# Training parameters ==================================================================================================
batch_size = 3



# train_path = dropbox_path + 'INRIA/projects/nprt/data/scenes/atelier/test'
# envmap_trainpath = dropbox_path + 'INRIA/projects/nprt/data/scenes/indoorenvs/test'
# batch_size = 1# 10#0# 60#  5# 5# 24



num_angles = 128# 64 # 256
latentdim = 64 # 128 #8# 72# 72#    128# 72#  350# #128#   512#
netwidth = 256# 512#  64# 400#      250#      64#
num_layers = 2#  4  # 4# 3#  4#    1#     5# 3#

learning_rate = 0.0001#  0.0001  # 0.0001#   0.005#   0.01# 0.5#
logloss = True# True  # False#
L2loss = False  # True#
max_epochs = 500000  # 10000
cnndim = 2048  # 96# 512# 2048#  4096#
withnormals = True # True  # False
encode_normals = False # False # True

tensorboard_writes = 500

experiment_name = experiment_name + '-angles-' + str(num_angles) + '-bats-' + str(batch_size) + '-lat-' + str(latentdim) + '-layer-' + str(
                    num_layers) + '-width-' + str(netwidth) + '-lr-' + str(learning_rate)
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")

# Other parameters (hardcoded)
num_workers = 0
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
if not os.path.exists(outputpath):
    os.makedirs(outputpath)
boundingboxmin = torch.tensor(aabb[0]).unsqueeze(0).unsqueeze(0)
boundingboxsize = torch.tensor(aabb[1]).unsqueeze(0).unsqueeze(0) - boundingboxmin

# Get the angular directions to sample:
sample_angles = torch.zeros([num_angles, 2])
sampling_directions = sgr.fibonacci_sphere(num_angles)#+1)[0:num_angles,:]
print(sampling_directions)
for i in range(num_angles):
    # TODO : check the envmap is the right way around. theta definitely needs to be negated, dunno about phi though
    # uv.x = atan2(p.x, -p.z) * M_1_2PI + 0.5
    # uv.y = acos(p.y) * M_1_PI;

    # theta = torch.asin( - sampling_directions[i,1])
    # phi = torch.atan2(sampling_directions[i,2],sampling_directions[i,0])
    # sample_angles[i, 0] = (theta / np.pi + 0.5) * 127
    # sample_angles[i, 1] = (phi / np.pi + 1.0) * 127
    theta = - torch.asin(sampling_directions[i,1]) / (np.pi / 2.0)
    phi = torch.atan2(sampling_directions[i,0], -sampling_directions[i,2]) / np.pi
    sample_angles[i,0] = (theta + 1.0) / 2.0 * 127 % 128
    sample_angles[i,1] = (phi + 1.0) / 2.0 * 255 % 256
print(sample_angles)

class BufferDataset(Dataset):
    def __init__(self, renderpath, envpath):
        self.alpha = []
        self.diffuse = []
        self.specular = []
        self.envmap = []
        self.gt = []
        self.normals = []
        self.position = []
        self.viewdir = []
        self.envfiles = []
        for r, d, f in os.walk(envpath):  # r=root, d=directories, f = files
            for file in f:
                if '.exr' in file:
                    self.envfiles.append(file)
                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH),
                                      cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(im)
                    self.envmap.append(im)
        c = 0
        for r, d, f in os.walk(renderpath):  # r=root, d=directories, f = files
            for file in f:
                if '.exr' in file:
                    if c % 50 == 0:
                        print('Read ', c, ' EXR buffers.')

                    filename = os.path.splitext(os.path.basename(file))[0]
                    fname = filename.split('.')
                    if int(fname[0]) == c:
                        c = c+1

                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(im)

                    if fname[2] == 'posW':
                        im = (im - boundingboxmin.expand(im.shape[0], im.shape[1], -1)) / boundingboxsize.expand(im.shape[0], im.shape[1], -1)
                        im = (im-0.5) * 2
                        self.position.append(im)
                    if fname[2] == 'alpha':
                        self.alpha.append(im)
                    if fname[2] == 'diffuseOpacity':
                        self.diffuse.append(im / np.pi)
                    if fname[2] == 'output':
                        self.gt.append(im)
                    if fname[2] == 'normW':
                        self.normals.append(im)
                    if fname[2] == 'specRough':
                        self.specular.append(im)
                    if fname[2] == 'viewW':
                        self.viewdir.append(im)

    def __len__(self):
        return len(self.gt)

    def __getitem__(self, idx):
        env = torch.log(1.0 + self.envmap[idx % len(self.envmap)])
        # env = self.envmap[idx % len(self.envmap)]

        pos = self.position[idx]
        # alpha = self.alpha[idx]
        alpha = torch.sqrt(self.alpha[idx])
        pos = self.position[idx]
        diff = self.diffuse[idx]
        gt = self.gt[idx]
        normals = self.normals[idx]
        specular = self.specular[idx]
        viewdir = self.viewdir[idx]
        specular = torch.clamp(specular, min = 0.0, max = 10000.0)
        return alpha, pos, normals, viewdir, diff, specular, gt, env

print('Load training data.')
train_dataset = BufferDataset(train_path, envmap_trainpath)
print('Load validation data.')
val_dataset = BufferDataset(test_path, envmap_testpath)
print('Training data loaded.')
train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True) # , num_workers=10) # , pin_memory=True, drop_last=True)
val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=True) # , pin_memory=True, drop_last=True)

class HybridPixelNet(nn.Module):
    def __init__(self, width, num_layers, angles=128): # Retry this with alternate parametrization
        super(HybridPixelNet, self).__init__()
        self.decoder = nets.posNet(latentdim, angles * 3, 256, num_layers)
        self.pos_decoder = siren.SirenNet(dim_in=6, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = nets.EnvmapSGencoder(4096, 64)

    def forward(self, envmap, posnorm):
        lightcode = self.encoder(envmap).expand(posnorm.shape[0], -1)
        poscode_diff = self.pos_decoder(posnorm)
        return self.decoder(lightcode * poscode_diff)

net = HybridPixelNet(netwidth, num_layers, angles=num_angles).to(device=gpudevice)
print(net)

optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
writer = SummaryWriter(outputpath + '/runs/' + experiment_name)  # logloss-l1-adam-5e-3-testFaceNormals')
test_example = iter(val_dataloader)
iteration = 0

def render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap, gpudevice, net, timings=False):
    # flatten everything
    mask = (torch.sqrt(torch.sum(normals**2, dim=-1, keepdim=True)).expand(-1,-1,3) > 0.0)  # to make sure, remove background
    gtcpu[~mask] = 0.0
    mask = mask[:,:,0].view(-1)

    pos3d = pos3d.view(-1, 3)
    normals = normals.view(-1, 3)
    diffuse_bsdf_val = diffuse_bsdf_val.view(-1, 3)
    trans_nrj = alpha[:, :, 2].view(-1).unsqueeze(1).expand(-1, 3)
    diffuse_bsdf_val = diffuse_bsdf_val * (1.0 - trans_nrj)
    specular_bsdf_val = specular_bsdf_val.view(-1, 3)
    alpha = alpha[:, :, 0].view(-1)
    wi = wi.view(-1, 3)

    pos3d = pos3d[mask, :]
    pred = torch.zeros(pos3d.shape).to(device=gpudevice)
    normals = normals[mask, :]
    diffuse_bsdf_val = diffuse_bsdf_val[mask, :]
    specular_bsdf_val = specular_bsdf_val[mask, :]
    wi = wi[mask, :]
    alpha = alpha[mask]

    start = torch.cuda.Event(enable_timing=True)
    end = torch.cuda.Event(enable_timing=True)

    envinp = envmap.unsqueeze(0).to(device=gpudevice)
    netin = torch.cat((pos3d, normals), dim=1).to(device=gpudevice)
    if timings:
        start.record()

    num_directions = sampling_directions.shape[0]
    netout = net(envinp, netin).view(-1, num_directions, 3)
    # netout = envinp.view(-1, num_directions, 3)
    inc_light = torch.nn.functional.relu(netout)
    # print(inc_light.shape)


    if timings:
        end.record()
        torch.cuda.synchronize()
        print("Time to evaluate network:  ", start.elapsed_time(end))
    assert (inc_light != inc_light).any() == False, "Damn! inc_light contains NaNs"

    if timings:
        start.record()
    # SHADE DIFFUSE
    diffuseim = torch.zeros(pos3d.shape)
    mask_diffuse = (torch.sum(diffuse_bsdf_val * diffuse_bsdf_val, dim=-1, keepdim=False) > 0.0)
    diffnormals = normals[mask_diffuse, :].to(device=gpudevice)
    n_dot_l = []
    with torch.no_grad():
        for dir in range(num_directions):
            lightdir = sampling_directions[dir, :].view(1, 3).to(device=gpudevice)
            ndotl = torch.nn.functional.relu(sgr.dot(diffnormals, lightdir))
            n_dot_l.append(ndotl)
        n_dot_l = torch.stack(n_dot_l, dim=1)
    pred[mask_diffuse, :] += torch.sum(n_dot_l.detach() * inc_light[mask_diffuse, :,:], dim=1) * diffuse_bsdf_val[mask_diffuse, :].to(device=gpudevice)
    diffuseim = pred.cpu()

    # SHADE Specular
    mask_specular = (torch.sum(specular_bsdf_val * specular_bsdf_val, dim=-1, keepdim=False) > 0.0)
    specbrdf = []
    specim = torch.zeros(pos3d.shape)
    with torch.no_grad():
        n = normals[mask_specular, :].to(device=gpudevice)
        wo = wi[mask_specular, :].to(device=gpudevice)
        # n_dot_v = torch.sum(n * wo, dim=-1, keepdim=True).view(-1, 1)
        a2 = (alpha[mask_specular] * alpha[mask_specular]).view(-1, 1).to(device=gpudevice)
        specshade = specular_bsdf_val[mask_specular, :].to(device=gpudevice)

        # TODO : bandlimit the brdf
        # a2 = torch.clamp(a2, 0.025)

        for dir in range(num_directions):
            lightdir = sampling_directions[dir,:].view(1,3).to(device=gpudevice)
            hnorm = torch.sqrt(torch.sum((wo + lightdir)**2, dim=-1, keepdim=True))
            h = (wo + lightdir) / hnorm

            cosTheta = torch.sum(n * h, dim=-1, keepdim=True).view(-1, 1)
            n_dot_v = torch.sum(n * wo, dim=-1, keepdim=True).view(-1, 1)

            d = ((cosTheta * a2 - cosTheta) * cosTheta + 1.0)
            D = a2 / (d * d * np.pi)

            n_dot_l = torch.sum(n * lightdir, dim=-1, keepdim=True).view(-1, 1)
            woDotH = torch.sum(wo * h, dim=-1, keepdim=True).view(-1, 1)
            visible_l = (n_dot_l > 0)

            tanThetaSqr = torch.nn.functional.relu(n_dot_l)
            tanThetaSqr[visible_l] = torch.nn.functional.relu((1.0 - n_dot_l[visible_l] * n_dot_l[visible_l]) / (n_dot_l[visible_l] * n_dot_l[visible_l]))
            lambdaI = 0.5 * (-1 + torch.sqrt(1 + a2 * tanThetaSqr))
            tanThetaSqr2 = torch.nn.functional.relu((1.0 - n_dot_v * n_dot_v) / (n_dot_v * n_dot_v))
            lambdaO = 0.5 * (-1 + torch.sqrt(1 + a2 * tanThetaSqr2))
            G = 1 / ((1 + lambdaI) * (1 + lambdaO))

            F = specshade + (1.0 - specshade) * torch.nn.functional.relu(1 - woDotH)**5

            bsdf_spec = F * D * 0.25 / n_dot_v * visible_l * G

            # bsdf_spec = specshade * D *  torch.nn.functional.relu(n_dot_l)
            specbrdf.append(bsdf_spec)

        specbrdf = torch.stack(specbrdf, dim=1)

    pred[mask_specular, :] += torch.sum(specbrdf.detach() * inc_light[mask_specular,:,:], dim=1)

    specim = pred.cpu() - diffuseim
    if timings:
        end.record()
        torch.cuda.synchronize()
        print("Time to shade:  ", start.elapsed_time(end))

    # print("done here ")
    return pred * 4.0 * np.pi / num_angles, gtcpu.view(-1, 3)[mask, :], diffuseim* 4.0 * np.pi / num_angles, specim* 4.0 * np.pi / num_angles


for epoch in range(max_epochs):
    epoch_loss = 0.0

    for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in train_dataloader:
        iteration = iteration + 1
        optimizer.zero_grad()
        batch_loss = 0.0
        groundtruth, pred_render = [], []

        for b in range(alpha_batch.shape[0]):
            pred, gtcpu2, _, _ = render_from_buffers(alpha_batch[b], pos3d_batch[b], normals_batch[b], wi_batch[b],
                                                 diffuse_bsdf_val_batch[b], specular_bsdf_val_batch[b], gtcpu_batch[b],
                                                envmap_batch[b].permute(2,0,1),
                                                gpudevice, net, timings=False)

            groundtruth.append(gtcpu2)
            pred_render.append(pred)

        groundtruth = torch.cat(groundtruth, dim=0).to(gpudevice)
        pred_render = torch.cat(pred_render, dim=0)
        loss = sgr.my_loss(pred_render, groundtruth, tonemap=False, clip=False, log=logloss, L2=L2loss)

        batch_loss += loss.item()
        loss.backward()
        optimizer.step()
        writer.add_scalar('training loss', batch_loss, iteration)
        epoch_loss = epoch_loss + batch_loss / len(train_dataloader)

        with torch.no_grad():
            if iteration % tensorboard_writes == 1:
                try:
                    alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch = test_example.next()
                except StopIteration:
                    test_example = iter(val_dataloader)
                    alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch = test_example.next()

                alpha = alpha_batch[0]  # .to(gpudevice)
                pos3d = pos3d_batch[0]  # .to(gpudevice)
                normals = normals_batch[0]  # .to(gpudevice)
                wi = wi_batch[0]  # .to(gpudevice)
                diffuse_bsdf_val = diffuse_bsdf_val_batch[0]  # .to(gpudevice)
                specular_bsdf_val = specular_bsdf_val_batch[0]  # .to(gpudevice)
                gtcpu = gtcpu_batch[0]  # .to(gpudevice)
                envmap = envmap_batch[0].permute(2, 0, 1)  # .to(gpudevice)

                pred, gtcpu2, diffim, specim = render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap,
                        gpudevice, net, timings=True)

                alpha = alpha[:, :, 0].view(-1)
                mask = (torch.sqrt(torch.sum(normals ** 2, dim=-1, keepdim=False)) > 0.0).view(-1)
                pr = torch.zeros(gtcpu.size())
                pr = pr.view(-1, 3)
                pr[mask, :] = pred.cpu()
                pr = pr.view(gtcpu.shape)

                gtcpu = torch.zeros(normals.size())
                gtcpu = gtcpu.view(-1, 3)
                gtcpu[mask, :] = gtcpu2.cpu()
                gtcpu = gtcpu.view(normals.shape)

                diff = torch.zeros(normals.size())
                diff = diff.view(-1, 3)
                diff[mask, :] = diffim.cpu()
                diff = diff.view(normals.shape)
                spec = torch.zeros(normals.size())
                spec = spec.view(-1, 3)
                spec[mask, :] = specim.cpu()
                spec = spec.view(normals.shape)

                static_spec = torch.zeros(gtcpu.shape)
                static_spec = static_spec + specular_bsdf_val.cpu()
                static_spec = static_spec.permute(2, 0, 1)
                static_diff = torch.zeros(gtcpu.shape)
                static_diff = static_diff + diffuse_bsdf_val.cpu()
                static_diff = static_diff.permute(2, 0, 1)

                gtcpu = gtcpu.permute(2, 0, 1)
                pr = pr.permute(2, 0, 1)
                diff = diff.permute(2, 0, 1)
                spec = spec.permute(2, 0, 1)
                difference = torch.abs(pr - gtcpu)

                # print(gtcpu.shape, pr.shape, diff.shape)
                print('maxs and mins  ', gtcpu.max(), pr.max(), diff.max(), gtcpu.min(), pr.min(), diff.min())
                print('diffuse minmax : ', diff.min(), diff.mean(), diff.max())
                print('specular minmax : ', spec.min(), spec.mean(), spec.max())
                img_grid = torchvision.utils.make_grid([static_diff / static_diff.mean() * pr.mean(), static_spec / static_diff.mean() * pr.mean(), diff, spec, pr, gtcpu, difference])
                img_grid = img_grid / gtcpu.max()
                img_grid = img_grid ** (1.0 / 2.2)
                img_grid = torch.clip(1.2 * img_grid, 0.0, 1.0)

                writer.add_image('static vs predictions vs. GT', img_grid, global_step=iteration)

    writer.add_scalar('Epoch loss (training)', epoch_loss, epoch)

    with torch.no_grad():  # Loop over the test data:
        val_loss = 0.0
        for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in val_dataloader:
            pred_render, groundtruth, sharpprior,_ = render_from_buffers(alpha_batch[0], pos3d_batch[0], normals_batch[0], wi_batch[0],
                                                 diffuse_bsdf_val_batch[0], specular_bsdf_val_batch[0], gtcpu_batch[0],
                                                 envmap_batch[0].permute(2, 0, 1),
                                                 gpudevice, net,
                                                 timings=False)
            loss = sgr.my_loss(pred_render, groundtruth.to(gpudevice), tonemap=False, clip=False, log=logloss, L2=L2loss)
            val_loss += loss.item() / len(val_dataloader)

        writer.add_scalar('Epoch loss (validation)', val_loss, epoch)

        # TODO : save network and run it on the high-res testing images
        torch.save(net.state_dict(), outputpath + "/"+ experiment_name + "-net.pth")

print('Finished Training')