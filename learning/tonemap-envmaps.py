import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
import imageio
# imageio.plugins.freeimage.download() #Uncomment if the plugin to save EXR is not available on the system
import torch
from torchvision.utils import save_image


# data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/atelier/path'
data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/refpaths/atelier/path'
out_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/atelier/reference'
multiplier = 0.5 # 0.5 for atelier, 5 for kitchen, 5 bedroom, 10 for sanmiguel

data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/realtime'
out_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/sanmiguel/realtime-pathtracing'
data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/refpaths/sanmiguel/path'
out_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/sanmiguel/reference'
multiplier = 10 # 0.5 for atelier, 5 for kitchen, 5 bedroom, 10 for sanmiguel

data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/bedroom/path-alias'
out_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/bedroom/reference'
multiplier = 5 # 0.5 for atelier, 5 for kitchen, 5 bedroom, 10 for sanmiguel

# # data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/kitchen/path'
# data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/refpaths/kitchen/path'
# out_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/kitchen/reference'
# multiplier = 5 # 0.5 for atelier, 5 for kitchen, 5 bedroom, 10 for sanmiguel




data_dir = 'F:/gilles/paperresults/ios-denoised/kitchen/path-denoised'
out_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/kitchen/realtime-denoised'
multiplier = 20 # 1.4 for denoised atelier, 10 for kitchen, 3.0 bedroom, 10 for sanmiguel



# data_dir = 'F:/gilles/paperresults/results/results/atelier/dir_spp_01_accum_05'
# out_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/atelier/NRC-cache'
# # data_dir = 'F:/gilles/paperresults/results/results/atelier/nrc_spp_01_accum_05'
# # out_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/atelier/NRC-5spp'
# multiplier = 0.5 # 0.5 for atelier, 5 for kitchen, 5 bedroom, 10 for sanmiguel

# data_dir = 'F:/gilles/paperresults/results/results/bedroom/dir_spp_01_accum_05'
# out_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/bedroom/NRC-cache'
# data_dir = 'F:/gilles/paperresults/results/results/bedroom/nrc_spp_01_accum_05'
# out_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/bedroom/NRC-5spp'
# multiplier = 5 # 0.5 for atelier, 5 for kitchen, 5 bedroom, 10 for sanmiguel




# data_dir = 'C:/Users/grainer/Downloads/results/results/bedroom/nrc_spp_01_accum_05'
# out_dir = 'C:/Users/grainer/Downloads/results/results/bedroom/nrc_spp_01_accum_05'




# data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/path'
# out_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/pathpng'

# data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/bedroom/path'
# out_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/bedroom/pathpng'
#
# data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/path'
# out_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/path-png'
# data_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/sanmiguel/2predictions-noalbedo-withreflect'
# out_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/sanmiguel/2predictions-noalbedo-withreflect'
#
# data_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/sanmiguel/naivebaseline-withreflect'
# out_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/sanmiguel/naivebaseline-withreflect'
#
#
# data_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/sanmiguel/splitnets'
# out_dir  = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/sanmiguel/splitnets'
#
# data_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/kitchen2/2predictions'
# out_dir  = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/kitchen2/2predictions'
#
# data_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/atelier/NRC-10spp'
# out_dir  = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/atelier/NRC-10spp'


# data_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/atelier/baseline-linearout----bats-3-lat-64-layer-2-width-256-lr-0.0001'
# out_dir = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/atelier/baseline-linearout----bats-3-lat-64-layer-2-width-256-lr-0.0001'
# data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/kitchen2/path'
# out_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/kitchen2/path-png'

# multiplier = 5 # 0.5 for atelier, 5 for kitchen, 5 bedroom, 10 for sanmiguel

if not os.path.exists(out_dir):
    os.makedirs(out_dir)
filenames = []
counter = 0
for r, d, f in os.walk(data_dir):  # r=root, d=directories, f = files
    f.sort()
    for file in f:
        # if 'AccumulatePass.output.0.exr' in file:
        if 'Denoiser.output.0.exr' in file:
        # if '.exr' in file:
            filenames.append(file)
            im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
            pr = torch.from_numpy(im)
            pr = pr.permute(2, 0, 1)
            pr = torch.clip(multiplier * pr, 0.0, 1.0)
            pr = pr ** (1.0 / 2.2)

            save_image(pr, out_dir + "/" + str(counter).zfill(8) + ".png")

            counter+=1
