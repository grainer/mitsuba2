# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import torch

# import mitsuba
# mitsuba.variants()
#
# mitsuba.set_variant('gpu_autodiff_rgb')
# from mitsuba.core import Float, Vector3f
# print('lol')
#
# import enoki as ek
# import mitsuba
#
# # Set the desired mitsuba variant
# mitsuba.set_variant('packet_rgb')
#
# from mitsuba.core import Float, Vector3f
# from mitsuba.core.xml import load_string
# from mitsuba.render import SurfaceInteraction3f, BSDFContext
#
#
# def sph_dir(theta, phi):
#     """ Map spherical to Euclidean coordinates """
#     st, ct = ek.sincos(theta)
#     sp, cp = ek.sincos(phi)
#     return Vector3f(cp*st, sp*st, ct)
#
#
# # Load desired BSDF plugin
# # bsdf = load_string("""<bsdf version='2.0.0' type='roughconductor'>
# #                           <float name="alpha" value="0.2"/>
# #                           <string name="distribution" value="ggx"/>
# #                       </bsdf>""")
#
# # Create a (dummy) surface interaction to use for the evaluation
# si = SurfaceInteraction3f()
#
# # Specify an incident direction with 45 degrees elevation
# si.wi = sph_dir(ek.pi * 45 / 180, 0.0)
#
# # Create grid in spherical coordinates and map it onto the sphere
# res = 300
# theta_o, phi_o = ek.meshgrid(
#     ek.linspace(Float, 0,     ek.pi,     res),
#     ek.linspace(Float, 0, 2 * ek.pi, 2 * res)
# )
# wo = sph_dir(theta_o, phi_o)
#
# # Evaluate the whole array (18000 directions) at once
# values = bsdf.eval(BSDFContext(), si, wo)
#
# def print_hi(name):
#     # Use a breakpoint in the code line below to debug your script.
#     print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.
#
#
# # Press the green button in the gutter to run the script.
# if __name__ == '__main__':
#     print_hi('PyCharm')
#
# # See PyCharm help at https://www.jetbrains.com/help/pycharm/
import torch

import os
import numpy as np
import mitsuba

mitsuba.set_variant('gpu_autodiff_rgb')
from mitsuba.core import Bitmap, Struct, Thread
from mitsuba.core.xml import load_file

filename = 'cbox.xml'

Thread.thread().file_resolver().append(os.path.dirname(filename))

scene = load_file(filename)

scene.integrator().render(scene, scene.sensors()[0])

film = scene.sensors()[0].film()

film.set_destination_file('output.exr')
film.develop()

bmp = film.bitmap(raw=True)
bmp.convert(Bitmap.PixelFormat.RGB, Struct.Type.UInt8, srgb_gamma=True).write('output.jpg')

bmp_linear_rgb = bmp.convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
image_np = np.array(bmp_linear_rgb)
print(image_np.shape)