import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
import imageio
# imageio.plugins.freeimage.download() #Uncomment if the plugin to save EXR is not available on the system

# out_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/indoorenvs/rotate'
# envmap_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/assets/scene2-atelier/indoors.exr'
#
out_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/rotate'
envmap_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/rotate.exr'


# out_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/indoorenvs/rotate'
# envmap_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/indoorenvs/rotate.exr'

envmap = cv2.cvtColor(cv2.imread(envmap_path, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
envmap = cv2.resize(envmap, (256, 128))
envmap = envmap / np.mean(envmap)

counter = 0
for i in range(200):
    imageio.imwrite(out_dir + "/env-" + str(counter).zfill(5) + ".exr", envmap)
    counter = counter +1

for az in range(200):
    azimuth = int(az / 200.0 * envmap.shape[1])
    env2 = np.zeros(envmap.shape, dtype=np.float32)
    env2[:, 0:envmap.shape[1] - azimuth, :] = envmap[:, azimuth:envmap.shape[1], :]
    env2[:, envmap.shape[1] - azimuth:envmap.shape[1], :] = envmap[:, 0:azimuth, :]
    imageio.imwrite(out_dir + "/env-" + str(counter).zfill(5) + ".exr", env2)
    counter = counter + 1

for az in range(200):
    azimuth = int(az / 200.0 * envmap.shape[1])
    env2 = np.zeros(envmap.shape, dtype=np.float32)
    env2[:, 0:envmap.shape[1] - azimuth, :] = envmap[:, azimuth:envmap.shape[1], :]
    env2[:, envmap.shape[1] - azimuth:envmap.shape[1], :] = envmap[:, 0:azimuth, :]
    imageio.imwrite(out_dir + "/env-" + str(counter).zfill(5) + ".exr", env2)
    counter = counter + 1

# for i in range(200):
#     imageio.imwrite(out_dir + "/env-" + str(counter).zfill(5) + ".exr", env2)
#     counter = counter +1