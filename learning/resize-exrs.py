import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
import imageio
# imageio.plugins.freeimage.download() #Uncomment if the plugin to save EXR is not available on the system

data_dir = 'F:/gilles/data/envmaps/IndoorHDRDataset2018'
# out_dir = 'F:/gilles/data/envmaps/IndoorHDRDataset2018-normalized'
out_dir = 'F:/gilles/data/envmaps/indoor_train'
new_size = (256, 128)


if not os.path.exists(out_dir):
    os.makedirs(out_dir)
c = 0
for r, d, f in os.walk(data_dir):  # r=root, d=directories, f = files
    for file in f:
        if '.exr' in file:
            if c % 50 == 0:
                print('Read ', c, ' EXR files.')
            # print('reading ', file)
            im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
            print(im.dtype, np.min(im), np.max(im), np.mean(im))
            im = cv2.resize(im, new_size)
            im = im / np.mean(im)
            # print(im.dtype, np.min(im), np.max(im), np.mean(im))
            c += 1
            # cv2.imwrite(out_dir + "/" + file, im) #somehow OpenCV saves in LDR not HDR
            # imageio.imwrite(out_dir + "/envmap-" + str(c).zfill(5) + ".exr", im)
            imageio.imwrite(out_dir + "/" + file, im)