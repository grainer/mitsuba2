import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
import imageio
# imageio.plugins.freeimage.download() #Uncomment if the plugin to save EXR is not available on the system
import torch
from torchvision.utils import save_image


data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/atelier/pathAA'
data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/bedroom/pathAA'

def averageIm(inp):
    out = np.zeros((int(inp.shape[0] / 2), int(inp.shape[1] / 2), 3))
    out = np.stack((inp[0::2, 0::2, :], inp[0::2, 1::2, :], inp[1::2, 0::2, :], inp[1::2, 1::2, :]))
    return np.mean(out, axis=0)

def medianIm(inp):
    out = np.zeros((int(inp.shape[0] / 2), int(inp.shape[1] / 2), 3))
    out = np.stack((inp[0::2, 0::2, :], inp[0::2, 1::2, :], inp[1::2, 0::2, :], inp[1::2, 1::2, :]))
    return np.median(out, axis=0)

def medianVector(inp):
    out = np.zeros((int(inp.shape[0] / 2), int(inp.shape[1] / 2), 3))
    samples = np.stack((inp[0::2, 0::2, :], inp[0::2, 1::2, :], inp[1::2, 0::2, :], inp[1::2, 1::2, :]))
    m = np.mean(samples, axis=0)

    dist1 = np.sum((inp[0::2, 0::2, :] - m)**2, axis=2)
    out = inp[0::2, 0::2, :]

    temp = inp[0::2, 1::2, :]
    dist2 = np.sum((temp - m) ** 2, axis=2)
    out[(dist2 < dist1), :] = temp[(dist2 < dist1), :]
    dist1 = np.minimum(dist1, dist2)

    temp = inp[1::2, 0::2, :]
    dist2 = np.sum((temp - m) ** 2, axis=2)
    out[(dist2 < dist1), :] = temp[(dist2 < dist1), :]
    dist1 = np.minimum(dist1, dist2)

    temp = inp[1::2, 1::2, :]
    dist2 = np.sum((temp - m) ** 2, axis=2)
    out[(dist2 < dist1), :] = temp[(dist2 < dist1), :]
    return out


for r, d, f in os.walk(data_dir):  # r=root, d=directories, f = files
    f.sort()
    for file in f:
        # if 'AccumulatePass.output.0.exr' in file:
        # if 'Denoiser.output.0.exr' in file:
        if '.exr' in file:
            # filenames.append(file)
            print(data_dir + "/" + file)
            im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)


            # downim = cv2.pyrDown(im)

            # downim = cv2.resize(im, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_NEAREST)


            if 'pos' in file:
                # im = cv2.medianBlur(im, 1)
                # downim = averageIm(im)
                # downim = medianVector(im)

                # downim = medianVector(im)

                downim = cv2.resize(im, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_LINEAR_EXACT)
                # im = cv2.medianBlur(im, 1)
                # downim = cv2.resize(im, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_NEAREST)
            elif 'norm' in file:
                # im = cv2.medianBlur(im, 1)
                # downim = medianVector(im)
                downim = cv2.resize(im, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_LINEAR_EXACT)

                # downim = cv2.resize(im, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_NEAREST)
            # elif 'alpha' in file:
            #     im = cv2.medianBlur(im, 1)
            #     downim = cv2.resize(im, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_NEAREST)
            else:
                # im = cv2.medianBlur(im, 1)
                # downim = cv2.resize(im, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_NEAREST)
                # downim = averageIm(im)
                downim = cv2.resize(im, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_LINEAR_EXACT)
                #

            # # # Normalize if normal or view direction
            # if 'pos' in file:
            #     downim = medianIm(im)
            # elif 'norm' in file:
            #     downim = medianIm(im)
            # else:
            #     downim = averageIm(im)


            # im = cv2.medianBlur(im, 1)

            # downim = medianIm(im)# cv2.resize(im, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_NEAREST)
            # downim = averageIm(im)# cv2.resize(im, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_NEAREST)

            if 'norm' in file or 'view' in file:
                print('normalizing', file, downim.shape)
                normim = np.sqrt(np.sum(downim ** 2, axis=2))
                normim[normim == 0.0] = 1.0
                print(np.min(normim), np.max(normim), normim.shape)
                downim = downim / normim[:, :, np.newaxis]
                normim = np.sum(downim ** 2, axis=2)
                print(np.min(normim), np.max(normim))

            # plt.imshow(downim)
            # plt.show()
            imageio.imwrite(data_dir + "-downsampled/" + file, downim)



