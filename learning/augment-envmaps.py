import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
import imageio
# imageio.plugins.freeimage.download() #Uncomment if the plugin to save EXR is not available on the system

data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/envmaps/outdoor-test'
out_dir  = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/test'
augment = True
# data_dir = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/nprt/data/envmaps/indoor-test'
# out_dir  = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/nprt/data/scenes/envmaps/test'
# augment = False


start_idx = 0
end_idx = 200# 10000

new_size = (256, 128)

width = new_size[0]
if not os.path.exists(out_dir):
    os.makedirs(out_dir)
c = start_idx
filenames = []
for r, d, f in os.walk(data_dir):  # r=root, d=directories, f = files
    for file in f:
        if '.exr' in file:
            filenames.append(file)

while c < end_idx:
    if c % 50 == 0:
        print('Read ', c, ' EXR files.')
    # print('reading ', file)
    im = cv2.cvtColor(cv2.imread(os.path.join(r, filenames[c%len(filenames)]), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
    print(im.dtype, np.min(im), np.max(im), np.mean(im))

    im = cv2.resize(im, new_size)
    im = im / np.mean(im)
    rota = int(1 + (im.shape[1]-2) * np.random.rand(1))
    im2 = np.zeros(im.shape, dtype=np.float32)

    if augment:
        im2[:, 0:width - rota, :] = im[:, rota:width, :]
        im2[:, width - rota:width, :] = im[:, 0:rota, :]
    else:
        im2 = im

    # rgbperm = torch.randperm(3)
    # im2[:, :, 0:width - rota] = im[rgbperm, :, rota:width]
    # im2[:, :, width - rota:width] = im[rgbperm, :, 0:rota]

    # cv2.imwrite(out_dir + "/" + file, im) #somehow OpenCV saves in LDR not HDR
    imageio.imwrite(out_dir + "/env-" + str(c).zfill(5) + ".exr", im2)
    c += 1
