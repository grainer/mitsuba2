import matplotlib.pyplot as plt
import csv
import numpy as np
# import configargparse
import cv2
import os



multiplier = 5.0
data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/bedroom' # 110
inset_1 = [10, 229] # [26, 224]
inset_2 = [112, 355]




#
#
# data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/atelier' # 110
# multiplier = 0.5
# inset_1 = [22, 270] # [26, 224]
# inset_2 = [412, 372]



#
# data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/nrc/atelier' # 110
# multiplier = 0.5
# inset_1 = [96, 92] # [26, 224]
# inset_2 = [150, 253]
#
#
#
# data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/nrc/bedroom' # 110
# multiplier = 5.0
# inset_1 = [110, 339] # [26, 224]
# inset_2 = [393, 405]


#
# data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/denoising/kitchen'
# multiplier = 10.0
# inset_1 = [110, 339] # [26, 224]
# inset_2 = [393, 405]
# denoise_multiplier = 20.0



#
inset_width = 64
linew = 2
out_dir = data_dir
ims = []
insets_1 = []
insets_2 = []
for r, d, f in os.walk(data_dir):  # r=root, d=directories, f = files
    f.sort()
    for file in f:
        # if 'AccumulatePass.output.0.exr' in file:
        # if 'Denoiser.output.0.exr' in file:
        if '.exr' in file:
            im = cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)

            if 'denoise' in file:
                im = np.clip(denoise_multiplier * im, 0.0, 1.0) ** (1.0 / 2.2) * 255
            else:
                im = np.clip(multiplier * im, 0.0, 1.0) ** (1.0 / 2.2) * 255

            cv2.rectangle(im, (int(inset_1[0]), int(inset_1[1])), (int(inset_1[0])+inset_width, int(inset_1[1])+inset_width), (31, 42,220), linew, lineType=cv2.FILLED)
            cv2.rectangle(im, (int(inset_2[0]), int(inset_2[1])), (int(inset_2[0])+inset_width, int(inset_2[1])+inset_width), (232,160,56), linew, lineType=cv2.FILLED)

            ins_1=im[int(inset_1[1]):int(inset_1[1])+inset_width, int(inset_1[0]):int(inset_1[0])+inset_width]
            ins_2=im[int(inset_2[1]):int(inset_2[1])+inset_width, int(inset_2[0]):int(inset_2[0])+inset_width]

            ins_1 = cv2.resize(ins_1, (0, 0), fx=4, fy=4, interpolation=cv2.INTER_NEAREST)
            ins_2 = cv2.resize(ins_2, (0, 0), fx=4, fy=4, interpolation=cv2.INTER_NEAREST)

            fullim = np.concatenate( (im, np.concatenate((ins_1, ins_2), axis=1)), axis=0)
            ims.append(fullim)


cv2.imwrite(out_dir + '/fullfig.png', np.concatenate(ims, axis=1) )





# ours = cv2.imread(conf.ours_path[0], cv2.IMREAD_COLOR)
# uni = cv2.imread(conf.uni_path[0], cv2.IMREAD_COLOR)
# gt = cv2.imread(conf.gt_path[0], cv2.IMREAD_COLOR)
#
#
# ours_inset_1 = ours[int(conf.inset_1[1]):int(conf.inset_1[1])+int(conf.inset_1[3]), int(conf.inset_1[0]):int(conf.inset_1[0])+int(conf.inset_1[2])]
# ours_inset_2 = ours[int(conf.inset_2[1]):int(conf.inset_2[1])+int(conf.inset_2[3]), int(conf.inset_2[0]):int(conf.inset_2[0])+int(conf.inset_2[2])]
#
# uni_inset_1 = uni[int(conf.inset_1[1]):int(conf.inset_1[1])+int(conf.inset_1[3]), int(conf.inset_1[0]):int(conf.inset_1[0])+int(conf.inset_1[2])]
# uni_inset_2 = uni[int(conf.inset_2[1]):int(conf.inset_2[1])+int(conf.inset_2[3]), int(conf.inset_2[0]):int(conf.inset_2[0])+int(conf.inset_2[2])]
#
# gt_inset_1 = gt[int(conf.inset_1[1]):int(conf.inset_1[1])+int(conf.inset_1[3]), int(conf.inset_1[0]):int(conf.inset_1[0])+int(conf.inset_1[2])]
# gt_inset_2 = gt[int(conf.inset_2[1]):int(conf.inset_2[1])+int(conf.inset_2[3]), int(conf.inset_2[0]):int(conf.inset_2[0])+int(conf.inset_2[2])]
#
# cv2.rectangle(ours, (int(conf.inset_1[0]), int(conf.inset_1[1])), (int(conf.inset_1[0])+int(conf.inset_1[2]), int(conf.inset_1[1])+int(conf.inset_1[3])), (31,42,220), 5, lineType=cv2.FILLED)
# cv2.rectangle(ours, (int(conf.inset_2[0]), int(conf.inset_2[1])), (int(conf.inset_2[0])+int(conf.inset_2[2]), int(conf.inset_2[1])+int(conf.inset_2[3])), (232,160,56), 5, lineType=cv2.FILLED)
#
# cv2.rectangle(uni, (int(conf.inset_1[0]), int(conf.inset_1[1])), (int(conf.inset_1[0])+int(conf.inset_1[2]), int(conf.inset_1[1])+int(conf.inset_1[3])), (31,42,220), 5, lineType=cv2.FILLED)
# cv2.rectangle(uni, (int(conf.inset_2[0]), int(conf.inset_2[1])), (int(conf.inset_2[0])+int(conf.inset_2[2]), int(conf.inset_2[1])+int(conf.inset_2[3])), (232,160,56), 5, lineType=cv2.FILLED)
#
# cv2.rectangle(gt, (int(conf.inset_1[0]), int(conf.inset_1[1])), (int(conf.inset_1[0])+int(conf.inset_1[2]), int(conf.inset_1[1])+int(conf.inset_1[3])), (31,42,220), 5, lineType=cv2.FILLED)
# cv2.rectangle(gt, (int(conf.inset_2[0]), int(conf.inset_2[1])), (int(conf.inset_2[0])+int(conf.inset_2[2]), int(conf.inset_2[1])+int(conf.inset_2[3])), (232,160,56), 5, lineType=cv2.FILLED)
