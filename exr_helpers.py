import numpy as np
import OpenEXR
import Imath
import torch
import matplotlib.pyplot as plt

# ALL FUNCTIONS RETURN TORCH TENSORS BY DEFAULT FOR SIMPLICITY IN THE MAIN CODE

def readEXR_RGB(filepath, astorch=True):
    file = OpenEXR.InputFile(filepath)
    header = file.header()
    dw = header['dataWindow']
    imsize = (dw.max.y - dw.min.y + 1, dw.max.x - dw.min.x + 1)
    (r, g, b) = file.channels("RGB")

    if header['channels']['R'] == Imath.Channel(Imath.PixelType(OpenEXR.HALF)):
        filetype = np.float16

    if header['channels']['R'] == Imath.Channel(Imath.PixelType(OpenEXR.FLOAT)):
        filetype = np.float32

    im = np.zeros((imsize[0], imsize[1], 3), dtype=np.float32)
    rnp = np.reshape(np.frombuffer(r, dtype=filetype), imsize)
    gnp = np.reshape(np.frombuffer(g, dtype=filetype), imsize)
    bnp = np.reshape(np.frombuffer(b, dtype=filetype), imsize)
    im[:, :, 0] = rnp.astype(np.float32)
    im[:, :, 1] = gnp.astype(np.float32)
    im[:, :, 2] = bnp.astype(np.float32)

    if astorch:
        return torch.from_numpy(im)
    else:
        return im

def writeEXR_RGB(im, savepath):
    im32 = np.float32(im)
    (r, g, b) = [im32[:, :, c].tostring() for c in range(0, 3)]
    # print(im32.shape, im32.dtype)
    out = OpenEXR.OutputFile(savepath, OpenEXR.Header(im32.shape[1], im32.shape[0]))
    out.writePixels({'R': r, 'G': g, 'B': b})

def displayEXR(im):
    print(im.size())
    im2 = torch.clip(im, 0.0, 1.0)**(1.0/2.2)
    plt.imshow(im2)
    plt.show()
    # plt.draw()
    # plt.show(block=False)

def to_gpu_var(t):
    return torch.autograd.Variable(t, requires_grad=True).cuda()

def to_gpu_leaf_var(t):
    return torch.autograd.Variable( t.cuda().detach(), requires_grad=True)