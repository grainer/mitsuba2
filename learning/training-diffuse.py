import os
import numpy as np
import enoki as ek


import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter



import mitsuba
# Set the desired mitsuba variant (MUST BE THE SAME IN ALL PYTHON FILES !!!)
import nets

mitsuba.set_variant('gpu_rgb')
from mitsuba.core import Float, UInt32, Vector2f, Vector3f, Thread, Frame3f, xml, ScalarTransform4f, Bitmap, Struct
from mitsuba.core.xml import load_file, load_dict
from mitsuba.render import (BSDF, BSDFContext, BSDFFlags,
                            BSDFSample3f, SurfaceInteraction3f, register_bsdf,
                            DirectionSample3f, Emitter, ImageBlock,
                            SamplingIntegrator, has_flag,
                            register_integrator, reflect)

import renderhelpers as sgr
import matplotlib.pyplot as plt

torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True


# Input paths and parameters ===========================================================================================
scenepath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/bunny-plane'
# outputpath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/results/learning-bunny/diffusebunny'
outputpath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/results/directionallight/firsttests'

# scenepath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/bunny-plane-alienware'
# outputpath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/results/learning-bunny/diffusebunny-alienware'
scenename_train = 'diffuse-gt.xml'
scenename_gt = 'diffuse-gt.xml'

# Training parameters
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
max_epochs = 10000
numSGs = 12#  25#      50#
netwidth = 128#  250#     64#
batch_size = 0
num_layers = 3#  4#    1#     5# 3#
learning_rate = 0.001#   0.0001#   0.005#   0.01# 0.5#
logloss = True#    False#
L2loss = False#   True#
exponentiate = False#
normalize = False#  True#
rgbprior = False# True#
kldiv = False#  True
n2ntonemap = False# True#

pos_encoding = True# False#
num_freqs=0

experiment_name = 'diffus-posenc-ennis-newcodemylossk-noposencode'

# experiment_name = 'debug-SGD-exponentiate-'  # 'ennis-addRGBprior-'  n2n-correctn2nprior0.1-
# experiment_name = experiment_name +'-batchsize-' +str(batch_size) +'-layers-' +str(num_layers) +'-width-' +str(netwidth) \
#                   +'-numSG-'+str(numSGs) +'-lr-'+str(learning_rate) +'-log-'+str(int(logloss)) +'-L2-'+str(int(L2loss)) \
#                     + '-posenc-' +str(int(pos_encoding))
                  # +'-kldiv-'+str(int(kldiv)) +'-priors-'+str(int(rgbprior)) +


# Other parameters (hardcoded)
spp = 16 # 32#
hybridanis = False#
anisowarp = True#   False
sharpness_thresh = 5 #for the anisotropic NDF warp
cos_sharpness = 2.13
cos_amplitude = 1.17

if not os.path.exists(outputpath):
    os.makedirs(outputpath)
# Load the scene XMLs
Thread.thread().file_resolver().append(scenepath)
scene_gt = load_file(scenepath+'/'+scenename_gt, spp=spp)
scene_train = load_file(scenepath+'/'+scenename_train, spp=spp)

# Setup NN, optimizer, tensorboard etc. ===================================================================================
if pos_encoding:
    net = nets.posEncodingNet(numSGs, netwidth, num_layers, num_freqs=num_freqs).to(device=gpudevice)
else:
    net = nets.posNet(numSGs, netwidth, num_layers).to(device=gpudevice)

sg_centers = sgr.initialiseSGcenters(numSGs).to(device=gpudevice)
# optimizer = torch.optim.SGD(net.parameters(), lr=learning_rate)
optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
writer = SummaryWriter(outputpath +'/runs/' + experiment_name) # logloss-l1-adam-5e-3-testFaceNormals')

klloss = torch.nn.KLDivLoss()
running_loss = 0.0
for epoch in range(max_epochs):
    optimizer.zero_grad()

    # Randomly set the camera position and render the GT scene
    randomsensor = sgr.generate_random_sensor(spp, 400, 400)
    if epoch % 251 == 0: #once in a while render the original view, useful for comparisons for meetings/summaries
        randomsensor = scene_gt.sensors()[0]

    scene_gt.integrator().render(scene_gt, randomsensor)
    groundtruth = torch.from_numpy(np.array(randomsensor.film().bitmap(raw=True).convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False))).to(device=gpudevice) # this conversion to torch doesn't work
    groundtruthshape = groundtruth.shape
    print(groundtruthshape)
    # film.bitmap(raw=True).convert(Bitmap.PixelFormat.RGB, Struct.Type.UInt8, srgb_gamma=True)

    # # Custom rendering pipeline in Python (from Mitsuba2 doc)
    # film = randomsensor.film()
    # sampler = randomsensor.sampler()
    # film_size = film.crop_size()
    # total_sample_count = ek.hprod(film_size)
    # if sampler.wavefront_size() != total_sample_count:
    #     sampler.seed(0, total_sample_count) #deactivate all the random samples ???
    #
    # # Enumerate discrete sample & pixel indices, and uniformly sample positions within each pixel.
    # pos = ek.arange(UInt32, total_sample_count)
    # # pos //= spp
    # scale = Vector2f(1.0 / film_size[0], 1.0 / film_size[1])
    # pos = Vector2f(Float(pos % int(film_size[0])), Float(pos // int(film_size[0])))
    # # pos += sampler.next_2d() # try deactivating this (no random position within the pixel but only center...)
    #
    # # Sample rays starting from the camera sensor
    # rays, weights = randomsensor.sample_ray_differential(time=0, sample1=sampler.next_1d(), sample2=pos*scale, sample3=0)
    #
    # # Intersect rays with the scene geometry
    # si = scene_train.ray_intersect(rays)
    # active_b = si.is_valid() & (Frame3f.cos_theta(si.wi) > 0.0)
    # ctx = BSDFContext()
    # bsdf = si.bsdf(rays)
    #
    # # world positions of intersections
    # mask = active_b.torch().bool()
    # pos3d = si.p.torch()[mask,:].unsqueeze_(1).view(-1,3)
    # print('position ', pos3d.min(), pos3d.max())



    mask, pos3d, normals, bsdf_val = sgr.get_buffers(scene_train, randomsensor)




    # net_output = net(pos3d)
    sglights = sgr.deparametrize_torch(net(pos3d).unsqueeze(2).view(-1,numSGs,6), sg_centers, exponentiate)

    # normals = si.to_world(Vector3f(0, 0, 1)).torch()[mask, :]
    # bsdf_val = BSDF.eval_vec(bsdf, ctx, si, Vector3f([0.0, 0.0, 1.0]), active_b).torch()[mask, :]
    irr = torch.zeros(pos3d.shape[0], 3).to(device=gpudevice)
    for l in range(numSGs):
        irr = irr + sgr.exact_cosSGinnerProduct(normals, sglights[:, l, 0:3], sglights[:, l, 3], sglights[:, l, 4:7])
        # irr = irr + sgr.stephen_hill_irradiance(normals, sglights[:, l, 0:3], sglights[:, l, 3], sglights[:, l, 4:7])
    pred_render = torch.nn.functional.relu(bsdf_val * irr)
    groundtruth = groundtruth.view(1, groundtruth.shape[0]*groundtruth.shape[1],3)

    # DEBUG FOR NaNs ===================================================================================================
    assert (mask != mask).any() == False, "Damn! mask contains NaNs"
    assert (sglights != sglights).any() == False, "Damn! sglights contains NaNs"
    assert (pos3d != pos3d).any() == False, "Damn! pos3d contains NaNs"
    assert (irr != irr).any() == False, "Damn! ndf_val contains NaNs"
    assert (pred_render != pred_render).any() == False, "Damn! pred_render contains NaNs"
    # assert (bsdf_val[mask, :] != bsdf_val[mask, :]).any() == False, "Damn! bsdf_val contains NaNs"
    # ==================================================================================================================

    loss = sgr.my_loss(pred_render, groundtruth.squeeze()[mask, :], tonemap=False, clip=False, log=logloss, L2=L2loss)
    # if kldiv:
    #     std = groundtruth.squeeze()[mask, :].std()
    #     # normalize the KL div but not the L1 loss
    #
    #     gtprob = groundtruth.squeeze()[mask, :]
    #     m = gtprob.min()
    #     s = torch.sum(gtprob)
    #     gtprob = gtprob - m
    #     gtprob = gtprob /s
    #
    #     loss = klloss(torch.log(torch.nn.functional.relu( (pred_render-m)/s ) + 1.0), gtprob) + torch.abs(pred_render - groundtruth.squeeze()[mask, :]).mean()
        # loss = klloss(torch.log(torch.nn.functional.relu(pred_render/ std) + 1.0), groundtruth.squeeze()[mask, :] / std) + ((pred_render/std - groundtruth.squeeze()[mask, :]/std) ** 2).mean()
        # loss = klloss(torch.log(torch.nn.functional.relu(pred_render) + 1.0), groundtruth.squeeze()[mask, :]) + ((pred_render - groundtruth.squeeze()[mask, :]) ** 2).mean()
        #+ torch.abs(pred_render - groundtruth.squeeze()[mask, :]).mean()/std

    # if logloss:
    #     if L2loss:
    #         if normalize:
    #             std = torch.log(groundtruth.squeeze()[mask, :] + 1.0).std()
    #             loss = ((torch.log(torch.nn.functional.relu(pred_render) + 1.0) / std - torch.log(
    #                 groundtruth.squeeze()[mask, :] + 1.0) / std) ** 2).mean()
    #         else:
    #             loss = ((torch.log(torch.nn.functional.relu(pred_render) + 1.0) - torch.log(
    #                 groundtruth.squeeze()[mask, :] + 1.0)) ** 2).mean()
    #     else:
    #         if normalize:
    #             std = torch.log(groundtruth.squeeze()[mask, :] + 1.0).std()
    #             loss = torch.abs(torch.log(torch.nn.functional.relu(pred_render) + 1.0) - torch.log(
    #                 groundtruth.squeeze()[mask, :] + 1.0)).mean() / std
    #         else:
    #             loss = torch.abs(torch.log(torch.nn.functional.relu(pred_render) +1.0) - torch.log(groundtruth.squeeze()[mask, :] +1.0)).mean()
    # else:
    #     if n2ntonemap:
    #         if L2loss:
    #             # loss = (( pred_render/(pred_render+1.0) - groundtruth.squeeze()[mask, :]/(groundtruth.squeeze()[mask, :]+1.0) ) ** 2).mean()
    #             # loss = ( (pred_render - groundtruth.squeeze()[mask, :])**2 / (pred_render + 1.0) ).mean()
    #             loss = (((pred_render/(pred_render+1.0))**(1.0/2.2) - (groundtruth.squeeze()[mask, :]/(groundtruth.squeeze()[mask, :]+1.0))**(1.0/2.2))**2).mean()
    #             # loss = torch.abs((pred_render / (pred_render + 1.0)) ** (1.0 / 2.2) - (
    #             #             groundtruth.squeeze()[mask, :] / (groundtruth.squeeze()[mask, :] + 1.0)) ** (
    #             #                              1.0 / 2.2)).mean()
    #         else:
    #             loss = torch.abs( pred_render/(pred_render+1.0) - groundtruth.squeeze()[mask, :]/(groundtruth.squeeze()[mask, :]+1.0) ).mean()
    #             # loss = torch.abs( (pred_render / (pred_render + 1.0))**2 - (groundtruth.squeeze()[mask, :] / (
    #             #             groundtruth.squeeze()[mask, :] + 1.0))**2 ).mean()
    #             # loss = torch.abs( torch.log(1.0 + pred_render / (pred_render + 1.0)) - torch.log(1.0 + groundtruth.squeeze()[mask, :] / (
    #             #             groundtruth.squeeze()[mask, :] + 1.0)) ).mean()
    #     else:
    #         if L2loss:
    #             loss = ((pred_render - groundtruth.squeeze()[mask, :]) ** 2).mean()
    #         else:
    #             loss = torch.abs(pred_render - groundtruth.squeeze()[mask, :]).mean()
    #
    #
    # if rgbprior:
    #     loss = loss + 0.1 * ( (1.0 - sglights[:, :, 0:3]/(1.0+sglights[:, :, 0:3])).mean() + (sglights[:, :, 3]/(1.0+sglights[:, :, 3])).mean())
    #     # loss = loss + 0.1 * (torch.log(1.0 + torch.log(1.0 + sglights[:, :, 3])).mean() - torch.log(
    #     #     1.0 + torch.log(1.0 + sglights[:, :, 0:3])).mean())
    #
    # if kldiv:
    #     # normalize the KL div but not the loss
    #     gtprob = groundtruth.squeeze()[mask, :]
    #     m = gtprob.min()
    #     s = torch.sum(gtprob)
    #     gtprob = gtprob - m
    #     gtprob = gtprob /s
    #     loss = loss + klloss(torch.log(torch.nn.functional.relu( (pred_render-m)/s ) + 1.0), gtprob)

    running_loss += loss.item()
    loss.backward()
    optimizer.step()
    writer.add_scalar('training loss', running_loss, epoch)
    print('loss  ', running_loss, 'for epoch  ', epoch)

    if epoch % 10 == 0:
        with torch.no_grad():
            # pred = pred_render.cpu().unsqueeze(0).view(groundtruthshape)#.numpy()
            pred = torch.zeros(groundtruthshape[0]*groundtruthshape[1], 3)
            gt = (groundtruth*mask.unsqueeze(1).expand(-1,3)).view(groundtruthshape).cpu().permute(2,0,1)
            pred[mask.cpu(),:] = pred_render.cpu()#.unsqueeze(1).view(-1,3)
            pred = pred.view(groundtruthshape).permute(2,0,1)
            diff = torch.abs(pred - gt)
            # print(gt.is_cuda, pred.is_cuda, diff.is_cuda)
            print(gt.shape, pred.shape, diff.shape)
            print('maxs and mins  ', gt.max(), pred.max(), diff.max(), gt.min(), pred.min(), diff.min())
            img_grid = torchvision.utils.make_grid([pred, gt, diff])
            img_grid = img_grid**(1/2.2)
            img_grid = img_grid / img_grid.max()
            img_grid = torch.clip(img_grid, 0.0, 1.0)
            writer.add_image('predictions vs. GT', img_grid, global_step=epoch)

            net_output = net(torch.zeros(1, 3).to(device=gpudevice))
            sglights = sgr.deparametrize_torch(net_output.view(numSGs, 6).unsqueeze_(0), sg_centers, exponentiate).squeeze()
            envmap = sgr.displaySGEnvmap(sglights.cpu().detach())
            print(envmap.shape, envmap.min(), envmap.max())
            img_grid2 = torchvision.utils.make_grid([envmap.permute(2,0,1)])
            writer.add_image('envmap at origin', img_grid2, global_step=epoch)

    if epoch % 40 == 0:
        with torch.no_grad():
            lpos0 = [1.5, 0.0, 0.0]
            net_output = net(torch.tensor(lpos0).unsqueeze(0).to(device=gpudevice))
            sglights = sgr.deparametrize_torch(net_output.view(numSGs, 6).unsqueeze_(0), sg_centers, exponentiate).squeeze()
            envmap0 = sgr.displaySGEnvmap(sglights.cpu().detach())
            lpos1 = [0.0, 1.5, 0.0]
            net_output = net(torch.tensor(lpos1).unsqueeze(0).to(device=gpudevice))
            sglights = sgr.deparametrize_torch(net_output.view(numSGs, 6).unsqueeze_(0), sg_centers, exponentiate).squeeze()
            envmap1 = sgr.displaySGEnvmap(sglights.cpu().detach())
            lpos2 = [0.0, 0.0, 1.5]
            net_output = net(torch.tensor(lpos2).unsqueeze(0).to(device=gpudevice))
            sglights = sgr.deparametrize_torch(net_output.view(numSGs, 6).unsqueeze_(0), sg_centers, exponentiate).squeeze()
            envmap2 = sgr.displaySGEnvmap(sglights.cpu().detach())

            img_grid2 = torchvision.utils.make_grid([envmap0.permute(2,0,1), envmap1.permute(2,0,1), envmap2.permute(2,0,1)])
            writer.add_image('envmaps at 1,0,0 and 0,1,0 and 0,0,1', img_grid2, global_step=epoch)

    if epoch % 251 == 0:
        with torch.no_grad():
            # pred = pred_render.cpu().unsqueeze(0).view(groundtruthshape)#.numpy()
            pred = torch.zeros(groundtruthshape[0]*groundtruthshape[1], 3)
            gt = (groundtruth*mask.unsqueeze(1).expand(-1,3)).view(groundtruthshape).cpu().permute(2,0,1)
            pred[mask.cpu(),:] = pred_render.cpu()#.unsqueeze(1).view(-1,3)
            pred = pred.view(groundtruthshape).permute(2,0,1)
            diff = torch.abs(pred - gt)
            # print(gt.is_cuda, pred.is_cuda, diff.is_cuda)
            print(gt.shape, pred.shape, diff.shape)
            print('maxs and mins  ', gt.max(), pred.max(), diff.max(), gt.min(), pred.min(), diff.min())
            img_grid = torchvision.utils.make_grid([pred, gt, diff])
            img_grid = img_grid**(1/2.2)
            img_grid = img_grid / img_grid.max()
            img_grid = torch.clip(img_grid, 0.0, 1.0)
            writer.add_image('original view', img_grid, global_step=epoch)





            # z = torch.zeros(groundtruthshape[0], groundtruthshape[1], 2)
            # pred= torch.cat((pred.permute(1,2,0),z), dim=-1)
            bmp = Bitmap(pred.permute(1,2,0).numpy(), Bitmap.PixelFormat.RGB)
            # bmp = bmp.convert(Bitmap.PixelFormat.Y, Struct.Type.Float32, srgb_gamma=False)
            bmp.write(outputpath + '/' + experiment_name + '-render.exr')

    running_loss = 0.0
print('Finished Training')


