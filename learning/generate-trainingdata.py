import os
import numpy as np
import enoki as ek
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader
import sys
import os

sys.path += ['C:/Users/grainer.AD/Documents/GitHub/renderers/mitsuba2/build/dist/python']
os.environ['PATH'] += os.pathsep + 'C:/Users/grainer.AD/Documents/GitHub/renderers/mitsuba2/build/dist'

import mitsuba
# Set the desired mitsuba variant (MUST BE THE SAME IN ALL PYTHON FILES !!!)
import nets

mitsuba.set_variant('gpu_rgb')  # 'gpu_autodiff_rgb')
from mitsuba.core import Float, UInt32, Vector2f, Vector3f, Thread, Frame3f, xml, ScalarTransform4f, Bitmap, Struct
from mitsuba.core.xml import load_file, load_dict
from mitsuba.render import (BSDF, BSDFContext, BSDFFlags,
                            BSDFSample3f, SurfaceInteraction3f, register_bsdf,
                            DirectionSample3f, Emitter, ImageBlock,
                            SamplingIntegrator, has_flag,
                            register_integrator, reflect)
from mitsuba.python.util import traverse
import renderhelpers as sgr
import matplotlib.pyplot as plt
import cv2
import imageio
imageio.plugins.freeimage.download()

torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True

# Input paths and parameters ===========================================================================================
scenepath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/scenes/living-room'
outputpath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/livinroom-blend'
envmap_dir_train = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/envmaps/OutdoorHDR-256x128-train'
envmap_dir_test = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/envmaps/OutdoorHDR-256x128-test'

#
# # TODO : testing for single envmap, no dynamic light
# envmap_dir_test = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/envmaps/singleenv'
# outputpath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/livinroom-singleenv'

scenename_gt = 'scene-gilles-blend.xml'
scenename_gt_direct = 'scene-gilles-blend-direct.xml'

write_direct = True

gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
if not os.path.exists(outputpath+'-train'):
    os.makedirs(outputpath+'-train')
if not os.path.exists(outputpath+'-test'):
    os.makedirs(outputpath+'-test')
# Load the scene XMLs
Thread.thread().file_resolver().append(scenepath)
scene_gt = load_file(scenepath + '/' + scenename_gt)
scene_gt_direct = load_file(scenepath + '/' + scenename_gt_direct)

# Load envmaps, DO the train validation data split on the filenames
exr_files = []
files_train, files_val = [], []
for r, d, f in os.walk(envmap_dir_train):  # r=root, d=directories, f = files
    for file in f:
        if '.exr' in file:
            files_train.append(os.path.join(r, file))
for r, d, f in os.walk(envmap_dir_test):  # r=root, d=directories, f = files
    for file in f:
        if '.exr' in file:
            files_val.append(os.path.join(r, file))
print(len(exr_files), ' EXR files found')
# idxs = np.arange(len(exr_files))
# np.random.shuffle(idxs)
# cutoff = int(0.8 * len(exr_files))
# exr_files = np.array(exr_files)
# files_train = exr_files[idxs[0:cutoff]]
# files_val = exr_files[idxs[cutoff:]]
print(len(files_train), ' training files')
print(len(files_val), ' testing files')
print('Load training data.')
train_dataset = sgr.EnvmapDataset(files_train, True, False)
print('Load validation data.')
val_dataset = sgr.EnvmapDataset(files_val, False, False)
print('Training data loaded.')
train_dataloader = DataLoader(train_dataset, batch_size=1, shuffle=True, num_workers=0)
val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=True, num_workers=0)

# Find differentiable scene parameters
params = traverse(scene_gt)
param_res = params['dynamic_envmap.resolution']
param_ref = params['dynamic_envmap.data']
params.keep(['dynamic_envmap.data'])
envmap_mitsuba = torch.ones(param_res[1], param_res[0], 4).to(gpudevice)

if write_direct:
    params_direct = traverse(scene_gt_direct)
    params_direct.keep(['dynamic_envmap.data'])

imsize = 128
spp = 256  #

number_views_per_lighting = 1000  # training views
number_views_test = 10# 10000# 5# 10

startview = 0
starttestview = 0

iteration = 0 # 10070 # startview * len(train_dataloader)

for view in range(startview, number_views_per_lighting):
    print('view : ', view)

    for envmap, envmap_m, envmap_std in train_dataloader:

        randomsensor, campos = sgr.generate_completely_random_sensor(spp, imsize, imsize)

        envmap_mitsuba[:, :, 0:3] = (torch.exp(envmap.squeeze() * envmap_std + envmap_m)).permute(1, 2, 0).to(gpudevice)
        params['dynamic_envmap.data'] = Float(envmap_mitsuba.view(-1))
        params.update()
        scene_gt.integrator().render(scene_gt, randomsensor)

        # GENERATE THE BUFFERS
        components = randomsensor.film().bitmap(raw=False).split()
        for i in range(len(components)):
            if 'position' in components[i][0]:
                buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
                pos3d = torch.from_numpy(buffer.data_np())
            elif 'normals' in components[i][0]:
                buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
                normals = torch.from_numpy(buffer.data_np())
            elif 'diffuse' in components[i][0]:
                buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
                diffuse_bsdf_val = torch.from_numpy(buffer.data_np())
            elif 'specular' in components[i][0]:
                buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
                specular_bsdf_val = torch.from_numpy(buffer.data_np())
            elif 'gt' in components[i][0]:
                buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
                gtcpu = torch.from_numpy(buffer.data_np())
            elif 'alpha' in components[i][0]:
                buffer = components[i][1].convert(Bitmap.PixelFormat.Y, Struct.Type.Float32, srgb_gamma=False)
                alpha = torch.from_numpy(buffer.data_np())

        # normalize position (for living-room) between -1,1
        pos3d[:, :, 0] = 2.0 * pos3d[:, :, 0] / 5.122005 - 1.0
        pos3d[:, :, 1] = 2.0 * pos3d[:, :, 1] / 2.879001 - 1.0
        pos3d[:, :, 2] = 2.0 * pos3d[:, :, 2] / 3.632799 - 1.0

        mask = (alpha > 0.0).expand(-1, -1, 3)
        # to make sure, remove background
        gtcpu[~mask] = 0.0

        wi = sgr.normalize( (campos.unsqueeze(0).unsqueeze(0).expand(pos3d.shape) - pos3d).view(-1,3) ).view(gtcpu.shape)

        envmap = torch.exp( envmap.squeeze().permute(1, 2, 0) )

        #Export all layers and the envmap
        savename = 'render-' + str(iteration).zfill(6)
        imageio.imwrite(outputpath + '-train' + "/" + savename + '-wi' + '.exr', wi.numpy())
        imageio.imwrite(outputpath + '-train' + "/" + savename + '-env' + '.exr', envmap.numpy())
        imageio.imwrite(outputpath + '-train' + "/" + savename + '-pos' + '.exr', pos3d.numpy())
        imageio.imwrite(outputpath + '-train' + "/" + savename + '-n' + '.exr', normals.numpy())
        imageio.imwrite(outputpath + '-train' + "/" + savename + '-diff' + '.exr', diffuse_bsdf_val.numpy())
        imageio.imwrite(outputpath + '-train' + "/" + savename + '-spec' + '.exr', specular_bsdf_val.numpy())
        imageio.imwrite(outputpath + '-train' + "/" + savename + '-gt' + '.exr', gtcpu.numpy())
        imageio.imwrite(outputpath + '-train' + "/" + savename + '-alpha' + '.exr', alpha.numpy())

        if write_direct:
            params_direct['dynamic_envmap.data'] = Float(envmap_mitsuba.view(-1))
            params_direct.update()
            scene_gt_direct.integrator().render(scene_gt, randomsensor)
            components = randomsensor.film().bitmap(raw=False).split()
            for i in range(len(components)):
                if 'gt' in components[i][0]:
                    buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
                    gtcpu_direct = torch.from_numpy(buffer.data_np())
                    gtcpu_direct[~mask] = 0.0
            imageio.imwrite(outputpath + '-train' + "/" + savename + '-gtdirect' + '.exr', gtcpu_direct.numpy())

        iteration = iteration+1


# iteration = 0 # starttestview * len(val_dataloader)
# for view in range(starttestview, number_views_test):
#
#     for envmap, envmap_m, envmap_std in val_dataloader:
#
#         print('test view : ', view)
#         randomsensor, campos = sgr.generate_completely_random_sensor(spp, imsize, imsize)
#
#         envmap_mitsuba[:, :, 0:3] = (torch.exp(envmap.squeeze() * envmap_std + envmap_m)).permute(1, 2, 0).to(gpudevice)
#         params['dynamic_envmap.data'] = Float(envmap_mitsuba.view(-1))
#         params.update()
#         scene_gt.integrator().render(scene_gt, randomsensor)
#
#         # GENERATE THE BUFFERS
#         components = randomsensor.film().bitmap(raw=False).split()
#         for i in range(len(components)):
#             if 'position' in components[i][0]:
#                 buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
#                 pos3d = torch.from_numpy(buffer.data_np())
#             elif 'normals' in components[i][0]:
#                 buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
#                 normals = torch.from_numpy(buffer.data_np())
#             elif 'diffuse' in components[i][0]:
#                 buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
#                 diffuse_bsdf_val = torch.from_numpy(buffer.data_np())
#             elif 'specular' in components[i][0]:
#                 buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
#                 specular_bsdf_val = torch.from_numpy(buffer.data_np())
#             elif 'gt' in components[i][0]:
#                 buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
#                 gtcpu = torch.from_numpy(buffer.data_np())
#             elif 'alpha' in components[i][0]:
#                 buffer = components[i][1].convert(Bitmap.PixelFormat.Y, Struct.Type.Float32, srgb_gamma=False)
#                 alpha = torch.from_numpy(buffer.data_np())
#
#         # normalize position (for living-room) between -1,1
#         pos3d[:, :, 0] = 2.0 * pos3d[:, :, 0] / 5.122005 - 1.0
#         pos3d[:, :, 1] = 2.0 * pos3d[:, :, 1] / 2.879001 - 1.0
#         pos3d[:, :, 2] = 2.0 * pos3d[:, :, 2] / 3.632799 - 1.0
#
#         mask = (alpha > 0.0).expand(-1, -1, 3)
#         gtcpu[~mask] = 0.0
#         wi = sgr.normalize( (campos.unsqueeze(0).unsqueeze(0).expand(pos3d.shape) - pos3d).view(-1,3) ).view(gtcpu.shape)
#
#         envmap = torch.exp(envmap.squeeze().permute(1, 2, 0))
#
#         # Export all layers and the envmap
#         savename = 'render-' + str(iteration).zfill(6)
#         imageio.imwrite(outputpath + '-test' + "/" + savename + '-wi' + '.exr', wi.numpy())
#         imageio.imwrite(outputpath + '-test' + "/" + savename + '-env' + '.exr', envmap.numpy())
#         imageio.imwrite(outputpath + '-test' + "/" + savename + '-pos' + '.exr', pos3d.numpy())
#         imageio.imwrite(outputpath + '-test' + "/" + savename + '-n' + '.exr', normals.numpy())
#         imageio.imwrite(outputpath + '-test' + "/" + savename + '-diff' + '.exr', diffuse_bsdf_val.numpy())
#         imageio.imwrite(outputpath + '-test' + "/" + savename + '-spec' + '.exr', specular_bsdf_val.numpy())
#         imageio.imwrite(outputpath + '-test' + "/" + savename + '-gt' + '.exr', gtcpu.numpy())
#         imageio.imwrite(outputpath + '-test' + "/" + savename + '-alpha' + '.exr', alpha.numpy())
#
#         if write_direct:
#             params_direct['dynamic_envmap.data'] = Float(envmap_mitsuba.view(-1))
#             params_direct.update()
#             scene_gt_direct.integrator().render(scene_gt, randomsensor)
#             components = randomsensor.film().bitmap(raw=False).split()
#             for i in range(len(components)):
#                 if 'gt' in components[i][0]:
#                     buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
#                     gtcpu_direct = torch.from_numpy(buffer.data_np())
#                     gtcpu_direct[~mask] = 0.0
#             imageio.imwrite(outputpath + '-test' + "/" + savename + '-gtdirect' + '.exr', gtcpu_direct.numpy())
#
#         iteration = iteration + 1

print('Finished')
