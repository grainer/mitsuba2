import matplotlib.pyplot as plt
import csv
import numpy as np
# import configargparse
import cv2

# conf = configargparse.ArgumentParser()
#
# conf.add('--ours_path', required=True, nargs='+', help='Path to values to be plotted')
# conf.add('--uni_path', required=True, nargs='+', help='Path to values to be plotted')
# conf.add('--gt_path', required=True, nargs='+', help='Path to values to be plotted')
# conf.add('--inset_1', required=True, nargs='+', help='Labels for each plot')
# conf.add('--inset_2', required=True, nargs='+', help='Labels for each plot')
#
# conf = conf.parse_args()

# scene = 'atelier'
# frame = 310  #  150
# inset_1 = [106, 278] #[440,200]
# inset_2 = [186, 203] # [100,230]
# # inset_3 = [400, 280]
# # inset_4 = [300,300] #  #  [x, y, xwidth, ywidth]
# multiplier = 0.5

scene = 'bedroom'
frame = 52
inset_1 = [203, 362] # [26, 224]
inset_2 = [360, 365]
multiplier = 5.0
#

# scene = 'sanmiguel'
# frame = 547 # 338
# inset_1 = [262, 349] ## [411, 238]
# inset_2 = [432, 171] ## [322, 445]
# multiplier = 5.0

#
# scene = 'kitchen'
# frame = 475 #  389  383  431
# inset_1 = [282, 220]
# inset_2 = [302, 363]
# multiplier = 7.0



inset_width = 64
naive_path = 'F:/gilles/paperresults/' + scene + '/naivebaseline---bats-3-lat-64-layer-4-width-256-lr-0.0001/' + str(frame).zfill(8) + '.exr'
prt_path = 'F:/gilles/paperresults/' + scene + '/prtbaseline---bats-3-lat-64-layer-2-width-256-lr-0.0001/' + str(frame).zfill(8) + '.exr'
twopred_path = 'F:/gilles/paperresults/' + scene + '/2predictions---bats-3-lat-64-layer-2-width-256-lr-0.0001/' + str(frame).zfill(8) + '.exr'
split_path = 'F:/gilles/paperresults/' + scene + '/0499-splitnets-withbuffers--bats-3-lat-64-layer-2-width-172-lr-0.0001/' + str(frame).zfill(8) + '.exr'
gt_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/refpaths/' + scene + '/path/' + str(frame).zfill(5) + '.AccumulatePass.output.0.exr'
out_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/4archis/' + scene
linew = 2


naive = cv2.imread(naive_path, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
prt = cv2.imread(prt_path, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
twopred = cv2.imread(twopred_path, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
split = cv2.imread(split_path, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
gt = cv2.imread(gt_path, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
# plt.imshow(gt)
# plt.show()


# TONEMAP EVERYTHING
naive = np.clip(multiplier * naive, 0.0, 1.0)** (1.0 / 2.2) * 255
prt = np.clip(multiplier * prt, 0.0, 1.0)** (1.0 / 2.2) * 255
twopred = np.clip(multiplier * twopred, 0.0, 1.0)** (1.0 / 2.2) * 255
split = np.clip(multiplier * split, 0.0, 1.0)** (1.0 / 2.2) * 255
gt = np.clip(multiplier * gt, 0.0, 1.0)** (1.0 / 2.2) * 255



cv2.rectangle(naive, (int(inset_1[0]), int(inset_1[1])), (int(inset_1[0])+inset_width, int(inset_1[1])+inset_width), (31, 42,220), linew, lineType=cv2.FILLED)
cv2.rectangle(naive, (int(inset_2[0]), int(inset_2[1])), (int(inset_2[0])+inset_width, int(inset_2[1])+inset_width), (232,160,56), linew, lineType=cv2.FILLED)
cv2.rectangle(prt, (int(inset_1[0]), int(inset_1[1])), (int(inset_1[0])+inset_width, int(inset_1[1])+inset_width), (31, 42,220), linew, lineType=cv2.FILLED)
cv2.rectangle(prt, (int(inset_2[0]), int(inset_2[1])), (int(inset_2[0])+inset_width, int(inset_2[1])+inset_width), (232,160,56), linew, lineType=cv2.FILLED)
cv2.rectangle(twopred, (int(inset_1[0]), int(inset_1[1])), (int(inset_1[0])+inset_width, int(inset_1[1])+inset_width), (31, 42,220), linew, lineType=cv2.FILLED)
cv2.rectangle(twopred, (int(inset_2[0]), int(inset_2[1])), (int(inset_2[0])+inset_width, int(inset_2[1])+inset_width), (232,160,56), linew, lineType=cv2.FILLED)
cv2.rectangle(split, (int(inset_1[0]), int(inset_1[1])), (int(inset_1[0])+inset_width, int(inset_1[1])+inset_width), (31, 42,220), linew, lineType=cv2.FILLED)
cv2.rectangle(split, (int(inset_2[0]), int(inset_2[1])), (int(inset_2[0])+inset_width, int(inset_2[1])+inset_width), (232,160,56), linew, lineType=cv2.FILLED)
cv2.rectangle(gt, (int(inset_1[0]), int(inset_1[1])), (int(inset_1[0])+inset_width, int(inset_1[1])+inset_width), (31, 42,220), linew, lineType=cv2.FILLED)
cv2.rectangle(gt, (int(inset_2[0]), int(inset_2[1])), (int(inset_2[0])+inset_width, int(inset_2[1])+inset_width), (232,160,56), linew, lineType=cv2.FILLED)


naive_inset_1 = naive[int(inset_1[1]):int(inset_1[1])+inset_width, int(inset_1[0]):int(inset_1[0])+inset_width]
naive_inset_2 = naive[int(inset_2[1]):int(inset_2[1])+inset_width, int(inset_2[0]):int(inset_2[0])+inset_width]
prt_inset_1 = prt[int(inset_1[1]):int(inset_1[1])+inset_width, int(inset_1[0]):int(inset_1[0])+inset_width]
prt_inset_2 = prt[int(inset_2[1]):int(inset_2[1])+inset_width, int(inset_2[0]):int(inset_2[0])+inset_width]
twopred_inset_1 = twopred[int(inset_1[1]):int(inset_1[1])+inset_width, int(inset_1[0]):int(inset_1[0])+inset_width]
twopred_inset_2 = twopred[int(inset_2[1]):int(inset_2[1])+inset_width, int(inset_2[0]):int(inset_2[0])+inset_width]
split_inset_1 = split[int(inset_1[1]):int(inset_1[1])+inset_width, int(inset_1[0]):int(inset_1[0])+inset_width]
split_inset_2 = split[int(inset_2[1]):int(inset_2[1])+inset_width, int(inset_2[0]):int(inset_2[0])+inset_width]
gt_inset_1 = gt[int(inset_1[1]):int(inset_1[1])+inset_width, int(inset_1[0]):int(inset_1[0])+inset_width]
gt_inset_2 = gt[int(inset_2[1]):int(inset_2[1])+inset_width, int(inset_2[0]):int(inset_2[0])+inset_width]

# naive_inset_3 = naive[int(inset_3[1]):int(inset_3[1])+inset_width, int(inset_3[0]):int(inset_3[0])+inset_width]
# naive_inset_4 = naive[int(inset_4[1]):int(inset_4[1])+inset_width, int(inset_4[0]):int(inset_4[0])+inset_width]
# prt_inset_3 = prt[int(inset_3[1]):int(inset_3[1])+inset_width, int(inset_3[0]):int(inset_3[0])+inset_width]
# prt_inset_4 = prt[int(inset_4[1]):int(inset_4[1])+inset_width, int(inset_4[0]):int(inset_4[0])+inset_width]
# twopred_inset_3 = twopred[int(inset_3[1]):int(inset_3[1])+inset_width, int(inset_3[0]):int(inset_3[0])+inset_width]
# twopred_inset_4 = twopred[int(inset_4[1]):int(inset_4[1])+inset_width, int(inset_4[0]):int(inset_4[0])+inset_width]
# split_inset_3 = split[int(inset_3[1]):int(inset_3[1])+inset_width, int(inset_3[0]):int(inset_3[0])+inset_width]
# split_inset_4 = split[int(inset_4[1]):int(inset_4[1])+inset_width, int(inset_4[0]):int(inset_4[0])+inset_width]
# gt_inset_3 = gt[int(inset_3[1]):int(inset_3[1])+inset_width, int(inset_3[0]):int(inset_3[0])+inset_width]
# gt_inset_4 = gt[int(inset_4[1]):int(inset_4[1])+inset_width, int(inset_4[0]):int(inset_4[0])+inset_width]


# UPSAMPLE INSETS
naive_inset_1 = cv2.resize(naive_inset_1, (0, 0), fx=4, fy=4, interpolation=cv2.INTER_NEAREST)
naive_inset_2 = cv2.resize(naive_inset_2, (0, 0), fx=4, fy=4, interpolation=cv2.INTER_NEAREST)
prt_inset_1 = cv2.resize(prt_inset_1, (0, 0), fx=4, fy=4, interpolation=cv2.INTER_NEAREST)
prt_inset_2 = cv2.resize(prt_inset_2, (0, 0), fx=4, fy=4, interpolation=cv2.INTER_NEAREST)
twopred_inset_1 = cv2.resize(twopred_inset_1, (0, 0), fx=4, fy=4, interpolation=cv2.INTER_NEAREST)
twopred_inset_2 = cv2.resize(twopred_inset_2, (0, 0), fx=4, fy=4, interpolation=cv2.INTER_NEAREST)
split_inset_1 = cv2.resize(split_inset_1, (0, 0), fx=4, fy=4, interpolation=cv2.INTER_NEAREST)
split_inset_2 = cv2.resize(split_inset_2, (0, 0), fx=4, fy=4, interpolation=cv2.INTER_NEAREST)
gt_inset_1 = cv2.resize(gt_inset_1, (0, 0), fx=4, fy=4, interpolation=cv2.INTER_NEAREST)
gt_inset_2 = cv2.resize(gt_inset_2, (0, 0), fx=4, fy=4, interpolation=cv2.INTER_NEAREST)

# # UPSAMPLE 2x
# naive_inset_1 = cv2.resize(naive_inset_1, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
# naive_inset_2 = cv2.resize(naive_inset_2, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
# prt_inset_1 = cv2.resize(prt_inset_1, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
# prt_inset_2 = cv2.resize(prt_inset_2, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
# twopred_inset_1 = cv2.resize(twopred_inset_1, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
# twopred_inset_2 = cv2.resize(twopred_inset_2, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
# split_inset_1 = cv2.resize(split_inset_1, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
# split_inset_2 = cv2.resize(split_inset_2, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
# gt_inset_1 = cv2.resize(gt_inset_1, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
# gt_inset_2 = cv2.resize(gt_inset_2, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)

# naive_inset_3 = cv2.pyrUp(naive_inset_3)
# naive_inset_4 = cv2.pyrUp(naive_inset_4)
# prt_inset_3 = cv2.pyrUp(prt_inset_3)
# prt_inset_4 = cv2.pyrUp(prt_inset_4)
# twopred_inset_3 = cv2.pyrUp(twopred_inset_3)
# twopred_inset_4 = cv2.pyrUp(twopred_inset_4)
# split_inset_3 = cv2.pyrUp(split_inset_3)
# split_inset_4 = cv2.pyrUp(split_inset_4)
# gt_inset_3 = cv2.pyrUp(gt_inset_3)
# gt_inset_4 = cv2.pyrUp(gt_inset_4)




# cv2.rectangle(naive, (int(inset_1[0]), int(inset_1[1])), (int(inset_1[0])+inset_width, int(inset_1[1])+inset_width), (31, 42,220), linew, lineType=cv2.FILLED)
# cv2.rectangle(naive, (int(inset_2[0]), int(inset_2[1])), (int(inset_2[0])+inset_width, int(inset_2[1])+inset_width), (232,160,56), linew, lineType=cv2.FILLED)
# cv2.rectangle(prt, (int(inset_1[0]), int(inset_1[1])), (int(inset_1[0])+inset_width, int(inset_1[1])+inset_width), (31, 42,220), linew, lineType=cv2.FILLED)
# cv2.rectangle(prt, (int(inset_2[0]), int(inset_2[1])), (int(inset_2[0])+inset_width, int(inset_2[1])+inset_width), (232,160,56), linew, lineType=cv2.FILLED)
# cv2.rectangle(twopred, (int(inset_1[0]), int(inset_1[1])), (int(inset_1[0])+inset_width, int(inset_1[1])+inset_width), (31, 42,220), linew, lineType=cv2.FILLED)
# cv2.rectangle(twopred, (int(inset_2[0]), int(inset_2[1])), (int(inset_2[0])+inset_width, int(inset_2[1])+inset_width), (232,160,56), linew, lineType=cv2.FILLED)
# cv2.rectangle(split, (int(inset_1[0]), int(inset_1[1])), (int(inset_1[0])+inset_width, int(inset_1[1])+inset_width), (31, 42,220), linew, lineType=cv2.FILLED)
# cv2.rectangle(split, (int(inset_2[0]), int(inset_2[1])), (int(inset_2[0])+inset_width, int(inset_2[1])+inset_width), (232,160,56), linew, lineType=cv2.FILLED)
# cv2.rectangle(gt, (int(inset_1[0]), int(inset_1[1])), (int(inset_1[0])+inset_width, int(inset_1[1])+inset_width), (31, 42,220), linew, lineType=cv2.FILLED)
# cv2.rectangle(gt, (int(inset_2[0]), int(inset_2[1])), (int(inset_2[0])+inset_width, int(inset_2[1])+inset_width), (232,160,56), linew, lineType=cv2.FILLED)





# plt.imshow(naive)
# plt.show()
# print(np.min(naive), np.mean(naive), np.max(naive))

# save_image(pr, out_dir + "/" + str(counter).zfill(8) + ".png")



cv2.imwrite(out_path + 'naive.png', naive)
cv2.imwrite(out_path + 'naive_inset_1.png',  naive_inset_1)
cv2.imwrite(out_path + 'naive_inset_2.png',  naive_inset_2)
cv2.imwrite(out_path + 'prt.png', prt)
cv2.imwrite(out_path + 'prt_inset_1.png',  prt_inset_1)
cv2.imwrite(out_path + 'prt_inset_2.png',  prt_inset_2)
cv2.imwrite(out_path + 'twopred.png', twopred)
cv2.imwrite(out_path + 'twopred_inset_1.png',  twopred_inset_1)
cv2.imwrite(out_path + 'twopred_inset_2.png',  twopred_inset_2)
cv2.imwrite(out_path + 'split.png', split)
cv2.imwrite(out_path + 'split_inset_1.png',  split_inset_1)
cv2.imwrite(out_path + 'split_inset_2.png',  split_inset_2)
cv2.imwrite(out_path + 'gt.png', gt)
cv2.imwrite(out_path + 'gt_inset_1.png',  gt_inset_1)
cv2.imwrite(out_path + 'gt_inset_2.png',  gt_inset_2)


print(naive.shape, naive_inset_2.shape, np.concatenate((naive_inset_1, naive_inset_2), axis=1).shape)

naivecomp = np.concatenate( (naive, np.concatenate((naive_inset_1, naive_inset_2), axis=1)), axis=0)
prtcomp = np.concatenate( (prt, np.concatenate((prt_inset_1, prt_inset_2), axis=1)), axis=0)
twopredcomp = np.concatenate( (twopred, np.concatenate((twopred_inset_1, twopred_inset_2), axis=1)), axis=0)
splitcomp = np.concatenate( (split, np.concatenate((split_inset_1, split_inset_2), axis=1)), axis=0)
gtcomp = np.concatenate( (gt, np.concatenate((gt_inset_1, gt_inset_2), axis=1)), axis=0)


cv2.imwrite(out_path + 'comparison.png', np.concatenate([naivecomp, prtcomp, twopredcomp, splitcomp, gtcomp], axis=1) )





# ours = cv2.imread(conf.ours_path[0], cv2.IMREAD_COLOR)
# uni = cv2.imread(conf.uni_path[0], cv2.IMREAD_COLOR)
# gt = cv2.imread(conf.gt_path[0], cv2.IMREAD_COLOR)
#
#
# ours_inset_1 = ours[int(conf.inset_1[1]):int(conf.inset_1[1])+int(conf.inset_1[3]), int(conf.inset_1[0]):int(conf.inset_1[0])+int(conf.inset_1[2])]
# ours_inset_2 = ours[int(conf.inset_2[1]):int(conf.inset_2[1])+int(conf.inset_2[3]), int(conf.inset_2[0]):int(conf.inset_2[0])+int(conf.inset_2[2])]
#
# uni_inset_1 = uni[int(conf.inset_1[1]):int(conf.inset_1[1])+int(conf.inset_1[3]), int(conf.inset_1[0]):int(conf.inset_1[0])+int(conf.inset_1[2])]
# uni_inset_2 = uni[int(conf.inset_2[1]):int(conf.inset_2[1])+int(conf.inset_2[3]), int(conf.inset_2[0]):int(conf.inset_2[0])+int(conf.inset_2[2])]
#
# gt_inset_1 = gt[int(conf.inset_1[1]):int(conf.inset_1[1])+int(conf.inset_1[3]), int(conf.inset_1[0]):int(conf.inset_1[0])+int(conf.inset_1[2])]
# gt_inset_2 = gt[int(conf.inset_2[1]):int(conf.inset_2[1])+int(conf.inset_2[3]), int(conf.inset_2[0]):int(conf.inset_2[0])+int(conf.inset_2[2])]
#
# cv2.rectangle(ours, (int(conf.inset_1[0]), int(conf.inset_1[1])), (int(conf.inset_1[0])+int(conf.inset_1[2]), int(conf.inset_1[1])+int(conf.inset_1[3])), (31,42,220), 5, lineType=cv2.FILLED)
# cv2.rectangle(ours, (int(conf.inset_2[0]), int(conf.inset_2[1])), (int(conf.inset_2[0])+int(conf.inset_2[2]), int(conf.inset_2[1])+int(conf.inset_2[3])), (232,160,56), 5, lineType=cv2.FILLED)
#
# cv2.rectangle(uni, (int(conf.inset_1[0]), int(conf.inset_1[1])), (int(conf.inset_1[0])+int(conf.inset_1[2]), int(conf.inset_1[1])+int(conf.inset_1[3])), (31,42,220), 5, lineType=cv2.FILLED)
# cv2.rectangle(uni, (int(conf.inset_2[0]), int(conf.inset_2[1])), (int(conf.inset_2[0])+int(conf.inset_2[2]), int(conf.inset_2[1])+int(conf.inset_2[3])), (232,160,56), 5, lineType=cv2.FILLED)
#
# cv2.rectangle(gt, (int(conf.inset_1[0]), int(conf.inset_1[1])), (int(conf.inset_1[0])+int(conf.inset_1[2]), int(conf.inset_1[1])+int(conf.inset_1[3])), (31,42,220), 5, lineType=cv2.FILLED)
# cv2.rectangle(gt, (int(conf.inset_2[0]), int(conf.inset_2[1])), (int(conf.inset_2[0])+int(conf.inset_2[2]), int(conf.inset_2[1])+int(conf.inset_2[3])), (232,160,56), 5, lineType=cv2.FILLED)
