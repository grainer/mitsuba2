import os, sys
import numpy as np
import imageio as im
import cv2  # resize images with float support
from scipy import ndimage  # gaussian blur
import time
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import imageio
import cv2

shcoeffs = [[4.055, 4.421, 2.739],
            [-1.453, -1.541, -1.241],
            [0.687, 1.357, 1.663],
            [0.743, 0.801, 0.612],
            [-1.445, -1.457, -0.933],
            [-1.316, -1.545, -1.191],
            [0.537, 0.581, 0.752],
            [2.248, 2.148, 1.347],
            [-0.057, -0.11, -0.27],
            [-0.976, -0.852, -0.557],
            [-2.025, -2.022, -1.374],
            [-0.854, -0.887, -0.622],
            [-0.328, -0.5, -0.544],
            [0.809, 0.918, 0.636],
            [0.37, 0.161, 0.009],
            [-0.61, -0.569, -0.318],
            [-0.027, -0.045, -0.064],
            [-1.21, -1.235, -0.74],
            [-1.763, -1.744, -1.167],
            [0.071, 0.199, 0.167],
            [-1.8, -1.807, -1.248],
            [0.007, 0.042, 0.12],
            [-0.066, -0.055, -0.113],
            [-0.919, -0.883, -0.536],
            [-0.776, -0.75, -0.47],
            [0.267, 0.261, 0.206] ]

# shcoeffs =  [ [ 3.207 , 4.189 , 6.174 ],
# [ 2.402 , 3.747 , 5.844 ],
# [ 0.665 , 0.919 , 1.416 ],
# [ -0.25 , -0.247 , -0.257 ],
# [ -0.251 , -0.396 , -0.53 ],
# [ 0.31 , 0.675 , 1.219 ],
# [ -1.712 , -2.442 , -3.649 ],
# [ 0.049 , -0.008 , -0.133 ],
# [ -2.274 , -3.547 , -5.423 ],
# [ -1.071 , -1.882 , -2.772 ],
# [ -0.183 , -0.277 , -0.506 ],
# [ -1.791 , -2.699 , -4.067 ],
# [ -0.376 , -0.668 , -1.11 ],
# [ 0.184 , 0.232 , 0.358 ],
# [ -0.191 , -0.577 , -1.125 ],
# [ 0.508 , 0.697 , 0.911 ],
# [ 0.441 , 0.875 , 1.337 ],
# [ -0.16 , -0.596 , -1.233 ],
# [ 0.51 , 0.663 , 0.977 ],
# [ -0.435 , -0.986 , -1.778 ],
# [ 1.217 , 1.749 , 2.54 ],
# [ 0.104 , 0.195 , 0.4 ],
# [ 1.304 , 2.019 , 2.965 ],
# [ 0.048 , 0.182 , 0.454 ],
# [ 0.946 , 1.735 , 2.607 ],
# [ 0.816 , 1.271 , 1.797 ],
# [ 0.001 , 0.244 , 0.644 ]   ]

# ATELIER 300 indoor
shcoeffs = [[ 3.207 , 4.189 , 6.174 ],
[ -2.402 , -3.747 , -5.844 ],
[ 0.665 , 0.919 , 1.416 ],
[ 0.25 , 0.247 , 0.257 ],
[ -0.251 , -0.396 , -0.53 ],
[ -0.31 , -0.675 , -1.219 ],
[ -1.712 , -2.442 , -3.649 ],
[ -0.049 , 0.008 , 0.133 ],
[ -2.274 , -3.547 , -5.423 ],
[ 1.071 , 1.882 , 2.772 ],
[ -0.183 , -0.277 , -0.506 ],
[ 1.791 , 2.699 , 4.067 ],
[ -0.376 , -0.668 , -1.11 ],
[ -0.184 , -0.232 , -0.358 ],
[ -0.191 , -0.577 , -1.125 ],
[ -0.509 , -0.697 , -0.911 ],
[ 0.441 , 0.875 , 1.337 ],
[ 0.16 , 0.596 , 1.233 ],
[ 0.51 , 0.663 , 0.977 ],
[ 0.435 , 0.986 , 1.778 ],
[ 1.217 , 1.749 , 2.54 ],
[ -0.104 , -0.195 , -0.4 ],
[ 1.304 , 2.019 , 2.965 ],
[ -0.048 , -0.182 , -0.454 ],
[ 0.946 , 1.735 , 2.607 ],
[ -0.816 , -1.271 , -1.797 ],
[ 0.001 , 0.244 , 0.644 ],
[ -0.679 , -1.137 , -1.646 ],
[ 0.014 , 0.125 , 0.328 ],
[ -1.136 , -1.621 , -2.251 ],
[ 0.658 , 1.071 , 1.735 ],
[ 0.212 , 0.253 , 0.384 ],
[ 0.565 , 1.159 , 2.068 ],
[ 0.382 , 0.522 , 0.771 ],
[ 0.24 , 0.671 , 1.344 ],
[ 0.389 , 0.778 , 1.14 ] ]

data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/prt/atelier'
shmax = 25
out_file = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/atelier/300/5-prt25.exr'
# shmax = 9
# out_file = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/atelier/300/4-prt9.exr'

data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/precomputed/atelier'
shmax = 25
out_file = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/atelier/5-prt25.exr'
# shmax = 9
# out_file = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/atelier/4-prt9.exr'

#
# # BEDROOM 458 OUTDOOR
# shcoeffs = [[ 4.055 , 4.421 , 2.739 ],
# [ -0.367 , -0.403 , -0.292 ],
# [ 0.687 , 1.357 , 1.663 ],
# [ -1.59 , -1.69 , -1.352 ],
# [ 1.248 , 1.233 , 0.696 ],
# [ -1.861 , -1.708 , -1.017 ],
# [ 0.537 , 0.58 , 0.752 ],
# [ -1.823 , -2.02 , -1.483 ],
# [ 0.731 , 0.784 , 0.678 ],
# [ -1.107 , -0.994 , -0.61 ],
# [ 1.96 , 1.859 , 1.216 ],
# [ -0.578 , -0.675 , -0.466 ],
# [ -0.328 , -0.5 , -0.544 ],
# [ -1.025 , -1.084 , -0.758 ],
# [ 0.629 , 0.811 , 0.64 ],
# [ 0.314 , 0.25 , 0.2 ],
# [ 0.63 , 0.599 , 0.355 ],
# [ -1.494 , -1.484 , -0.894 ],
# [ 1.524 , 1.512 , 0.976 ],
# [ -0.024 , -0.09 , -0.157 ],
# [ -1.8 , -1.807 , -1.248 ],
# [ 0.067 , 0.183 , 0.133 ],
# [ 0.889 , 0.871 , 0.649 ],
# [ 0.28 , 0.322 , 0.189 ],
# [ -0.454 , -0.454 , -0.314 ],
# [ -0.11 , -0.122 , -0.134 ],
# [ 0.656 , 0.693 , 0.531 ],
# [ -1.869 , -1.798 , -1.138 ],
# [ 0.586 , 0.581 , 0.354 ],
# [ 0.447 , 0.435 , 0.273 ],
# [ -0.265 , -0.278 , -0.243 ],
# [ 0.996 , 0.998 , 0.751 ],
# [ 0.225 , 0.187 , 0.093 ],
# [ 0.267 , 0.27 , 0.25 ],
# [ -1.052 , -0.989 , -0.669 ],
# [ 0.484 , 0.432 , 0.238 ] ]
#
#
# data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/precomputed/bedroom'
# shmax = 25
# out_file = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/bedroom/5-prt25.exr'
# shmax = 9
# out_file = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/bedroom/4-prt9.exr'



res = np.zeros((512,512, 3), dtype=np.float32)
# res = np.zeros((300, 600, 3), dtype=np.float32)
shidx = 0
# READ ALL BASIS IMAGES

for r, d, f in os.walk(data_dir):  # r=root, d=directories, f = files
    f.sort()
    for file in f:
        if 'AccumulatePass.output' in file:
        # if 'sh-' in file:
            # if '.exr' in file:
            if shidx < shmax:
                sh = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
                print(sh.shape)
                res[:,:,0] += shcoeffs[shidx][0] * sh[:,:,0]
                res[:,:,1] += shcoeffs[shidx][1] * sh[:,:,1]
                res[:,:,2] += shcoeffs[shidx][2] * sh[:,:,2]
                shidx += 1

imageio.imwrite(out_file, res)
