import os
import numpy as np
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader
import sys
import os
import cv2
import nets
from torchvision.utils import save_image
import renderhelpers as sgr
import matplotlib.pyplot as plt
import imageio

torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True

dropboxpath = 'F:/gilles/Dropbox/'
# dropboxpath = 'C:/Users/grainer.AD/Dropbox/'

# Input paths and parameters ===========================================================================================
aabb = [[-146.994873, -59.336391, -170.382034], [146.993851, 85.836891, 113.362228]] # for atelier
buffer_dir = dropboxpath + 'INRIA/projects/nprt/data/scenes/atelier/rotate'
envmap_path = dropboxpath + 'INRIA/projects/nprt/data/scenes/indoorenvs/rotate'
outputpath = dropboxpath + 'INRIA/projects/nprt/res/atelier/baselinetests'
writepath = outputpath + '/movies'
networkpath = outputpath + '/integralnet-withnormals-alphanowarp-linearout--bats-3-lat-64-layer-2-width-256-numSG-8-lr-0.0001-freq-0-net.pth' #

# aabb = [[-12.3, -0.0, -4.0], [-8.3, 2.0, 0.0]] # san miguel
# buffer_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/test/00009.'
# envmap_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/test/9C4A9592 Panorama_hdr.exr'
#
# outputpath = 'F:/gilles/Dropbox/INRIA/projects/nprt/res/sanmiguel/splitnet'
# writepath = outputpath + '/twotracks-movie'
# networkpath = outputpath + '/twotracks--bats-3-lat-64-layer-2-width-256-numSG-8-lr-0.0001-freq-0-net.pth' #
# Training parameters ==================================================================================================

latentdim = 64 #8# 72# 72#    128# 72#  350# #128#   512#

numSGs = 8 #  25# 25#  25#      50#

cnndim = 2048  # 96# 512# 2048#  4096#
netwidth = 256# 512#  64# 400#      250#      64#

lightinputdepth = 0
sharpnessprior = False # True

batch_size = 3# 10#0# 60#  5# 5# 24

num_layers = 2#  4  # 4# 3#  4#    1#     5# 3#
learning_rate = 0.0001#  0.0001  # 0.0001#   0.005#   0.01# 0.5#
logloss = True# True  # False#
L2loss = False  # True#

exponentiate = True  # False#  True#
withnormals = False # True  # False
encode_normals = False # False # True
insize = 3 + 3 * withnormals
if encode_normals:
    num_freqs_normals = 3
    insize = insize + 2 * 3 * num_freqs_normals

# for reflected direction
insize = insize + 3
# for roughness
insize = insize + 1
# for material buffers
insize = 10

boundingboxmin = torch.tensor(aabb[0]).unsqueeze(0).unsqueeze(0)
boundingboxsize = torch.tensor(aabb[1]).unsqueeze(0).unsqueeze(0) - boundingboxmin
envmapmean = 0.0

class BufferDataset(Dataset):
    def __init__(self, renderpath, envpath):
        self.alpha = []
        self.diffuse = []
        self.specular = []
        self.envmap = []
        self.gt = []
        self.normals = []
        self.position = []
        self.viewdir = []

        self.envfiles = []
        for r, d, f in os.walk(envpath):  # r=root, d=directories, f = files
            for file in f:
                if '.exr' in file:
                    self.envfiles.append(file)
                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH),
                                      cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(cv2.resize(im, (256, 128)))
                    envmapmean = im.mean()                     # plt.imshow(im)
                    # plt.show()
                    # im /= envmapmean
                    self.envmap.append(im)

        c = 0
        for r, d, f in os.walk(renderpath):  # r=root, d=directories, f = files
            for file in f:
                if '.exr' in file:
                    if c % 50 == 0:
                        print('Read ', c, ' EXR buffers.')

                    filename = os.path.splitext(os.path.basename(file))[0]
                    fname = filename.split('.')
                    if int(fname[0]) == c:
                        c = c+1

                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(im)

                    if fname[2] == 'posW':
                        im = (im - boundingboxmin.expand(im.shape[0], im.shape[1], -1)) / boundingboxsize.expand(im.shape[0], im.shape[1], -1)
                        im = (im-0.5) * 2
                        self.position.append(im)
                    if fname[2] == 'alpha':
                        self.alpha.append(im)
                    if fname[2] == 'diffuseOpacity':
                        self.diffuse.append(im / np.pi)
                    if fname[2] == 'output':
                        self.gt.append(im)
                    if fname[2] == 'normW':
                        self.normals.append(im)
                    if fname[2] == 'specRough':
                    # if fname[2] == 'specShading':
                        self.specular.append(im)
                    if fname[2] == 'viewW':
                        self.viewdir.append(im)

    def __len__(self):
        return len(self.gt)

    def __getitem__(self, idx):
        envmapmean = self.envmap[idx].mean()
        env = torch.log(1.0 + self.envmap[idx]/envmapmean)

        pos = self.position[idx]
        alpha = self.alpha[idx]
        alpha = torch.sqrt(self.alpha[idx])

        diff = self.diffuse[idx]
        gt = self.gt[idx] / envmapmean
        normals = self.normals[idx]
        specular = self.specular[idx]
        viewdir = self.viewdir[idx]

        #
        # plt.imshow(env)
        # plt.show()
        #
        # print("position ", pos.min(), pos.max())
        # plt.imshow(pos)
        # plt.show()
        #
        # plt.imshow(alpha)
        # plt.show()
        #
        # plt.imshow(diff)
        # plt.show()
        #
        # plt.imshow(gt)
        # plt.show()
        #
        # plt.imshow(normals)
        # plt.show()
        #
        # print("specu: ", specular.min(), specular.mean(), specular.max())
        specular = torch.clamp(specular, min = 0.0, max = 10000.0)
        # plt.imshow(specular)
        # plt.show()
        #
        # plt.imshow(viewdir)
        # plt.show()

        return alpha.squeeze(), pos.squeeze(), normals.squeeze(), viewdir.squeeze(), diff.squeeze(), specular.squeeze(), gt.squeeze(), env.squeeze()
train_dataset = BufferDataset(buffer_dir, envmap_path)
train_dataloader = DataLoader(train_dataset, batch_size=1, shuffle=False) # , num_workers=10) # , pin_memory=True, drop_last=True)
print("mean of envmap ", envmapmean)




# alpha = torch.sqrt(alpha)
# boundingboxmin = torch.tensor(aabb[0]).unsqueeze(0).unsqueeze(0)
# boundingboxsize = torch.tensor(aabb[1]).unsqueeze(0).unsqueeze(0) - boundingboxmin
# pos3d = (pos3d - boundingboxmin.expand(pos3d.shape[0], pos3d.shape[1], -1)) / boundingboxsize.expand(pos3d.shape[0], pos3d.shape[1], -1)
# pos3d = (pos3d-0.5) * 2
# specular = torch.clamp(specular, min = 0.0, max = 10000.0)
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")

# Setup NN, optimizer, tensorboard etc. ===================================================================================
net = nets.IntegralNet(latentdim, netwidth, num_layers, insize=insize).to(device=gpudevice)
# net = nets.SplitEncodernet(latentdim, netwidth, num_layers, insize=6).to(device=gpudevice)


print(net)
net_params = sum(p.numel() for p in net.parameters() if p.requires_grad)
print("Number of parameters in the network : %d" % net_params)

net.load_state_dict(torch.load(networkpath) )#, strict=False)
net.eval()
with torch.no_grad():
    az = 0
    frames = []
    gridmax = 0
    for alpha, pos3d, normals, viewdir, diffuse, specular, gtcpu, envmap in train_dataloader:
        alpha.squeeze_()
        pos3d.squeeze_()
        normals.squeeze_()
        viewdir.squeeze_()
        diffuse.squeeze_()
        specular.squeeze_()
        gtcpu.squeeze_()
        envmap.squeeze_()

        result = torch.zeros(pos3d.shape)
        origshape = pos3d.shape
        print(origshape)
        # flatten everything
        mask = (torch.sqrt(torch.sum(normals**2, dim=-1, keepdim=True)).expand(-1,-1,3) > 0.0)  # to make sure, remove background
        mask = mask[:,:,0].view(-1)

        pos3d = pos3d.view(-1, 3)
        normals = normals.view(-1, 3)
        diffuse_bsdf_val = diffuse.view(-1, 3)
        specular_bsdf_val = specular.view(-1, 3)
        alpha = alpha[:, :, 0].view(-1)
        wi = viewdir.view(-1, 3)

        # isolate pixels that need shading
        pos3d = pos3d[mask, :]
        pred = torch.zeros(pos3d.shape).to(device=gpudevice)
        normals = normals[mask, :]
        diffuse_bsdf_val = diffuse_bsdf_val[mask, :]
        specular_bsdf_val = specular_bsdf_val[mask, :]
        wi = wi[mask, :]
        alpha = alpha[mask]

        # SPHERICAL WARP
        # cos_alpha = torch.ones(pos3d.shape[0],1)
        # warped_alpha = (alpha**2).unsqueeze(1) * torch.sum(wi * normals, dim=-1, keepdim=True)
        # reflected_dir = sgr.normalize(-wi + 2.0 * sgr.dot(wi, normals) * normals)
        # diff_netin = torch.cat((pos3d, normals, cos_alpha), dim=1)
        # spec_netin = torch.cat((pos3d, reflected_dir, warped_alpha), dim=1)

        cos_alpha = torch.ones(pos3d.shape[0], 1)
        warped_alpha = (alpha ** 2).unsqueeze(1) * torch.sum(wi * normals, dim=-1, keepdim=True)
        # warped_alpha = 2.133 / warped_alpha

        # # ADD REFLECTED DIRECTION
        reflected_dir = sgr.normalize(-wi + 2.0 * sgr.dot(wi, normals) * normals)
        diff_netin = torch.cat((pos3d, normals, normals, torch.ones(pos3d.shape[0], 1)), dim=1)
        spec_netin = torch.cat((pos3d, normals, reflected_dir, alpha.unsqueeze(1)), dim=1)

    # for az in range(90):
        az = az + 1


        # azimuth = int(4.0 * az / 360.0 * envmap.shape[1])
        # # rota = int(1 + (envmap.shape[1] - 2) * np.random.rand(1))
        # env2 = torch.zeros(envmap.shape)
        # env2[:, 0:envmap.shape[1] - azimuth, :] = envmap[:, azimuth:envmap.shape[1], :]
        # env2[:, envmap.shape[1] - azimuth:envmap.shape[1], :] = envmap[:, 0:azimuth, :]
        env2 = envmap

        start = torch.cuda.Event(enable_timing=True)
        end = torch.cuda.Event(enable_timing=True)
        envinp = env2.permute(2,0,1).unsqueeze(0).to(device=gpudevice)
        netin = torch.cat((diff_netin, spec_netin), dim=0).to(device=gpudevice)

        start.record()
        out = net(envinp, netin)
        # diff, spec = net(envinp, pos3d.to(device=gpudevice), normals.to(device=gpudevice), wi.to(device=gpudevice), alpha.unsqueeze(1).to(device=gpudevice))
        end.record()
        torch.cuda.synchronize()
        print("Time to evaluate network:  ", start.elapsed_time(end))

        diffuseshade = diffuse_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(out[0:pos3d.shape[0],:])
        specularshade = specular_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(out[pos3d.shape[0]:,:])
        # diffuseshade = diffuse_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(diff) - 1.0)
        # specularshade = specular_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(spec) - 1.0)

        pred += diffuseshade + specularshade

        result *= 0.0
        result = result.view(-1, 3)
        result[mask, :] = pred.cpu()
        result = result.view(origshape)
        # plt.imshow(torch.exp(env2)-1.0)
        # plt.show()
        # plt.imshow(result)
        # plt.show()
        # groundtruth = torch.from_numpy(cv2.cvtColor(cv2.imread(groundtruthpath + str(az).zfill(5) + ".AccumulatePass.output.0.exr", cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR))
        img_grid = torchvision.utils.make_grid([result.permute(2,0,1), gtcpu.permute(2,0,1)])

        if az==1:
            gridmax = img_grid.max()

        img_grid = img_grid / gridmax
        img_grid = img_grid ** (1.0 / 2.2)
        img_grid = torch.clip(1.6 * img_grid, 0.0, 1.0)

        save_image(img_grid, writepath + "/rotate" + str(az) + ".jpg")
        print('Finished rendering im ', az)

        frames.append(img_grid.permute(1,2,0).numpy())
    imageio.mimsave(writepath + '/movie.gif', frames, format='GIF', fps= 15)

    w = imageio.get_writer(writepath + '/rotate.mp4', format='FFMPEG', mode='I', fps=10 ,
                           quality=10 )
                           # codec='hevc_vaapi' )#,
                           # output_params=['-vaapi_device',
                           #                '/dev/dri/renderD128',
                           #                '-vf',
                           #                'format=gray|nv12,hwupload'],
                           # pixelformat='vaapi_vld')
    for f in frames:
        w.append_data(f)
    w.close()

