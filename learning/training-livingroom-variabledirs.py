import os
import numpy as np
import enoki as ek
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader
import sys
import os

sys.path += ['C:/Users/grainer.AD/Documents/GitHub/renderers/mitsuba2/build/dist/python']
os.environ['PATH'] += os.pathsep + 'C:/Users/grainer.AD/Documents/GitHub/renderers/mitsuba2/build/dist'
import cv2

import mitsuba
# Set the desired mitsuba variant (MUST BE THE SAME IN ALL PYTHON FILES !!!)
import nets

mitsuba.set_variant('gpu_rgb')  # 'gpu_autodiff_rgb')
from mitsuba.core import Float, UInt32, Vector2f, Vector3f, Thread, Frame3f, xml, ScalarTransform4f, Bitmap, Struct
from mitsuba.core.xml import load_file, load_dict
from mitsuba.render import (BSDF, BSDFContext, BSDFFlags,
                            BSDFSample3f, SurfaceInteraction3f, register_bsdf,
                            DirectionSample3f, Emitter, ImageBlock,
                            SamplingIntegrator, has_flag,
                            register_integrator, reflect)
from mitsuba.python.util import traverse
import renderhelpers as sgr
import matplotlib.pyplot as plt

torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True
# Input paths and parameters ===========================================================================================
train_path = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/livinroom-direct-train'
test_path = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/livinroom-direct-test'

train_path = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/correctlivinroom-train'
test_path = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/correctlivinroom-test'

# train_path = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/miniset-direct-train'

outputpath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/results/livinroom'
# Training parameters ==================================================================================================

max_epochs = 5000  # 10000
num_freqs = 10 # 10  # 0 # 0 means no positional encoding

latentdim = 128 # 128 #8# 72# 72#    128# 72#  350# #128#   512#

numSGs = 12  #  25# 25#  25#      50#
numLightSGs = numSGs

cnndim = 2048# 8192 #   # 96# 512# 2048#  4096#
netwidth = 512# 512#  64# 400#      250#      64#
lightinputdepth = 0

sharpnessprior = True # True

batch_size = 3# 10#0# 60#  5# 5# 24
num_layers = 2# 4  # 4# 3#  4#    1#     5# 3#
learning_rate = 0.0001#  0.0001  # 0.0001#   0.005#   0.01# 0.5#
logloss = True# True  # False#
L2loss = False# False  # True#

exponentiate = True  # False#  True#
withnormals = True  # False
encode_normals = False # False # True

insize = 3 + 2 * 3 * num_freqs + 3 * withnormals
if encode_normals:
    num_freqs_normals = 3
    insize = insize + 2 * 3 * num_freqs_normals

experiment_name = 'fixxedfibdir-newdata-logloss--' #

tensorboard_writes = 25 #0

# TODO : add an envmap loss. encoded SGs - envmap (but only evaluated where the SG has support)

# TODO: launch on the full dataset
# TODO : try more SGs
# TODO : do sharpness = exp(output) + numSG

# TODO : train this on direct dataset not direct+indirect



# TODO : make the multipliernet dependent on the SG direction (not the view direction)


# TODO try inserting light code at a different depth

# TODO : do the ablation without view vector
# TODO : think about how to incorporate negative SG lights (maybe only in the residual)

# TODO : idea - weight the SG prediction by inverse of alpha -> specular materials give a better estimation of the incoming lighting

alternate_parametrization = True  # False # True
subtract_direct = False  # False#    True


# Number of parameters in the encoder : 93466
# Number of parameters in the decoder : 237568

# TODO: try with the relative weighted loss, or even just L1
# TODO : try with more SGs
# TODO : experiment with a prior to make sharpness as low as possible so specular mirrors see something

# TODO : train with only 1 envmap and figure out ow to best get spatial details


experiment_name = experiment_name + '-bats-' + str(batch_size) + '-lat-' + str(latentdim) + '-layer-' + str(
                    num_layers) + '-width-' + str(netwidth) \
                  + '-numSG-' + str(numSGs) + '-lr-' + str(learning_rate) + '-freq-' + str(num_freqs)
                  # + '-log-' + str(int(logloss)) + '-L2-' + str(int(L2loss)) + '-withnormals-' + str(int(withnormals))
# Other parameters (hardcoded)
num_workers = 0
hybridanis = False  #
anisowarp = True
# False
sharpness_thresh = 5  # for the anisotropic NDF warp
cos_sharpness = 2.13
cos_amplitude = 1.17
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
if not os.path.exists(outputpath):
    os.makedirs(outputpath)

class BufferDataset(Dataset):
    def __init__(self, path):
        self.alpha = []
        self.diffuse = []
        self.specular = []
        self.envmap = []
        self.gt = []
        self.normals = []
        self.position = []
        self.viewdir = []

        c = 0
        for r, d, f in os.walk(path):  # r=root, d=directories, f = files
            for file in f:
                if '.exr' in file:
                    if c % 50 == 0:
                        print('Read ', c, ' EXR buffers.')

                    filename = os.path.splitext(os.path.basename(file))[0]
                    fname = filename.split('-')
                    if int(fname[1]) == c+1:
                        c = c+1

                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(im)
                    if fname[2] == 'pos':
                        self.position.append(im)
                    if fname[2] == 'alpha':
                        self.alpha.append(im)
                    if fname[2] == 'diff':
                        self.diffuse.append(im)
                    if fname[2] == 'env':
                        self.envmap.append(im)
                    if fname[2] == 'gtdirect':
                        self.gt.append(im)
                    if fname[2] == 'n':
                        self.normals.append(im)
                    if fname[2] == 'spec':
                        self.specular.append(im)
                    if fname[2] == 'wi':
                        self.viewdir.append(im)
    def __len__(self):
        return len(self.gt)

    def __getitem__(self, idx):
        env = torch.log( 1.0 + self.envmap[idx])

        pos = self.position[idx]
        alpha = self.alpha[idx]
        diff = self.diffuse[idx]
        gt = self.gt[idx]
        normals = self.normals[idx]
        specular = self.specular[idx]
        viewdir = self.viewdir[idx]

        return alpha, pos, normals, viewdir, diff, specular, gt, env

class FixedDirNet(nn.Module):
    def __init__(self, cnndim, numLightSGs, inpsize, width, num_layers, sgcenters):
        super(FixedDirNet, self).__init__()
        self.lightSGs = nets.EnvmapSGencoder(cnndim, 3 * numLightSGs)
        self.SGmultipliers = nets.VisibilityNet(numLightSGs, inpsize, width, num_layers)
        self.numLightSGs = numLightSGs
        self.width = width
        self.num_layers = num_layers
        self.activate = nn.ReLU()  # nn.LeakyReLU()#
        self.sgcenters = sgcenters

        sharp = torch.ones(numSGs).unsqueeze(1).cuda() * np.sqrt(numSGs)
        self.sharpness = nn.Parameter(torch.autograd.Variable(sharp,requires_grad=True).cuda())
        self.dirs = nn.Parameter(torch.autograd.Variable(self.sgcenters, requires_grad=True).cuda())


    def forward(self, envmap, pos):
        lightsgs = self.lightSGs(envmap).expand(pos.shape[0], -1).unsqueeze(2).view(-1, self.numLightSGs, 3)
        sgmult = torch.sigmoid(self.SGmultipliers(pos).unsqueeze(2).expand(-1,-1, 3))

        dirvars = torch.cat((self.sharpness, sgr.normalize(self.dirs)), dim=1)
        out = torch.cat( ( torch.exp(lightsgs) * sgmult, dirvars.unsqueeze(0).expand(pos.shape[0], -1,-1)), dim=2)
        # print("netout:  ", out.shape)
        return out

    def getEnvmap(self, envmap):
        rgb = self.lightSGs(envmap).unsqueeze(2).view(-1, self.numLightSGs, 3)
        dirvars = torch.cat((self.sharpness, sgr.normalize(self.dirs)), dim=1)
        return torch.cat( ( torch.exp(rgb), dirvars.unsqueeze(0).expand(rgb.shape[0], -1,-1)), dim=2)

def render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap,
                        gpudevice, net, num_freqs, withnormals, numSGs, alternate_parametrization, exponentiate,
                        timings=False, encode_normals=False, new_complex_sgnet=False, anisotropic=False):

    mask = (alpha > 0.0)  # to make sure, remove background
    gtcpu[~mask] = 0.0
    # flatten everything
    pos3d = pos3d.view(-1, 3)
    normals = normals.view(-1, 3)
    diffuse_bsdf_val = diffuse_bsdf_val.view(-1, 3)
    specular_bsdf_val = specular_bsdf_val.view(-1, 3)
    alpha = alpha[:, :, 0].view(-1)
    wi = wi.view(-1, 3)

    # isolate pixels that need shading
    mask = (alpha > 0.0)
    pos3d = pos3d[mask, :]
    pred = torch.zeros(pos3d.shape).to(device=gpudevice)
    normals = normals[mask, :]
    diffuse_bsdf_val = diffuse_bsdf_val[mask, :]
    specular_bsdf_val = specular_bsdf_val[mask, :]
    wi = wi[mask, :]
    alpha = alpha[mask]
    netin = pos3d
    if num_freqs > 0:
        netin = nets.positional_encoding(pos3d, num_encoding_functions=num_freqs)
    if withnormals:
        nn = normals
        if encode_normals and num_freqs > 0:
            nn = nets.positional_encoding(normals, num_encoding_functions=3)
        netin = torch.cat((netin, nn), dim=1)


        # print('netin ', netin.shape)
    assert (envmap != envmap).any() == False, "Damn! envmap contains NaNs"

    if timings:
        start = torch.cuda.Event(enable_timing=True)
        end = torch.cuda.Event(enable_timing=True)
    envinp = envmap.unsqueeze(0).to(device=gpudevice)
    posinp = netin.to(device=gpudevice)

    if timings:
        start.record()
    sglights = net(envinp, posinp)

    if timings:
        end.record()
        torch.cuda.synchronize()
        print("Time to evaluate network:  ", start.elapsed_time(end))


    if timings:
        print('minmax RGB prediction: ', sglights[:, :, 0:3].min(), sglights[:, :, 0:3].max())
        print('minmax sharpness: ', sglights[:, :, 3].min(), sglights[:, :, 3].max())
        print('minmax thetaphi: ', sglights[:, :, 4:6].min(), sglights[:, :, 4:6].max())

    assert (sglights[:, :, 0:3] != sglights[:, :, 0:3]).any() == False, "Damn! Amplitudes contains NaNs after denormalizing"

    # diffuse shading
    mask_diffuse = (torch.sum(diffuse_bsdf_val * diffuse_bsdf_val, dim=-1, keepdim=False) > 0.0)
    irr = torch.zeros(torch.sum(mask_diffuse), 3).to(device=gpudevice)
    for l in range(sglights.shape[1]):
        irr = irr + sgr.exact_cosSGinnerProduct(normals[mask_diffuse, :].to(device=gpudevice),
                                                sglights[mask_diffuse, l, 0:3],
                                                sglights[mask_diffuse, l, 3],
                                                sglights[mask_diffuse, l, 4:7])
    pred[mask_diffuse, :] = pred[mask_diffuse, :] + irr * diffuse_bsdf_val[mask_diffuse, :].to(device=gpudevice)
    assert (irr != irr).any() == False, "Damn! irr contains NaNs"

    # specular shading
    mask_specular = (torch.sum(specular_bsdf_val * specular_bsdf_val, dim=-1, keepdim=False) > 0.0)
    r2 = alpha[mask_specular] ** 2
    ndf_amp = 1.0 / (r2 * np.pi)
    ndf_amp = ndf_amp.unsqueeze(1).expand(-1, 3)
    ndf_sharp = 2.0 / r2
    ndf_sharp = ndf_sharp.unsqueeze(1).expand(-1, 3)
    ndf_val = torch.zeros(ndf_sharp.shape, device=gpudevice)
    if anisotropic:
        amp, warpX, warpY, warpZ, sharpX, sharpY = sgr.warpBRDF_ASG(ndf_amp.to(gpudevice),
                                                                    ndf_sharp.to(gpudevice),
                                                                    normals[mask_specular, :].to(gpudevice),
                                                                    wi[mask_specular, :].to(gpudevice))

        for l in range(sglights.shape[1]):
            ndf_val = ndf_val + sgr.convolveASG_SG(amp, warpX, warpY, warpZ, sharpX, sharpY,
                                                   sglights[mask_specular, l, 0:3],
                                                   sglights[mask_specular, l, 3].unsqueeze(1).expand(-1, 3),
                                                   sglights[mask_specular, l, 4:7])
    else:
        sg_amp, sg_sharp, sg_axi = sgr.warpBRDF_SG(ndf_amp.to(gpudevice), ndf_sharp.to(gpudevice), normals[mask_specular, :].to(gpudevice),
                                                                    wi[mask_specular, :].to(gpudevice))
        for l in range(sglights.size()[1]):
            ndf_val = ndf_val + sgr.SGinnerProduct(sglights[mask_specular, l, 0:3], sglights[mask_specular, l, 3].unsqueeze(1).expand(-1, 3),
                                               sglights[mask_specular, l, 4:7], sg_amp, sg_sharp, sg_axi)

    assert (ndf_val != ndf_val).any() == False, "Damn! ndf_val contains NaNs"
    pred[mask_specular, :] = pred[mask_specular, :] + torch.nn.functional.relu(
        ndf_val * specular_bsdf_val[mask_specular, :].to(gpudevice))

    return pred, gtcpu.view(-1, 3)[mask, :]

print('Load training data.')
train_dataset = BufferDataset(train_path)
print('Load validation data.')
val_dataset = BufferDataset(test_path)
print('Training data loaded.')
train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True) # , num_workers=10) # , pin_memory=True, drop_last=True)
val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=True) # , pin_memory=True, drop_last=True)


# Setup NN, optimizer, tensorboard etc. ===================================================================================
# sgcenters = torch.from_numpy( (np.random.rand(numSGs, 3) -0.5) ).float()
# sgcenters = sgcenters / torch.clamp(torch.sqrt(torch.sum(sgcenters*sgcenters, dim=1, keepdim=True)), min=1e-6).expand(-1,3)
sgcenters = sgr.fibonacci_sphere(numSGs).to(device=gpudevice)
print('sgcenters ', sgcenters)
net = FixedDirNet(cnndim, numSGs, insize, netwidth, num_layers, sgcenters.cuda()).to(gpudevice)
print(net)
optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
writer = SummaryWriter(outputpath + '/runs/' + experiment_name)  # logloss-l1-adam-5e-3-testFaceNormals')

test_example = iter(val_dataloader)
iteration = 0

with torch.autograd.set_detect_anomaly(True):
    for epoch in range(max_epochs):
        epoch_loss = 0.0

        for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in train_dataloader:
            iteration = iteration + 1
            optimizer.zero_grad()
            batch_loss = 0.0
            groundtruth, pred_render = [], []
            envmaploss = 0.0

            for b in range(alpha_batch.shape[0]):
                pred, gtcpu2 = render_from_buffers(alpha_batch[b], pos3d_batch[b], normals_batch[b], wi_batch[b],
                                                     diffuse_bsdf_val_batch[b], specular_bsdf_val_batch[b], gtcpu_batch[b],
                                                    envmap_batch[b].permute(2,0,1),
                                                    gpudevice, net, num_freqs, withnormals, numSGs,
                                                    alternate_parametrization, exponentiate,
                                                    timings=False, encode_normals=encode_normals, new_complex_sgnet=True)
                # groundtruth.append(gtcpu2)
                # pred_render.append(pred)

                groundtruth.append(gtcpu2 / (1.0 + gtcpu2))
                pred_render.append(pred /  (1.0 + pred))

            groundtruth = torch.cat(groundtruth, dim=0).to(gpudevice)
            pred_render = torch.cat(pred_render, dim=0)
            loss = sgr.my_loss(pred_render, groundtruth, tonemap=False, clip=False, log=logloss, L2=L2loss)

            batch_loss += loss.item()
            loss.backward()
            optimizer.step()
            writer.add_scalar('training loss', batch_loss, iteration)
            epoch_loss = epoch_loss + batch_loss / len(train_dataloader)

            with torch.no_grad():
                if iteration % tensorboard_writes == 1:
                    try:
                        alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch = test_example.next()
                    except StopIteration:
                        test_example = iter(val_dataloader)
                        alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch = test_example.next()

                    alpha = alpha_batch[0]  # .to(gpudevice)
                    pos3d = pos3d_batch[0]  # .to(gpudevice)
                    normals = normals_batch[0]  # .to(gpudevice)
                    wi = wi_batch[0]  # .to(gpudevice)
                    diffuse_bsdf_val = diffuse_bsdf_val_batch[0]  # .to(gpudevice)
                    specular_bsdf_val = specular_bsdf_val_batch[0]  # .to(gpudevice)
                    gtcpu = gtcpu_batch[0]  # .to(gpudevice)
                    envmap = envmap_batch[0].permute(2, 0, 1)  # .to(gpudevice)

                    pred, gtcpu2 = render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap,
                            gpudevice, net, num_freqs, withnormals, numSGs, alternate_parametrization, exponentiate,
                            timings=True, encode_normals=encode_normals, new_complex_sgnet=True)

                    print("batchloss ", batch_loss)

                    alpha = alpha[:, :, 0].view(-1)
                    mask = (alpha > 0.0)
                    pr = torch.zeros(normals.size())
                    pr = pr.view(-1, 3)
                    pr[mask, :] = pred.cpu()
                    pr = pr.view(normals.shape)

                    gtcpu = torch.zeros(normals.size())
                    gtcpu = gtcpu.view(-1, 3)
                    gtcpu[mask, :] = gtcpu2.cpu()
                    gtcpu = gtcpu.view(normals.shape)

                    static = torch.zeros(gtcpu.shape)
                    static = static + diffuse_bsdf_val.cpu() + specular_bsdf_val.cpu()
                    # static = static.unsqueeze(0).view(gtcpu.shape)
                    static = static.permute(2, 0, 1)

                    gtcpu = gtcpu.permute(2, 0, 1)
                    pr = pr.permute(2, 0, 1)
                    diff = torch.abs(pr - gtcpu)

                    env = (torch.exp(envmap.squeeze()) -1.0).cpu()
                    sglights = net.getEnvmap(envmap.unsqueeze(0).to(device=gpudevice)).squeeze()

                    env2 = sgr.displaySGEnvmap(sglights.cpu().detach()).permute(2, 0, 1)
                    print('pred envmap max ', env2.max())
                    img_grid2 = torchvision.utils.make_grid([0.5 * env / env.mean(), 0.5 * env2 / env.mean()])
                    img_grid2 = torch.clip(img_grid2, 0.0, 1.0)
                    img_grid2 = img_grid2 ** (1.0 / 2.2)
                    writer.add_image('envmap at center of scene', img_grid2, global_step=iteration)

                    # print(gtcpu.shape, pr.shape, diff.shape)
                    print('maxs and mins  ', gtcpu.max(), pr.max(), diff.max(), gtcpu.min(), pr.min(), diff.min())
                    img_grid = torchvision.utils.make_grid([static / static.mean() * pr.mean(), pr, gtcpu, diff])
                    img_grid = img_grid / gtcpu.max()
                    img_grid = img_grid ** (1.0 / 2.2)
                    img_grid = torch.clip(1.2 * img_grid, 0.0, 1.0)

                    writer.add_image('static vs predictions vs. GT', img_grid, global_step=iteration)

        writer.add_scalar('Epoch loss (training)', epoch_loss, epoch)

        with torch.no_grad():  # Loop over the test data:
            val_loss = 0.0
            testprint = np.random.randint(0, len(val_dataloader))
            testidx = 0
            for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in val_dataloader:
                pred_render, groundtruth = render_from_buffers(alpha_batch[0], pos3d_batch[0], normals_batch[0], wi_batch[0],
                                                     diffuse_bsdf_val_batch[0], specular_bsdf_val_batch[0], gtcpu_batch[0],
                                                     envmap_batch[0].permute(2, 0, 1),
                                                     gpudevice, net, num_freqs, withnormals, numSGs,
                                                     alternate_parametrization, exponentiate,
                                                     timings=False, encode_normals = encode_normals, new_complex_sgnet=True)
                loss = sgr.my_loss(pred_render, groundtruth.to(gpudevice), tonemap=False, clip=False, log=logloss, L2=L2loss)
                val_loss += loss.item() / len(val_dataloader)

                if testidx == testprint:
                    alpha = alpha_batch[0, :, :, 0].view(-1)
                    mask = (alpha > 0.0)
                    pr = torch.zeros(normals_batch[0].size())
                    pr = pr.view(-1, 3)
                    pr[mask, :] = pred_render.cpu()
                    pr = pr.view(normals_batch[0].shape)

                    gtcpu = torch.zeros(normals_batch[0].size())
                    gtcpu = gtcpu.view(-1, 3)
                    gtcpu[mask, :] = groundtruth.cpu()
                    gtcpu = gtcpu.view(normals_batch[0].shape)

                    static = torch.zeros(gtcpu.shape)
                    static = static + diffuse_bsdf_val_batch[0].cpu() + specular_bsdf_val_batch[0].cpu()
                    # static = static.unsqueeze(0).view(gtcpu.shape)
                    static = static.permute(2, 0, 1)

                    gtcpu = gtcpu.permute(2, 0, 1)
                    pr = pr.permute(2, 0, 1)
                    diff = torch.abs(pr - gtcpu)
                    print('maxs and mins  ', gtcpu.max(), pr.max(), diff.max(), gtcpu.min(), pr.min(), diff.min())
                    img_grid = torchvision.utils.make_grid([static / static.mean() * pr.mean(), pr, gtcpu, diff])
                    img_grid = img_grid / gtcpu.mean()
                    img_grid = img_grid ** (1.0 / 2.2)
                    img_grid = torch.clip(0.5 * img_grid, 0.0, 1.0)
                    writer.add_image('TEST: static vs predictions vs. GT', img_grid, global_step=iteration)
                testidx = testidx +1

            writer.add_scalar('Epoch loss (validation)', val_loss, epoch)
print('Finished Training')
