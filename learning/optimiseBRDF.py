import enoki as ek
import mitsuba
mitsuba.set_variant('gpu_autodiff_rgb')

from mitsuba.core import Thread, Vector3f
from mitsuba.core.xml import load_file
from mitsuba.python.util import traverse
from mitsuba.python.autodiff import render_torch, write_bitmap
import torch
import time

scene_path = 'C:/Users/grainer.AD/Desktop/projects/siddhant/scene.xml'
# scene_path = 'C:/Users/grainer.AD/Desktop/projects/siddhant/scene-plastic.xml'
gt_path = 'C:/Users/grainer.AD/Desktop/projects/siddhant/cu-gt.xml'
# gt_path = 'C:/Users/grainer.AD/Desktop/projects/siddhant/plastic-gt.xml'
output_dir = 'C:/Users/grainer.AD/Desktop/projects/siddhant'

iterations = 500# 100
learning_rate = 0.01


scene = load_file(scene_path)
# Find differentiable scene parameters
params = traverse(scene)
print(params)

# {'OBJMesh.bsdf.weight.value': tensor([0.0263], device='cuda:0', requires_grad=True), 'OBJMesh.bsdf.bsdf_0.reflectance.value': tensor([[0.0870, 0.1993, 0.2560]], device='cuda:0', requires_grad=True), 'OBJMesh.bsdf.bsdf_1.alpha.value': tensor([0.1910], device='cuda:0', requires_grad=True), 'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.6164, 0.1291, 0.2348]], device='cuda:0', requires_grad=True)}

# {'OBJMesh.bsdf.weight.value': tensor([0.2570], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_0.reflectance.value': tensor([[0.0790, 0.2270, 0.3144]], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.0602, 0.0699, 0.0758]], device='cuda:0', requires_grad=True)}
# {'OBJMesh.bsdf.weight.value': tensor([0.3002], device='cuda:0', requires_grad=True), 'OBJMesh.bsdf.bsdf_0.reflectance.value': tensor([[0.0840, 0.2431, 0.3377]], device='cuda:0', requires_grad=True), 'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.0548, 0.0580, 0.0609]], device='cuda:0', requires_grad=True)}


# Diamond (ior 2.5) as blend:
# {'OBJMesh.bsdf.weight.value': tensor([0.5608], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_0.reflectance.value': tensor([[0.0534, 0.1008, 0.1179]], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.3510, 0.3233, 0.3254]], device='cuda:0', requires_grad=True)}

# Water (ior 1.3) as blend:
# {'OBJMesh.bsdf.weight.value': tensor([0.2999], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_0.reflectance.value': tensor([[0.0944, 0.2388, 0.3125]], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.1098, 0.0569, 0.0651]], device='cuda:0', requires_grad=True)}


# Cu as blend:
# {'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.8908, 0.5725, 0.4731]], device='cuda:0', requires_grad=True)}

# {'OBJMesh.bsdf.bsdf_0.reflectance.value': tensor([[0.0647, 0.0482, 0.0354]],
# 'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.8346, 0.5481, 0.4576]],


# {'OBJMesh.bsdf.bsdf_0.reflectance.value': tensor([[0.7714, 0.4208, 0.2679]], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.9512, 0.7600, 0.7061]],

# {'OBJMesh.bsdf.bsdf_0.reflectance.value': tensor([[0.8352, 0.3905, 0.2283]], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_1.alpha.value': tensor([-0.0078], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.8433, 0.6688, 0.6598]], device='cuda:0',

# L1
# {'OBJMesh.bsdf.bsdf_0.reflectance.value': tensor([[0.7394, 0.3569, 0.2217]], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_1.alpha.value': tensor([nan], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.9519, 0.7184, 0.6710]], device='cuda:0',

# {'OBJMesh.bsdf.bsdf_0.reflectance.value': tensor([[2.7220, 0.5588, 0.3949]], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_1.alpha.value': tensor([-1.0010], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.7149, 0.5411, 0.5149]], device='cuda:0',

# CU as plastic:
# {'OBJMesh.bsdf.diffuse_reflectance.value': tensor([[4.3748, 0.6067, 0.4831]], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.specular_reflectance.value': tensor([[13.7019, 10.7582, 14.7256]], device='cuda:0', requires_grad=True)}

# plastic as blend:

# {'OBJMesh.bsdf.weight.value': tensor([0.1065], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_0.reflectance.value': tensor([[0.1109, 0.2704, 0.3429]],
# 'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.9762, 0.8187, 0.8583]],


# {'OBJMesh.bsdf.bsdf_0.reflectance.value': tensor([[0.1817, 0.4237, 0.5186]], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.2321, 0.2047, 0.2264]], device='cuda:0',

# {'OBJMesh.bsdf.bsdf_0.reflectance.value': tensor([[0.2147, 0.4538, 0.5561]], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_1.alpha.value': tensor([-1.0010], device='cuda:0', requires_grad=True),
# 'OBJMesh.bsdf.bsdf_1.specular_reflectance.value': tensor([[0.1914, 0.1669, 0.1807]],
#



# Comment/uncomment as needed ===============================================================================================
# # For the blend material:
params.keep([
    'OBJMesh.bsdf.weight.value',
'OBJMesh.bsdf.bsdf_0.reflectance.value',
'OBJMesh.bsdf.bsdf_1.alpha.value',
# 'OBJMesh.bsdf.bsdf_1.eta.value',
# 'OBJMesh.bsdf.bsdf_1.k.value',
'OBJMesh.bsdf.bsdf_1.specular_reflectance.value'])


# # For a roughplastic optimisation (alpha is not optimisable so change it in the XML):
# params.keep([
#     'OBJMesh.bsdf.diffuse_reflectance.value',
# 'OBJMesh.bsdf.specular_reflectance.value'])

# ============================================================================================================================

# Cu: 0.006



# Render a reference image (no derivatives used yet)
scene_gt = load_file(gt_path)
image_ref = render_torch(scene_gt, spp=8)
crop_size = scene_gt.sensors()[0].film().crop_size()
write_bitmap(output_dir + '/out_ref.png', image_ref, crop_size)
# Which parameters should be exposed to the PyTorch optimizer?
params_torch = params.torch()
# Construct a PyTorch Adam optimizer that will adjust 'params_torch'
opt = torch.optim.Adam(params_torch.values(), lr = learning_rate)
objective = torch.nn.MSELoss()
# objective = torch.nn.L1Loss()
time_a = time.time()

for it in range(iterations):
    # Zero out gradients before each iteration
    opt.zero_grad()
    # Perform a differentiable rendering of the scene
    image = render_torch(scene, params=params, unbiased=True, spp=4, **params_torch)
    # Objective: MSE between 'image' and 'image_ref'
    ob_val = objective(image, image_ref)
    # Back-propagate errors to input parameters
    ob_val.backward()
    # Optimizer: take a gradient step
    opt.step()
    print("step ", it, ob_val.mean())

params = params_torch
params.update()
# image = render_torch(scene, params=params, unbiased=True, spp=8, **params_torch)
image_ref = render_torch(scene_gt, params=params, spp=8)
write_bitmap(output_dir + '/recon.png', image, crop_size)
time_b = time.time()
print()
print('%f ms per iteration' % (((time_b - time_a) * 1000) / iterations))
print(params)