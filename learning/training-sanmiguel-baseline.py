import os
import numpy as np
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader
import sys
import os
import cv2
import nets
from torchvision.utils import save_image

import renderhelpers as sgr
import matplotlib.pyplot as plt

torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True

# Input paths and parameters ===========================================================================================
train_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/train'
test_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/test'

aabb = [[-12.3, -0.0, -4.0], [-8.3, 2.0, 0.0]] # manually modified

envmap_trainpath = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/train'
envmap_testpath = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/test'

pretrain_enc_path = None # 'C:/Users/grainer.AD/Dropbox/INRIA/projects/nprt/res/atelier/pretrained.pth' #

highres_test_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/test'
highres_envmap_testpath = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/test'

outputpath = 'F:/gilles/Dropbox/INRIA/projects/nprt/res/sanmiguel/splitnet'

experiment_name = 'integralnetnowarp-nonormals-reflected-exp-' #
# experiment_name = 'SIRENposconditioned--' #
# experiment_name = 'splitENcoders-L2-' #
# experiment_name = 'doubletransfer-specwithnormal-minustransmit-' #
# Training parameters ==================================================================================================

# # # # # Small datasets for testing
# train_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/atelier/test'
# envmap_trainpath = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/indoorenvs/test'


# TODO :
# try different sharpness multiplier
# TODO try batches of images
# TODO try feeding reflected direction to SIREN
# TODO : try bigger encoder. try not alternate param
# TODO : try coordnet again


# try softplus instead of sigmoid on the multipliers
# try unfreezing the encoder (with multipliernet)
# try same unfreezing with normalnet
# try training from scratch with reasonable network sizes and params
# try bigger batches (more light variation)
# try not alternate parametrization


max_epochs = 5000  # 10000
num_freqs = 0 # 10  # 0 # 0 means no positional encoding
latentdim = 64 # 128 #8# 72# 72#    128# 72#  350# #128#   512#

numSGs = 8 #  25# 25#  25#      50#

cnndim = 2048  # 96# 512# 2048#  4096#
netwidth = 256# 512#  64# 400#      250#      64#

lightinputdepth = 0
sharpnessprior = False # True

batch_size = 3# 10#0# 60#  5# 5# 24

num_layers = 2#  4  # 4# 3#  4#    1#     5# 3#
learning_rate = 0.0001#  0.0001  # 0.0001#   0.005#   0.01# 0.5#
logloss = True# True  # False#
L2loss = False  # True#

exponentiate = True  # False#  True#
withnormals = True # True  # False
encode_normals = False # False # True

insize = 3 + 2 * 3 * num_freqs + 3 * withnormals
if encode_normals:
    num_freqs_normals = 3
    insize = insize + 2 * 3 * num_freqs_normals

# # for reflected direction
# insize = insize + 3
# # for roughness
# insize = insize + 1


# # for material buffers
# insize = insize + 10

# TODO try giving SIREN roughness too
# TODO try pos encoding instead of SIREN. That way the multiplier network can get the light code too
# TODO compare resnet vs no resnet
# TODO try LPIPS loss


# TODO : idea - weight the SG prediction by inverse of alpha -> specular materials give a better estimation of the incoming lighting

alternate_parametrization = True  # False #   True
subtract_direct = False  # False#    True
tensorboard_writes = 500
# Number of parameters in the encoder : 93466
# Number of parameters in the decoder : 237568

# TODO: try with the relative weighted loss, or even just L1
# TODO : try with more SGs
# TODO : experiment with a prior to make sharpness as low as possible so specular mirrors see something

# TODO : train with only 1 envmap and figure out ow to best get spatial details


experiment_name = experiment_name + '-bats-' + str(batch_size) + '-lat-' + str(latentdim) + '-layer-' + str(
                    num_layers) + '-width-' + str(netwidth) \
                  + '-numSG-' + str(numSGs) + '-lr-' + str(learning_rate) + '-freq-' + str(num_freqs)
                  # + '-log-' + str(int(logloss)) + '-L2-' + str(int(L2loss)) + '-withnormals-' + str(int(withnormals))
# Other parameters (hardcoded)
num_workers = 0
hybridanis = False  #
anisowarp = True
# False
sharpness_thresh = 5  # for the anisotropic NDF warp
cos_sharpness = 2.13
cos_amplitude = 1.17
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
if not os.path.exists(outputpath):
    os.makedirs(outputpath)
boundingboxmin = torch.tensor(aabb[0]).unsqueeze(0).unsqueeze(0)
boundingboxsize = torch.tensor(aabb[1]).unsqueeze(0).unsqueeze(0) - boundingboxmin


class BufferDataset(Dataset):
    def __init__(self, renderpath, envpath):
        self.alpha = []
        self.diffuse = []
        self.specular = []
        self.envmap = []
        self.gt = []
        self.normals = []
        self.position = []
        self.viewdir = []

        self.envfiles = []
        for r, d, f in os.walk(envpath):  # r=root, d=directories, f = files
            for file in f:
                if '.exr' in file:
                    self.envfiles.append(file)
                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH),
                                      cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(im)
                    self.envmap.append(im)

        c = 0
        for r, d, f in os.walk(renderpath):  # r=root, d=directories, f = files
            for file in f:
                if '.exr' in file:
                    if c % 50 == 0:
                        print('Read ', c, ' EXR buffers.')

                    filename = os.path.splitext(os.path.basename(file))[0]
                    fname = filename.split('.')
                    if int(fname[0]) == c:
                        c = c+1

                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(im)

                    if fname[2] == 'posW':
                        im = (im - boundingboxmin.expand(im.shape[0], im.shape[1], -1)) / boundingboxsize.expand(im.shape[0], im.shape[1], -1)
                        im = (im-0.5) * 2
                        self.position.append(im)
                    if fname[2] == 'alpha':
                        self.alpha.append(im)
                    if fname[2] == 'diffuseOpacity':
                        self.diffuse.append(im / np.pi)
                    if fname[2] == 'output':
                        self.gt.append(im)
                    if fname[2] == 'normW':
                        self.normals.append(im)
                    if fname[2] == 'specRough':
                        self.specular.append(im)
                    if fname[2] == 'viewW':
                        self.viewdir.append(im)

    def __len__(self):
        return len(self.gt)

    def __getitem__(self, idx):
        env = torch.log(1.0 + self.envmap[idx % len(self.envmap)])

        pos = self.position[idx]
        # alpha = self.alpha[idx]
        alpha = torch.sqrt(self.alpha[idx])

        diff = self.diffuse[idx]
        gt = self.gt[idx]
        normals = self.normals[idx]
        specular = self.specular[idx]
        viewdir = self.viewdir[idx]

        # 
        # plt.imshow(env)
        # plt.show()
        # 
        # print("position ", pos.min(), pos.max())
        # plt.imshow(pos)
        # plt.show()
        # 
        # plt.imshow(alpha)
        # plt.show()
        # 
        # plt.imshow(diff)
        # plt.show()
        # 
        # plt.imshow(gt)
        # plt.show()
        # 
        # plt.imshow(normals)
        # plt.show()
        #
        # print("specu: ", specular.min(), specular.mean(), specular.max())
        specular = torch.clamp(specular, min = 0.0, max = 10000.0)
        # plt.imshow(specular)
        # plt.show()
        # 
        # plt.imshow(viewdir)
        # plt.show()

        return alpha, pos, normals, viewdir, diff, specular, gt, env

print('Load training data.')
train_dataset = BufferDataset(train_path, envmap_trainpath)
print('Load validation data.')
val_dataset = BufferDataset(test_path, envmap_testpath)
print('Training data loaded.')
train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True) # , num_workers=10) # , pin_memory=True, drop_last=True)
val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=True) # , pin_memory=True, drop_last=True)


# Setup NN, optimizer, tensorboard etc. ===================================================================================
# net = nets.Splitnet(latentdim, netwidth, num_layers, insize=insize).to(device=gpudevice)
# net = nets.Crazynet(latentdim, netwidth, num_layers, insize=insize).to(device=gpudevice)
# net = nets.SplitEncodernet(latentdim, netwidth, num_layers, insize=insize).to(device=gpudevice)
# net = nets.Crazynet(latentdim, netwidth, num_layers, insize=insize).to(device=gpudevice)
net = nets.IntegralNet(latentdim, netwidth, num_layers, insize=7).to(device=gpudevice)


# net = nets.SplitDumbnet(latentdim, netwidth, num_layers, insize=insize).to(device=gpudevice)
# net = nets.PretrainedMultiplierNet(latentdim, numSGs, netwidth, num_layers, pretrain_enc_path).to(device=gpudevice)
print(net)

sg_centers = sgr.initialiseSGcenters(numSGs).to(device=gpudevice)
optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
writer = SummaryWriter(outputpath + '/runs/' + experiment_name)  # logloss-l1-adam-5e-3-testFaceNormals')
test_example = iter(val_dataloader)
iteration = 0


def render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap,
                        gpudevice, net, num_freqs, withnormals, numSGs, alternate_parametrization, sg_centers, exponentiate,
                        timings=False, encode_normals=False, new_complex_sgnet=False, anisotropic=False, iteration=100000, withroughness=False):
    # flatten everything
    mask = (torch.sqrt(torch.sum(normals**2, dim=-1, keepdim=True)).expand(-1,-1,3) > 0.0)  # to make sure, remove background
    gtcpu[~mask] = 0.0
    mask = mask[:,:,0].view(-1)

    pos3d = pos3d.view(-1, 3)
    normals = normals.view(-1, 3)
    diffuse_bsdf_val = diffuse_bsdf_val.view(-1, 3)
    trans_nrj = alpha[:, :, 2].view(-1).unsqueeze(1).expand(-1, 3)
    diffuse_bsdf_val = diffuse_bsdf_val * (1.0 - trans_nrj)

    specular_bsdf_val = specular_bsdf_val.view(-1, 3)
    alpha = alpha[:, :, 0].view(-1)
    wi = wi.view(-1, 3)


    # isolate pixels that need shading
    # mask = (alpha > 0.0)
    pos3d = pos3d[mask, :]
    pred = torch.zeros(pos3d.shape).to(device=gpudevice)
    normals = normals[mask, :]
    diffuse_bsdf_val = diffuse_bsdf_val[mask, :]
    specular_bsdf_val = specular_bsdf_val[mask, :]
    wi = wi[mask, :]
    alpha = alpha[mask]
    netin = pos3d
    # # # # ADD REFLECTED DIRECTION
    # reflected_dir = sgr.normalize(-wi + 2.0 * sgr.dot(wi, normals) * normals)
    # reflected_dir = torch.cat((reflected_dir, alpha.unsqueeze(1)), dim=1)

    cos_alpha = torch.ones(pos3d.shape[0], 1)
    warped_alpha = (alpha ** 2).unsqueeze(1) #* torch.sum(wi * normals, dim=-1, keepdim=True)
    reflected_dir = sgr.normalize(-wi + 2.0 * sgr.dot(wi, normals) * normals)
    diff_netin = torch.cat((pos3d,  normals, torch.ones(pos3d.shape[0], 1)), dim=1)
    spec_netin = torch.cat((pos3d,  reflected_dir, alpha.unsqueeze(1)), dim=1)
    netin = torch.cat((diff_netin, spec_netin), dim=0)

    assert (envmap != envmap).any() == False, "Damn! envmap contains NaNs"
    if timings:
        start = torch.cuda.Event(enable_timing=True)
        end = torch.cuda.Event(enable_timing=True)

    envinp = envmap.unsqueeze(0).to(device=gpudevice)
    netin = netin.to(device=gpudevice)

    if timings:
        start.record()

    # irrdiff, irrspec = net(envinp, netin, normals.to(device=gpudevice), wi.to(device=gpudevice), alpha.unsqueeze(1).to(device=gpudevice))
    out = net(envinp, netin)

    if timings:
        end.record()
        torch.cuda.synchronize()
        print("Time to evaluate network:  ", start.elapsed_time(end))

    diffuseim = diffuse_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(out[0:pos3d.shape[0], :]) - 1.0)
    specim = specular_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(out[pos3d.shape[0]:, :]) - 1.0)
    pred += diffuseim + specim

    # pred = diffuse_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(irrdiff) - 1.0) + specular_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(irrspec) - 1.0)
    # diffuseim = diffuse_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(irrdiff) - 1.0)
    # specim = specular_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(irrspec) - 1.0)

    return pred, gtcpu.view(-1, 3)[mask, :], diffuseim, specim  #alpha.unsqueeze(1).expand(-1,3)


# with torch.autograd.detect_anomaly():

for epoch in range(max_epochs):
    epoch_loss = 0.0

    for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in train_dataloader:
        iteration = iteration + 1
        optimizer.zero_grad()
        batch_loss = 0.0
        groundtruth, pred_render = [], []

        for b in range(alpha_batch.shape[0]):
            pred, gtcpu2, _, _ = render_from_buffers(alpha_batch[b], pos3d_batch[b], normals_batch[b], wi_batch[b],
                                                 diffuse_bsdf_val_batch[b], specular_bsdf_val_batch[b], gtcpu_batch[b],
                                                envmap_batch[b].permute(2,0,1),
                                                gpudevice, net, num_freqs, withnormals, numSGs,
                                                alternate_parametrization, sg_centers, exponentiate,
                                                timings=False, encode_normals=encode_normals)

            groundtruth.append(gtcpu2)
            pred_render.append(pred)

        groundtruth = torch.cat(groundtruth, dim=0).to(gpudevice)
        pred_render = torch.cat(pred_render, dim=0)
        loss = sgr.my_loss(pred_render, groundtruth, tonemap=False, clip=False, log=logloss, L2=L2loss)

        batch_loss += loss.item()
        loss.backward()
        optimizer.step()
        writer.add_scalar('training loss', batch_loss, iteration)
        epoch_loss = epoch_loss + batch_loss / len(train_dataloader)

        with torch.no_grad():
            if iteration % tensorboard_writes == 1:
                try:
                    alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch = test_example.next()
                except StopIteration:
                    test_example = iter(val_dataloader)
                    alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch = test_example.next()

                alpha = alpha_batch[0]  # .to(gpudevice)
                pos3d = pos3d_batch[0]  # .to(gpudevice)
                normals = normals_batch[0]  # .to(gpudevice)
                wi = wi_batch[0]  # .to(gpudevice)
                diffuse_bsdf_val = diffuse_bsdf_val_batch[0]  # .to(gpudevice)
                specular_bsdf_val = specular_bsdf_val_batch[0]  # .to(gpudevice)
                gtcpu = gtcpu_batch[0]  # .to(gpudevice)
                envmap = envmap_batch[0].permute(2, 0, 1)  # .to(gpudevice)

                pred, gtcpu2, diffim, specim = render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap,
                        gpudevice, net, num_freqs, withnormals, numSGs, alternate_parametrization, sg_centers, exponentiate,
                        timings=True, encode_normals=encode_normals)

                alpha = alpha[:, :, 0].view(-1)
                mask = (torch.sqrt(torch.sum(normals ** 2, dim=-1, keepdim=False)) > 0.0).view(-1)
                pr = torch.zeros(gtcpu.size())
                pr = pr.view(-1, 3)
                pr[mask, :] = pred.cpu()
                pr = pr.view(gtcpu.shape)

                gtcpu = torch.zeros(normals.size())
                gtcpu = gtcpu.view(-1, 3)
                gtcpu[mask, :] = gtcpu2.cpu()
                gtcpu = gtcpu.view(normals.shape)

                diff = torch.zeros(normals.size())
                diff = diff.view(-1, 3)
                diff[mask, :] = diffim.cpu()
                diff = diff.view(normals.shape)
                spec = torch.zeros(normals.size())
                spec = spec.view(-1, 3)
                spec[mask, :] = specim.cpu()
                spec = spec.view(normals.shape)

                static_spec = torch.zeros(gtcpu.shape)
                static_spec = static_spec + specular_bsdf_val.cpu()
                static_spec = static_spec.permute(2, 0, 1)
                static_diff = torch.zeros(gtcpu.shape)
                static_diff = static_diff + diffuse_bsdf_val.cpu()
                static_diff = static_diff.permute(2, 0, 1)

                gtcpu = gtcpu.permute(2, 0, 1)
                pr = pr.permute(2, 0, 1)
                diff = diff.permute(2, 0, 1)
                spec = spec.permute(2, 0, 1)
                difference = torch.abs(pr - gtcpu)

                # print(gtcpu.shape, pr.shape, diff.shape)
                print('maxs and mins  ', gtcpu.max(), pr.max(), diff.max(), gtcpu.min(), pr.min(), diff.min())
                print('diffuse minmax : ', diff.min(), diff.mean(), diff.max())
                print('specular minmax : ', spec.min(), spec.mean(), spec.max())
                img_grid = torchvision.utils.make_grid([static_diff / static_diff.mean() * pr.mean(), static_spec / static_diff.mean() * pr.mean(), diff, spec, pr, gtcpu, difference])
                img_grid = img_grid / gtcpu.max()
                img_grid = img_grid ** (1.0 / 2.2)
                img_grid = torch.clip(1.2 * img_grid, 0.0, 1.0)

                writer.add_image('static vs predictions vs. GT', img_grid, global_step=iteration)

    writer.add_scalar('Epoch loss (training)', epoch_loss, epoch)

    with torch.no_grad():  # Loop over the test data:
        val_loss = 0.0
        for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in val_dataloader:
            pred_render, groundtruth, sharpprior,_ = render_from_buffers(alpha_batch[0], pos3d_batch[0], normals_batch[0], wi_batch[0],
                                                 diffuse_bsdf_val_batch[0], specular_bsdf_val_batch[0], gtcpu_batch[0],
                                                 envmap_batch[0].permute(2, 0, 1),
                                                 gpudevice, net, num_freqs, withnormals, numSGs,
                                                 alternate_parametrization, sg_centers, exponentiate,
                                                 timings=False, encode_normals = encode_normals)
            loss = sgr.my_loss(pred_render, groundtruth.to(gpudevice), tonemap=False, clip=False, log=logloss, L2=L2loss)
            val_loss += loss.item() / len(val_dataloader)

        writer.add_scalar('Epoch loss (validation)', val_loss, epoch)

        # TODO : save network and run it on the high-res testing images
        torch.save(net.state_dict(), outputpath + "/" + experiment_name + "-net.pth")

        dataset = BufferDataset(highres_test_path, highres_envmap_testpath)
        dataloader = DataLoader(dataset, batch_size=1, shuffle=False)  # , pin_memory=True, drop_last=True)
        imcount = 0
        for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in dataloader:
            pred_render, groundtruth, sharpprior, _ = render_from_buffers(alpha_batch[0], pos3d_batch[0],
                                                                          normals_batch[0], wi_batch[0],
                                                                          diffuse_bsdf_val_batch[0],
                                                                          specular_bsdf_val_batch[0], gtcpu_batch[0],
                                                                          envmap_batch[0].permute(2, 0, 1),
                                                                          gpudevice, net, num_freqs, withnormals,
                                                                          numSGs,
                                                                          alternate_parametrization, sg_centers,
                                                                          exponentiate,
                                                                          timings=False, encode_normals=encode_normals,
                                                                          anisotropic=anisowarp)
            alpha = alpha_batch[0]  # .to(gpudevice)
            pos3d = pos3d_batch[0]  # .to(gpudevice)
            normals = normals_batch[0]  # .to(gpudevice)
            wi = wi_batch[0]  # .to(gpudevice)
            diffuse_bsdf_val = diffuse_bsdf_val_batch[0]  # .to(gpudevice)
            specular_bsdf_val = specular_bsdf_val_batch[0]  # .to(gpudevice)
            gtcpu = gtcpu_batch[0]  # .to(gpudevice)
            envmap = envmap_batch[0].permute(2, 0, 1)
            alpha = alpha[:, :, 0].view(-1)
            mask = (torch.sqrt(torch.sum(normals ** 2, dim=-1, keepdim=False)) > 0.0).view(-1)
            pr = torch.zeros(gtcpu.size())
            pr = pr.view(-1, 3)
            pr[mask, :] = pred_render.cpu()
            pr = pr.view(gtcpu.shape)
            gtcpu = torch.zeros(normals.size())
            gtcpu = gtcpu.view(-1, 3)
            gtcpu[mask, :] = groundtruth.cpu()
            gtcpu = gtcpu.view(normals.shape)
            gtcpu = gtcpu.permute(2, 0, 1)
            pr = pr.permute(2, 0, 1)
            diff = torch.abs(pr - gtcpu)
            img_grid = torchvision.utils.make_grid([pr, gtcpu, diff])
            img_grid = img_grid / gtcpu.max()
            img_grid = img_grid ** (1.0 / 2.2)
            img_grid = torch.clip(1.2 * img_grid, 0.0, 1.0)
            save_image(img_grid, outputpath + "/"+ experiment_name + "-" + str(imcount) + ".jpg")
            imcount = imcount + 1

print('Finished Training')