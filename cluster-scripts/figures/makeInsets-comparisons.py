import matplotlib.pyplot as plt
import csv
import numpy as np
# import configargparse
import cv2
import os

multiplier = 5.0
data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/bedroom/458'
denoise_multiplier = 1.0


multiplier = 0.5
data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/atelier/300'
denoise_multiplier = 4.4


multiplier = 0.5
data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/progressive/atelier/510' # 110


multiplier = 5.0
data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/progressive/bedroom/0' # 110



data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/denoising/kitchen'
multiplier = 18.0
denoise_multiplier = 25.0




data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/denoising/sanmiguel'
multiplier = 15.0
denoise_multiplier = 15.0



data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/atelier/closeup-nets'
multiplier = 0.6



data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/atelier/closeup1'
multiplier = 0.6
data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/closeup1'
multiplier = 5.0
# data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/kitchen/closeup1'
# multiplier = 10.0
# data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/bedroom/closeup1'
# multiplier = 5.0



closeup = False # True
closecoords = [70,162, 467, 506]   # [149,88, 379,299]


out_dir = data_dir
ims = []
for r, d, f in os.walk(data_dir):  # r=root, d=directories, f = files
    f.sort()
    for file in f:
        # if 'AccumulatePass.output.0.exr' in file:
        # if 'Denoiser.output.0.exr' in file:
        if '.exr' in file:
            im = cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)

            if closeup:
                im = im[closecoords[1]:closecoords[3], closecoords[0]:closecoords[2], :]

            if 'denoise' in file:
                im = np.clip(denoise_multiplier * im, 0.0, 1.0) ** (1.0 / 2.2) * 255
            else:
                im = np.clip(multiplier * im, 0.0, 1.0) ** (1.0 / 2.2) * 255

            ims.append(im)



cv2.imwrite(out_dir + '/figure.png', np.concatenate(ims, axis=1))
cv2.imwrite(out_dir + '/figure-vertical.png', np.concatenate(ims, axis=0))

# cv2.imwrite(out_dir + '/figure-2lines.png', np.concatenate((np.concatenate(ims[0:3], axis=1),  np.concatenate(ims[3:6], axis=1)), axis=0))



