import torch
import torch.nn as nn
import siren_pytorch as siren


torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")

numberofframes = 1000
imwidth = 512

class BasicConvSkip(nn.Module):
    def __init__(self, in_channels, out_channels, kernel, batchnorm=False, nonlinearity=None, maxpool=False,
                 coordconv=False):
        super(BasicConvSkip, self).__init__()
        self.norm = batchnorm
        if self.norm:
            self.bn = nn.BatchNorm2d(out_channels)
        self.nonlin = True
        if nonlinearity is None:
            self.nonlin = False
        if nonlinearity == 'leaky':
            self.activate = nn.LeakyReLU()
        if nonlinearity == 'relu':
            self.activate = nn.ReLU()
        self.coordconv = coordconv
        if coordconv:
            # self.conv = CoordConv(in_channels, out_channels, kernel, stride=2, padding=1, with_r=True)
            self.conv = nn.Conv2d(in_channels+3, out_channels, kernel, stride=1, padding=1)
        else:
            self.conv = nn.Conv2d(in_channels, out_channels, kernel, stride=1, padding=1)
            # self.conv = nn.Conv2d(in_channels, out_channels, kernel, stride=2, padding=1)

        if maxpool:
            self.downsample = nn.MaxPool2d(2, 2)  # nn.AvgPool2d(2,2)#         #
        else:
            self.downsample = nn.AvgPool2d(2, 2)  # #
        self.avgpool = nn.AvgPool2d(2, 2)  # #

    def forward(self, x):
        xdown = self.downsample(x)
        out = self.downsample(self.conv(x))
        if self.norm:
            out = self.bn(out)
        if self.nonlin:
            out = self.activate(out)
        return out + xdown

class EnvmapSGencoder(nn.Module):
    def __init__(self, cnndim, latentdim):
        super(EnvmapSGencoder, self).__init__()
        self.latentdim = latentdim
        self.cnndim = cnndim
        chans = 32#32# 4  # 3# 16
        #
        # self.encoder_cnn = models.resnet18()
        # self.encoder_fc = nn.Linear(1000, latentdim)

        # self.encoder_cnn = nn.Sequential(
        #     nn.Conv2d(3, chans, 3, stride=1, padding=1),
        #     nn.ReLU(), nn.MaxPool2d(2, 2),
        #     BasicConvSkip2(chans, 3, maxpool=False, coordconv=False),
        #     BasicConvSkip2(chans *2, 3, maxpool=False, coordconv=False),
        #     BasicConvSkip2(chans *4, 3, maxpool=False, coordconv=False) )
        # self.encoder_fc = nn.Linear(self.cnndim, self.latentdim)
        self.encoder_cnn = nn.Sequential(
            nn.Conv2d(3, chans, 3, stride=1, padding=1), nn.ReLU(), nn.MaxPool2d(2, 2),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False),
            # BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False))
        self.encoder_fc = nn.Linear(self.cnndim, self.latentdim)

        enc_params = sum(p.numel() for p in self.encoder_fc.parameters() if p.requires_grad) + sum(
            p.numel() for p in self.encoder_cnn.parameters() if p.requires_grad)
        print("Number of parameters in the encoder : %d" % enc_params)
        # torch.nn.init.xavier_uniform_(self.encoder_fc.weight, gain=torch.nn.init.calculate_gain('relu'))
        # torch.nn.init.zeros_(self.encoder_fc.bias)

    def forward(self, envmap):
        x1 = self.encoder_cnn(envmap)
        # print('x1 shape ', x1.shape)
        x1 = x1.view(1, self.cnndim)
        # print('x1 shape2 ', x1.shape)
        return self.encoder_fc(x1)

class posNet(nn.Module):
    def __init__(self, insize, outsize, width, num_layers):
        super(posNet, self).__init__()
        self.width = width
        self.num_layers = num_layers

        layers = [nn.Linear(insize, width)]  # , nn.ReLU()]
        for i in range(num_layers - 1):
            layers.append(nn.Linear(width, width))
        self.lastlayer = nn.Linear(width, outsize, bias=True)
        self.module_list = nn.ModuleList(layers)
        self.activate = nn.ReLU()

    def forward(self, x):
        for f in self.module_list:
            x = self.activate(f(x))
        return self.lastlayer(x)


class Splitnet(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6): # Retry this with alternate parametrization
        super(Splitnet, self).__init__()
        self.diffusenet = posNet(latentdim + 64, 3, width, num_layers)
        self.specularnet = posNet(latentdim + 64, 3, width, num_layers)
        self.pos_decoder_diffuse = siren.SirenNet(dim_in=9, dim_hidden=width, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.pos_decoder_specular = siren.SirenNet(dim_in=16, dim_hidden=width, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, 64)

    def forward(self, envmap, posnorm, posviewrough):
        lightcode = self.encoder(envmap).expand(posnorm.shape[0], -1)
        poscode_diffuse = self.pos_decoder_diffuse(posnorm)
        poscode_specular = self.pos_decoder_specular(posviewrough)
        diff = self.diffusenet(torch.cat((lightcode, poscode_diffuse), dim=1))
        spec = self.specularnet(torch.cat((lightcode, poscode_specular), dim=1))
        return diff, spec
net = Splitnet(64, 172, 2).to(device=gpudevice)
print("number of parameters ", sum(p.numel() for p in net.parameters() if p.requires_grad))
compute_time = 0.0
with torch.no_grad():
    for f in range(numberofframes):
        envmap = torch.rand(1, 3, 256, 128).to(device=gpudevice)
        posnorm = torch.rand(imwidth * imwidth, 9).to(device=gpudevice)
        posviewrough = torch.rand(imwidth * imwidth, 16).to(device=gpudevice)

        start = torch.cuda.Event(enable_timing=True)
        end = torch.cuda.Event(enable_timing=True)
        start.record()

        diff, spec = net(envmap, posnorm, posviewrough)

        end.record()
        torch.cuda.synchronize()

        compute_time += start.elapsed_time(end) / numberofframes
print("Time to evaluate split tracks network:  ", compute_time)


class Dumbnet(nn.Module):
    def __init__(self, latentdim, width, num_layers, inpsize=6): # Retry this with alternate parametrization
        super(Dumbnet, self).__init__()
        self.sg_decoder = posNet(latentdim + latentdim, 3, width, 2)
        # self.pos_decoder = nets.posNet(inpsize, latentdim, width, 2)
        self.pos_decoder = siren.SirenNet(dim_in=inpsize, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, latentdim)

    def forward(self, envmap, pos):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        poscode = self.pos_decoder(pos)
        sgs = self.sg_decoder(torch.cat((lightcode, poscode), dim=1))
        return sgs
insize = 19
net = Dumbnet(64, 256, 2, inpsize=insize).to(device=gpudevice)
print("number of parameters ", sum(p.numel() for p in net.parameters() if p.requires_grad))
compute_time = 0.0
with torch.no_grad():
    for f in range(numberofframes):
        envmap = torch.rand(1, 3, 256, 128).to(device=gpudevice)
        posnorm = torch.rand(imwidth * imwidth, insize).to(device=gpudevice)

        start = torch.cuda.Event(enable_timing=True)
        end = torch.cuda.Event(enable_timing=True)
        start.record()

        res = net(envmap, posnorm)

        end.record()
        torch.cuda.synchronize()

        compute_time += start.elapsed_time(end) / numberofframes
print("Time to evaluate baseline network:  ", compute_time)


class DumbDumbnet(nn.Module):
    def __init__(self, latentdim, width, num_layers, inpsize=6): # Retry this with alternate parametrization
        super(DumbDumbnet, self).__init__()
        self.sg_decoder = posNet(inpsize + latentdim, 3, width, num_layers)
        # self.sg_decoder = siren.SirenNet(dim_in=inpsize + 64, dim_hidden=width, dim_out=3, num_layers=num_layers,
        #                                  w0_initial=30.)

        self.encoder = EnvmapSGencoder(4096, latentdim)

    def forward(self, envmap, pos):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        sgs = self.sg_decoder(torch.cat((lightcode, pos), dim=1))
        return sgs
insize = 19
net = DumbDumbnet(64, 256, 4, inpsize=insize).to(device=gpudevice)
print("number of parameters ", sum(p.numel() for p in net.parameters() if p.requires_grad))
compute_time = 0.0
with torch.no_grad():
    for f in range(numberofframes):
        envmap = torch.rand(1, 3, 256, 128).to(device=gpudevice)
        posnorm = torch.rand(imwidth * imwidth, insize).to(device=gpudevice)

        start = torch.cuda.Event(enable_timing=True)
        end = torch.cuda.Event(enable_timing=True)
        start.record()

        res = net(envmap, posnorm)

        end.record()
        torch.cuda.synchronize()

        compute_time += start.elapsed_time(end) / numberofframes
print("Time to evaluate naive network:  ", compute_time)


class TwoPredictionsNet(nn.Module):
    def __init__(self, latentdim, width, num_layers, inpsize=6):
        super(TwoPredictionsNet, self).__init__()
        self.sg_decoder = posNet(latentdim + latentdim, 6, width, num_layers)
        # self.sg_decoder = posNet(latentdim + 64, 3, width, num_layers)
        self.pos_decoder = siren.SirenNet(dim_in=inpsize, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, latentdim)
    def forward(self, envmap, pos):  # posdiff, posspec):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        poscode = self.pos_decoder(pos)
        out = self.sg_decoder(torch.cat((lightcode, poscode), dim=1))
        return out[:, 0:3], out[:,3:]
        # return self.sg_decoder(torch.cat((lightcode, poscode), dim=1))
insize = 19
net = TwoPredictionsNet(64, 256, 2, inpsize=insize).to(device=gpudevice)
print("number of parameters ", sum(p.numel() for p in net.parameters() if p.requires_grad))
compute_time = 0.0
with torch.no_grad():
    for f in range(numberofframes):
        envmap = torch.rand(1, 3, 256, 128).to(device=gpudevice)
        posnorm = torch.rand(imwidth * imwidth, insize).to(device=gpudevice)

        start = torch.cuda.Event(enable_timing=True)
        end = torch.cuda.Event(enable_timing=True)
        start.record()

        res, spec = net(envmap, posnorm)

        end.record()
        torch.cuda.synchronize()

        compute_time += start.elapsed_time(end) / numberofframes
print("Time to evaluate split predictions network:  ", compute_time)