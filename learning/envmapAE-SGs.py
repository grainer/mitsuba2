import os
import torch
import cv2
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
import nets
from collections import OrderedDict
import renderhelpers as sgr
import copy

# data_dir_train = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/envmaps/OutdoorHDR-256x128-train'
data_dir_train = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/envmaps/IndoorHDRDataset2018-256x128-train'
data_dir_test = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/envmaps/OutdoorHDR-256x128-test'

outputpath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/results/'

modelname = 'pretrained-deep-indoordata-12SGx7.pth' #  'outdoor-50SG-encodernet-newparam-maxpool-huberloss-cosineweightloss.pth'
# 0.439 for coordvonc, 0.49 without (best 0.4369 with avg pool)
coordconv = False # True

# TODO : test narrow deep encoder vs shallow encoder. See if diff??
# 0.65135104 with deep, 0.47297 with shallow. However deep results look a lot better :(

# TODO : test encoder without skip (same as with skip...)
# 0.4593

# TODO : now trying no coord conv, but with stride 1 and maxpooling
# maybe try max vs avg pooling for the skip

num_epochs = 250 #00# 2000# 5000# 2500# 50#  100

scaleinvariant_loss = False#    True
imwidth = 128

numSGs = 12 #  12#  60#  25

alternate_parametrization = True # True

logtransf = True#  True#    False#  False#  False#   False# True#
lossL2 = False#       True#   False#

cosweightedloss = True # True

# with the original parametrization
# L1 : good, except when a SG is initialised very sharp the optimisation doesnt resolve it
# L2: ok but clrealy overfits the highlights and doesnt care about anything else
# logL1:
# logL2: not bad but quite regular but colours are there

# WITH DIFFERENT PARAMETRIZATION

# without scale invariance:
# L1: very varied but semi-good
# L2: shite
# logL1: pretty regular
# logL2: very regular
# with blogpost scale invariance:
# L1: all black
# L2: shite
# logL1: very regular
# logL2: halfway regular but not bad
# with scale invariance:
# L1 is all black
# L2 is weird shite
# log L1 has regular grid of SGs
# log L2 is same but worse, color isnt really right

# TODO trying with exponential instead of tan(tanh())
# exponential with logL2 loss, original parametrization is the best
# Best test loss was  0.05976928038788693


resize_ims = False
num_workers = 0# 2
batch_size = 12
learning_rate = 0.000025# 0.0002# 0.0005
# Best test loss was  0.002839172903424383
exponentiate = True#   False#

latentdim = 6 * numSGs
cnndim = 4096# 64 #  1024# 256#  2048#  4096#

if alternate_parametrization:
    latentdim = 7 * numSGs

# TODO try with L2 loss and if it doesnt work go back to L1. log doesnt seem to work well
# TODO try exp activation instead of tan(tanh) in reparametrization



# TODO : test removing the scaling of the phi tanh remapping

# TODO : test the other deparametrization of SG parameters ???

# TODO add a clause to do something if predicted SG axis is 0 (norm=0)

# # DO the train validation data split on the filenames
# exr_files = []
# for r, d, f in os.walk(data_dir):  # r=root, d=directories, f = files
#     for file in f:
#         if '.exr' in file:
#             exr_files.append(os.path.join(r, file))
# print(len(exr_files), ' EXR files found')
# idxs = np.arange(len(exr_files))
# np.random.shuffle(idxs)
# cutoff = int(0.8 * len(exr_files))
# exr_files = np.array(exr_files)
# files_train = exr_files[idxs[0:cutoff]]
# files_val = exr_files[idxs[cutoff:]]
class AEDataset(Dataset):
    def __init__(self, filenames, augment=True, transform=None, max=False):
        self.transform = transform
        self.files = filenames
        self.images = []
        self.means = []
        self.stds = []
        self.width = 0
        self.augment = augment
        self.maxmin = max

        self.minvalue = 0.00001
        c = 0
        thresh = torch.nn.Threshold(self.minvalue, self.minvalue, inplace=True)

        mmin = 1.0
        mmax = 0.0

        for f in self.files:
            if c%50 == 0:
                print('Read ', c, ' EXR files.')
            im = torch.clip(torch.from_numpy(
                cv2.cvtColor(cv2.imread(f, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH),
                             cv2.COLOR_RGB2BGR)).permute(2, 0, 1), 0.0)
            if self.transform:
                im = self.transform(im)
            # if logtransf:

            im = im / im.mean() # (torch.sum(im) / im.numel())

            mmin = np.minimum(mmin, im.min().item())
            mmax = np.maximum(mmax, im.max().item())

            im = torch.log(1.0 + thresh(im))
            im_m = 0
            im_std = 1

            if self.maxmin:
                im_m = 0 # im.min()
                im_std = im.max() - im_m
                im = (im - im_m) / im_std

            self.images.append(im)
            self.means.append(im_m)
            self.stds.append(im_std)
            c += 1
            self.width = im.shape[2]
        print('minmax of envmaps, ', mmin, mmax)
    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        # print(idx)
        if self.augment:
            rota = int(self.width * np.random.rand(1))
            rgbperm = torch.randperm(3)
            t = self.images[idx]
            ims = torch.zeros(t.shape)
            ims[:,:, 0:self.width-rota] = t[rgbperm,:,rota:self.width]
            ims[:,:, self.width-rota:self.width] = t[rgbperm,:, 0:rota]

            return ims, self.means[idx], self.stds[idx]
        else:
            return self.images[idx], self.means[idx], self.stds[idx]

files_train = []
for r, d, f in os.walk(data_dir_train):  # r=root, d=directories, f = files
    for file in f:
        if '.exr' in file:
            files_train.append(os.path.join(r, file))
files_val = []
for r, d, f in os.walk(data_dir_test):  # r=root, d=directories, f = files
    for file in f:
        if '.exr' in file:
            files_val.append(os.path.join(r, file))

print(len(files_train), ' training files')
print(len(files_val), ' testing files')

transf = None
if resize_ims:
    transf = transforms.Resize(imwidth)
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('Load training data.')
train_dataset = AEDataset(files_train, True, transf, max = False) #sgr.EnvmapDataset(files_train, True, transf)
print('Load validation data.')
val_dataset = AEDataset(files_val, False, transf, max = False) # sgr.EnvmapDataset(files_val, False, transf)
print('Training data loaded.')
train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=num_workers)#, pin_memory=True, drop_last=True)
val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=False, num_workers=num_workers)#, pin_memory=True, drop_last=True)

if coordconv:
    model = nets.CoordEnvmapSGencoder(cnndim, latentdim).to(device)
else:
    model = nets.EnvmapSGencoder(cnndim, latentdim).to(device)
model.train()
print(model)
print('start training on '+ str(len(train_dataloader)) +' envmap batches.')
if lossL2:
    criterion = nn.SmoothL1Loss() # nn.MSELoss()#
else:
    criterion = nn.L1Loss()#
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
sg_centers = sgr.initialiseSGcenters(numSGs).to(device=device)
thresh = torch.nn.Threshold(0.00001, 0.00001, inplace=True)

best_testloss = 10000.0
for epoch in range(num_epochs):
    running_loss = 0.0
    for batch, _, _ in train_dataloader:
        batch = batch.to(device)

        if alternate_parametrization:
            net_out = model(batch).unsqueeze(2).view(-1, numSGs, 7)
            # print(net_out.min(), net_out.mean(), net_out.max())
            assert (net_out != net_out).any() == False, "Damn! network contains NaNs"
            sglights = sgr.deparametrize_2(net_out)
        else:
            net_out = model(batch).unsqueeze(2).view(-1, numSGs, 6)
            sglights = sgr.deparametrize_torch(net_out, sg_centers, exponentiate)
        sglights[:, :, 0:3] = sglights[:, :, 0:3] / (2.0 *np.pi)
        sglights[:, :, 3] = sglights[:, :, 3] * numSGs / np.pi

        # print(outputs.shape)
        optimizer.zero_grad()

        xyzgrid = sgr.create_envmap_grid(batch.shape[2],batch.shape[3]).unsqueeze(0).expand(batch.shape[0], -1,-1,-1).to(device)
        val = torch.zeros(xyzgrid.size()).to(device)
        for l in range(numSGs):
            cosangle = (xyzgrid[:,:, :, 0] * sglights[:,l, 4].unsqueeze(1).unsqueeze(1).expand(xyzgrid[:,:, :, 0].shape)
                        + xyzgrid[:,:, :, 1] * sglights[:,l, 5].unsqueeze(1).unsqueeze(1).expand(xyzgrid[:,:, :, 0].shape)
                        + xyzgrid[:,:, :, 2] * sglights[:,l, 6].unsqueeze(1).unsqueeze(1).expand(xyzgrid[:,:, :, 0].shape) )\
                        .unsqueeze(3).expand(xyzgrid.size())
            col = sglights[:,l, 0:3].unsqueeze(1).unsqueeze(1).expand(xyzgrid.size())
            sg = col * torch.exp(sglights[:,l, 3].unsqueeze(1).unsqueeze(1).unsqueeze(1).expand(xyzgrid.shape) * (cosangle - 1.0))
            val = val + sg

        # plt.imshow(val[0].clone().detach().cpu())
        # plt.show()
        val = thresh(val).permute(0, 3, 1, 2)
        if logtransf:
            val = torch.log(val + 1.0)
        else:
            batch = torch.exp(batch) - 1.0



        # loss = criterion(val, batch).mean()
        tloss = (torch.abs(val - batch) * (torch.exp(batch)-1.0) )
        # tloss = (torch.abs(val - batch) * batch)

        if cosweightedloss:
            # tloss = tloss * xyzgrid[None, :, :, 1]
            tloss = tloss * torch.sqrt(1.0 - xyzgrid[:,None, :,:, 1]**2)  #.expand(tloss.shape)

        loss = tloss.mean()

        if scaleinvariant_loss:
            # loss = loss + criterion(sgr.LSregress(val, batch, val), batch).mean()
            loss = loss - 0.5 / (torch.numel(val)**2) * torch.sum(sgr.LSregress(val, batch, val) - batch)**2

        loss.backward()
        optimizer.step()
        running_loss += loss.item()
        # scheduler.step()
    epoch_loss = running_loss / len(train_dataloader)
    print('Epoch:{} Loss: {:.4f} '.format(epoch, epoch_loss))

    with torch.no_grad():
        test_loss = 0.0
        for inp, m, std in val_dataloader:
            inp = inp.to(device)

            if alternate_parametrization:
                net_out = model(inp).view(numSGs, 7).unsqueeze(0)
                sglights = sgr.deparametrize_2(net_out).squeeze()
            else:
                net_out = model(inp).view(numSGs, 6).unsqueeze(0)
                sglights = sgr.deparametrize_torch(net_out, sg_centers, exponentiate).squeeze()
            sglights[:, 3] = sglights[:, 3] * numSGs / np.pi
            sglights[:,0:3] = sglights[:, 0:3] / (2.0 * np.pi)

            xyzgrid = sgr.create_envmap_grid(inp.shape[2], inp.shape[3]).to(device)
            val = torch.zeros(xyzgrid.size()).to(device)
            for l in range(numSGs):
                cosangle = (xyzgrid[:, :, 0] * sglights[l, 4].unsqueeze(0).unsqueeze(0).expand(
                    xyzgrid[:, :, 0].shape)
                            + xyzgrid[:, :, 1] * sglights[l, 5].unsqueeze(0).unsqueeze(0).expand(
                            xyzgrid[:, :, 0].shape)
                            + xyzgrid[:, :, 2] * sglights[l, 6].unsqueeze(0).unsqueeze(0).expand(
                            xyzgrid[:, :, 0].shape)) \
                    .unsqueeze(2).expand(xyzgrid.size())
                col = sglights[ l, 0:3].unsqueeze(0).expand(xyzgrid.size())
                sg = col * torch.exp(
                    sglights[l, 3].unsqueeze(0).unsqueeze(0).expand(xyzgrid.shape) * (cosangle - 1.0))
                val = val + sg

            # loss = criterion(torch.log(thresh(val)).permute(2,0,1), inp.squeeze_()).mean()
            val = thresh(val).permute(2,0,1)
            if logtransf:
                val = torch.log(val + 1.0)
            else:
                inp = torch.exp(inp) - 1.0 #.squeeze_())



            # loss = criterion(val, inp.squeeze_()).mean()
            tloss = (torch.abs(val - inp.squeeze_()) * (torch.exp(inp.squeeze_()) -1.0) )
            # tloss = (torch.abs(val - inp.squeeze_()) * inp.squeeze_())

            if cosweightedloss:
                tloss = tloss * torch.sqrt(1.0 - xyzgrid[None, :,:, 1]**2)  #.expand(tloss.shape)

            loss = tloss.mean()


            if scaleinvariant_loss:
                # loss = loss + criterion(sgr.LSregress(val, inp.squeeze_(), val), inp.squeeze_()).mean()
                loss = loss - 0.5 / (torch.numel(val) ** 2) * torch.sum(sgr.LSregress(val,  inp.squeeze_(), val) -  inp.squeeze_()) ** 2

            test_loss += loss.item()
        test_loss = test_loss / len(val_dataloader)
        print('      Test Loss: {:.4f} '.format(test_loss))
        if test_loss < best_testloss:
            best_testloss = test_loss
            best_model_state_dict = copy.deepcopy(model.state_dict())
            torch.save(best_model_state_dict, outputpath + modelname)
            # best_model_state_dict = {k: v.to('cpu') for k, v in model.state_dict().items()}
            # best_model_state_dict = OrderedDict(best_model_state_dict)

print('Best test loss was ', best_testloss)
model.load_state_dict(best_model_state_dict)
model.eval()
torch.save(best_model_state_dict, outputpath + modelname)
with torch.no_grad():
    test_loss = 0.0
    for inp, m, std in val_dataloader:
        inp = inp.to(device)

        if alternate_parametrization:
            net_out = model(inp).view(numSGs, 7).unsqueeze(0)
            sglights = sgr.deparametrize_2(net_out).squeeze()
        else:
            net_out = model(inp).view(numSGs, 6).unsqueeze(0)
            sglights = sgr.deparametrize_torch(net_out, sg_centers, exponentiate).squeeze()
        sglights[:, 3] = sglights[:, 3] * numSGs / np.pi
        sglights[:, 0:3] = sglights[:, 0:3] / (2.0 * np.pi)

        xyzgrid = sgr.create_envmap_grid(inp.shape[2], inp.shape[3]).to(device)
        val = torch.zeros(xyzgrid.size()).to(device)
        for l in range(numSGs):
            cosangle = (xyzgrid[:, :, 0] * sglights[l, 4].unsqueeze(0).unsqueeze(0).expand(
                xyzgrid[:, :, 0].shape)
                        + xyzgrid[:, :, 1] * sglights[l, 5].unsqueeze(0).unsqueeze(0).expand(
                        xyzgrid[:, :, 0].shape)
                        + xyzgrid[:, :, 2] * sglights[l, 6].unsqueeze(0).unsqueeze(0).expand(
                        xyzgrid[:, :, 0].shape)) \
                .unsqueeze(2).expand(xyzgrid.size())
            col = sglights[l, 0:3].unsqueeze(0).expand(xyzgrid.size())
            sg = col * torch.exp(
                sglights[l, 3].unsqueeze(0).unsqueeze(0).expand(xyzgrid.shape) * (cosangle - 1.0))
            val = val + sg

        # loss = criterion(torch.log(thresh(val)).permute(2,0,1), inp.squeeze_())
        # test_loss += loss.item()

        plt.figure(figsize=(10, 2))
        plt.subplot(1, 2, 1)
        gt = inp.cpu().squeeze_().permute(1,2,0)

        # gt = 0.5 * gt + 0.5
        # gt = gt * std + m
        gt = torch.exp(gt) - 1.0

        gtmax = gt.max() # gt.mean() *2.0
        plt.imshow((gt/gtmax).numpy()**(1.0/2.2) *3)
        plt.subplot(1, 2, 2)
        pred = val.cpu().squeeze_()
        plt.imshow((pred/gtmax).numpy()**(1.0/2.2) *3)
        plt.show()