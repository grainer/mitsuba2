import os
import numpy as np
import enoki as ek
import mitsuba
# Set the desired mitsuba variant
mitsuba.set_variant('gpu_autodiff_rgb')
from mitsuba.core import Float, Vector3f, Thread, xml
from mitsuba.core.xml import load_file
from mitsuba.render import (BSDF, BSDFContext, BSDFFlags,
                            DirectionSample3f, Emitter, ImageBlock,
                            SamplingIntegrator, has_flag,
                            register_integrator)

# filename = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/diffusescene-sg.xml'
filename = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere/diffusescene-sg.xml'

# lightSG = [[19.77183, 18.899605, 28.09268, 124.06032, 0.13278571, 0.39354134, 0.90966654], [13.951765, 15.265006, 23.527641, 120.74434, -0.15289421, 0.5411309, 0.8269225], [0.4071942, 0.22668266, 0.10997803, 0.89142746, -0.0812242, -0.6204905, 0.7799963], [64.97454, 42.578075, 16.2094, 154.95787, -0.20876692, 0.07599607, 0.97500813], [5.1314754, 4.024345, 3.1723998, 91.700905, -0.88628745, -0.11917673, -0.44753936], [26.087568, 35.68813, 47.75076, 40.86883, -0.18703045, 0.25262922, 0.94931453], [11.181716, 15.722419, 25.156954, 210.88081, 0.10982957, 0.5572564, 0.82304484], [1.8714151, 1.836281, 1.6746786, 35.524567, 0.34814197, -0.37467614, 0.8593107], [51.333977, 24.03316, 0.3976881, 90.55722, 0.124854945, 0.100686945, 0.98705286], [4.564536, 3.5192788, 2.8387885, 131.00095, 0.9625086, -0.11077466, -0.24760067], [0.1044002, 24.66059, 42.970016, 91.73856, 0.13887106, 0.15844153, 0.97755367], [103.84179, 74.74271, 46.163086, 1047.5914, -0.42761734, 0.07404973, 0.90092176]]
# pisa:
lightSG = [[0.9433718, 0.37715107, 0.030528598, 35.069885, -0.84324276, 0.2121104, 0.4939137], [0.70375603, 0.3655845, 0.15851796, 23.585962, 0.52402025, 0.7886307, -0.32165867], [0.08567178, 0.06427135, 0.044218015, 1.1091332, -0.2356962, -0.66532904, -0.70836747], [0.6558805, 0.31723064, 0.11005787, 21.001013, -0.60743445, 0.73271066, -0.30685267], [0.86364007, 0.35598388, 0.11529839, 22.24271, 0.46149436, 0.2683993, -0.8455677], [1.1314801, 1.2558013, 1.2232834, 41.10061, 0.053204477, 0.256633, 0.9650434], [1.0217043, 0.4285945, 0.13063939, 15.990939, -0.047010783, 0.8666322, -0.49672785], [0.20844299, 0.3255447, 0.49083838, 5.3000503, -0.44954064, 0.51849866, 0.72737354], [1.158383, 1.5155687, 1.7711425, 46.52204, 0.38086146, 0.38595143, 0.84022975], [2.3821747, 2.6763604, 2.6895938, 227.89229, 0.6516599, 0.27348915, 0.7074907], [0.66085684, 0.31836376, 0.1304988, 49.41547, 0.9093498, 0.18544276, -0.3724163], [0.7925673, 1.0842797, 1.393003, 14.961501, 0.7191419, 0.51414, 0.46743447]]

savefile = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere/pisa-sgprt-stephenhill.exr'
# savefile = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/pisa-sgprt-stephenhill.exr'



lightSG = [[1.0, 1.0, 1.0, 1000, 0.0, 1.0, 0.0]]
savefile = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere/lightfromtop-stephenhill.exr'

cos_sharpness = 2.133
cos_amplitude = 1.17
def exact_cosSGinnerProduct(normal, amp, sharp, axi):
    umLength = ek.norm(cos_sharpness * normal + sharp * Vector3f(axi))
    # print(umLength)
    # print('--------------')
    lambdam = cos_sharpness + sharp
    sinh_term = ek.exp(umLength-lambdam) - ek.exp(-umLength-lambdam)
    return (2.0 * np.pi * Vector3f(amp) * Vector3f(cos_amplitude) * sinh_term) / umLength

def eval_cosSGinnerProduct(normal, amp, sharp, axi):
    um = cos_sharpness * normal
    um = um + sharp * Vector3f(axi)
    umLength = ek.norm(um)
    # print(umLength)
    # print('--------------')
    expo = ek.exp(umLength - sharp - cos_sharpness)
    expo = expo * Vector3f(amp) * Vector3f(cos_amplitude)
    other = -ek.exp(-2.0 * umLength) + 1.0
    return (2.0 * np.pi * expo * other) / umLength

def stephen_hill_irradiance(normal, amp, sharp, axi):
    muDotN = ek.dot(normal, Vector3f(axi))
    lam = sharp
    c0 = 0.36
    c1 = 1.0 / (4.0 * c0)
    eml  = np.exp(- lam )
    em2l = eml * eml
    rl   = 1.0 / lam
    scale = 1.0 + 2.0 * em2l - rl
    bias  = (eml - em2l) * rl - em2l
    x  = np.sqrt(1.0 - scale)

    x0 = c0 * muDotN
    x1 = c1 * x
    n = x0 + x1
    y = ek.select((muDotN > 0.0), muDotN, Vector3f(0))

    y = ek.select( (ek.abs(x0)<=x1), n*n/x, y)
    # if (ek.abs(x0) <= x1):
    #     y = n * n / x

    result = scale * y + bias
    return Vector3f(result) * 2.0 * np.pi * (1.0 - np.exp(-2.0 * sharp)) * Vector3f(amp) / sharp #scale by integral of lighting



def integrator_sample(scene, sampler, rays, medium, active=True):
    si = scene.ray_intersect(rays)
    active_b = si.is_valid() & active

    ctx = BSDFContext()
    bsdf = si.bsdf(rays)
    bsdf_val = BSDF.eval_vec(bsdf, ctx, si, Vector3f([0.0,0.0,1.0]), active)
    # bsdf_val = Vector3f([0.159155, 0.159155, 0.159155])

    irr = stephen_hill_irradiance(si.n, lightSG[0][0:3], lightSG[0][3], lightSG[0][4:7])
    for l in range(len(lightSG) - 1):
        irr = irr + stephen_hill_irradiance(si.n, lightSG[l + 1][0:3], lightSG[l + 1][3], lightSG[l+1][4:7])

    result = ek.select(active_b, bsdf_val * irr, Vector3f(0))
    return result, si.is_valid(), ek.select(si.is_valid(), si.t, Float(0.0))


class SGIntegrator(SamplingIntegrator):
    def __init__(self, props):
        SamplingIntegrator.__init__(self, props)

    def sample(self, scene, sampler, ray, medium, active):
        result, is_valid, depth = integrator_sample(scene, sampler, ray, medium, active)
        return result, is_valid, [depth]

    def aov_names(self):
        return ["depth.Y"]

    def to_string(self):
        return "SGIntegrator[]"

# Register our integrator such that the XML file loader can instantiate it when loading a scene
register_integrator("sgintegrator", lambda props: SGIntegrator(props))
# Load an XML file which specifies "sgintegrator" as the scene's integrator
Thread.thread().file_resolver().append(os.path.dirname(filename))
scene = load_file(filename)
scene.integrator().render(scene, scene.sensors()[0])
film = scene.sensors()[0].film()
film.set_destination_file(savefile)
film.develop()