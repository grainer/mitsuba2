import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
import imageio
# imageio.plugins.freeimage.download() #Uncomment if the plugin to save EXR is not available on the system
import torch
from torchvision.utils import save_image

data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/atelier/311'
multiplier = 0.5 # 0.5 for atelier, 5 for kitchen, 5 bedroom, 10 for sanmiguel


data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/bedroom/450'
multiplier = 5 # 0.5 for atelier, 5 for kitchen, 5 bedroom, 10 for sanmiguel


out_dir = data_dir
if not os.path.exists(out_dir):
    os.makedirs(out_dir)
filenames = []
for r, d, f in os.walk(data_dir):  # r=root, d=directories, f = files
    f.sort()
    for file in f:
        # if 'AccumulatePass.output.0.exr' in file:
        # if 'Denoiser.output.0.exr' in file:
        if '.exr' in file:
            filenames.append(file)
            im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
            pr = torch.from_numpy(im)
            pr = pr.permute(2, 0, 1)
            pr = torch.clip(multiplier * pr, 0.0, 1.0)
            pr = pr ** (1.0 / 2.2)

            save_image(pr, out_dir + "/" + file[:-4] + ".png")
