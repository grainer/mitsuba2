import exr_helpers as ex
import matplotlib.pyplot as plt
import torch

groundtruthEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/envmaps-withsgs/pisa-gt.exr'
reconEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/envmaps-withsgs/pisa-sg.exr'
saveEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/envmaps-withsgs/pisa-comparison.jpg'



groundtruthEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene/newennis-sgenv-montecarlo.exr'
reconEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene/sg-diffuse.exr'
saveEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene/test-comparison.jpg'


groundtruthEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene/sg-diffuse-approx.exr'
reconEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene/sg-diffuse-exact.exr'
saveEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene/sg-comparison.jpg'

# groundtruthEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/cpurender-gt.exr'
# reconEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/cpurender-sgfit.exr'
# saveEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/cpurender-comparison.jpg'

groundtruthEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/cpurender-gt.exr' #'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/montecarlo-sgfit-cosineshading.exr'
#
reconEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/montecarlo-sgfit-shading.exr'
saveEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/gt-VS-montecarlo-fittedenv-sgcosine-comparison.jpg' #montecarlo-sgbsdfVScosine-comparison.jpg' #


groundtruthEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/sgprt-expinnerprod.exr' # 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/cpurender-gt.exr'
reconEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/sgprt-stephenhill.exr'
saveEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/stephenhill-vs-innerprod2.jpg'


groundtruthEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/pisa-cpugt.exr'#
reconEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/cpu-sgenvmapfit.exr'#pisa-sgprt-stephenhill.exr'
saveEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/pisa-cpusgenvmap-vs-cpugtenvmap.jpg'


groundtruthEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere/cpugt-ennis.exr'
reconEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere/cpu-sgenvmap-ennis.exr' #cpu-sgenvmap-pisa.exr'
saveEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere/ennis-sgenvmap-montecarlo-vs-gt.jpg'



# groundtruthEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere/lightfromtop-cpu.exr'
# reconEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere/lightfromtop-stephenhill.exr'
# saveEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere/lightfromtop-prt-vs-gt.jpg'

groundtruthEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere-glossy/cpu-pisaSG-r2.exr'#cpu-pisa-r02.exr' #
reconEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere-glossy/sg-pisa-anisowarp-r2.exr'# cpu-pisaSG-r02.exr' #
saveEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere-glossy/pisa-sg-anisowarp-vs-montecarlo-sgenvmap-r2.jpg'

groundtruthEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere-glossy/cpu-pisaSG-r02.exr'#cpu-ennisSG-r02.exr'#
reconEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere-glossy/sg-pisa-hybridwarp-r02.exr'#sg-ennis-hybridwarp-r02.exr'#
saveEXRpath = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere-glossy/pisa-sg-hybridwarp-vs-montecarlo-sgenvmap-r02.jpg'


path = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/results/learning-bunny/diffusebunny-alienware'
groundtruthEXRpath = path+'/diffuse-gt-highres.exr'
reconEXRpath = path+'/diffusenerf.exr'
saveEXRpath = path+'/diffusecomparison.jpg'

path = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/results/learning-bunny/diffusebunny-alienware'
groundtruthEXRpath = path+'/specular-gt-highres.exr'
reconEXRpath = path+'/spec400-8spp-loong2-25sg-noposenc-2x250-5dist-.exr'
saveEXRpath = path+'/spec-comparison-noposenc.jpg'


multiplier = 2.0
def compare(groundtruth_path, recon_path, save_path, mul=1.0):
    gt  = ex.readEXR_RGB(groundtruth_path)
    rec = ex.readEXR_RGB(recon_path)
    m = gt.max()
    gt = gt / m
    rec = rec/m
    diff = torch.abs(rec - gt)

    fig, axes = plt.subplots(1, 3, figsize=(18, 6))
    axes[0].imshow(mul * torch.clip(rec,  0.0, 1.0) **(1.0/2.2))
    axes[1].imshow(mul * torch.clip(gt,   0.0, 1.0) **(1.0/2.2))
    axes[2].imshow(mul * torch.clip(diff, 0.0, 1.0) **(1.0/2.2))
    plt.show()
    fig.savefig(saveEXRpath, bbox_inches='tight')
    plt.close(fig)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    compare(groundtruthEXRpath, reconEXRpath, saveEXRpath, multiplier)