import os
import numpy as np
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader
import sys
import os
import cv2
import nets
from torchvision.utils import save_image

import renderhelpers as sgr
import matplotlib.pyplot as plt

torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True

dropbox_path = 'F:/gilles/Dropbox/'
# Input paths and parameters ===========================================================================================
# train_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/train'
# test_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/test'
#
# aabb = [[-12.3, -0.0, -4.0], [-8.3, 2.0, 0.0]] # manually modified
#
# envmap_trainpath = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/train'
# envmap_testpath = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/test'
#
# pretrain_enc_path = None # 'C:/Users/grainer.AD/Dropbox/INRIA/projects/nprt/res/atelier/pretrained.pth' #
#
# highres_test_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/test'
# highres_envmap_testpath = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/test'


train_path = dropbox_path + 'INRIA/projects/nprt/data/scenes/atelier/train'
test_path = dropbox_path + 'INRIA/projects/nprt/data/scenes/atelier/test'

aabb = [[-146.994873, -59.336391, -170.382034], [146.993851, 85.836891, 113.362228]] # for atelier

envmap_trainpath = dropbox_path + 'INRIA/projects/nprt/data/scenes/indoorenvs/train'
envmap_testpath = dropbox_path + 'INRIA/projects/nprt/data/scenes/indoorenvs/test'

highres_test_path = dropbox_path + 'INRIA/projects/nprt/data/scenes/atelier/highres'
highres_envmap_testpath = dropbox_path + 'INRIA/projects/nprt/data/scenes/indoorenvs/highres'

outputpath = dropbox_path + 'INRIA/projects/nprt/res/pixelbasis'

experiment_name = 'bandlimitbrdf025-gaussianblur-posnorm-' #
# Training parameters ==================================================================================================
batch_size = 1# 10#0# 60#  5# 5# 24
resample_envmaps = True


num_angles = 128# 64 # 256


useSiren = True
max_epochs = 50000  # 10000
num_freqs = 0 # 10  # 0 # 0 means no positional encoding
netwidth = 128# 512#  64# 400#      250#      64#

lightinputdepth = 0
num_layers = 2#  4  # 4# 3#  4#    1#     5# 3#
learning_rate = 0.0001#  0.0001  # 0.0001#   0.005#   0.01# 0.5#
logloss = True# True  # False#
L2loss = False  # True#

exponentiate = True  # False#  True#
withnormals = True # True  # False
encode_normals = False # False # True

insize = 3 + 2 * 3 * num_freqs + 3 * withnormals
if encode_normals:
    num_freqs_normals = 3
    insize = insize + 2 * 3 * num_freqs_normals

subtract_direct = False  # False#    True
tensorboard_writes = 100# 500





experiment_name = experiment_name + '-useSiren-' + str(useSiren) + '-angles-' + str(num_angles) + '-layer-' + str(
                    num_layers) + '-width-' + str(netwidth) + '-lr-' + str(learning_rate)
# Other parameters (hardcoded)
num_workers = 0
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
if not os.path.exists(outputpath):
    os.makedirs(outputpath)
boundingboxmin = torch.tensor(aabb[0]).unsqueeze(0).unsqueeze(0)
boundingboxsize = torch.tensor(aabb[1]).unsqueeze(0).unsqueeze(0) - boundingboxmin

# Get the angular directions to sample:
sample_angles = torch.zeros([num_angles, 2])
sampling_directions = sgr.fibonacci_sphere(num_angles)#+1)[0:num_angles,:]
print(sampling_directions)
for i in range(num_angles):
    # TODO : check the envmap is the right way around. theta definitely needs to be negated, dunno about phi though
    # uv.x = atan2(p.x, -p.z) * M_1_2PI + 0.5
    # uv.y = acos(p.y) * M_1_PI;

    # theta = torch.asin( - sampling_directions[i,1])
    # phi = torch.atan2(sampling_directions[i,2],sampling_directions[i,0])
    # sample_angles[i, 0] = (theta / np.pi + 0.5) * 127
    # sample_angles[i, 1] = (phi / np.pi + 1.0) * 127
    theta = - torch.asin(sampling_directions[i,1]) / (np.pi / 2.0)
    phi = torch.atan2(sampling_directions[i,0], -sampling_directions[i,2]) / np.pi
    sample_angles[i,0] = (theta + 1.0) / 2.0 * 127 % 128
    sample_angles[i,1] = (phi + 1.0) / 2.0 * 255 % 256
print(sample_angles)

class BufferDataset(Dataset):
    def __init__(self, renderpath, envpath):
        self.alpha = []
        self.diffuse = []
        self.specular = []
        self.envmap = []
        self.gt = []
        self.normals = []
        self.position = []
        self.viewdir = []

        self.envfiles = []

        kw = int(256.0 / (num_angles ** 0.66))
        # kernel = np.ones((kw, kw), np.float32) / (kw*kw)
        # im = cv2.filter2D(im, -1, kernel)
        kw = int(256.0 / (num_angles ** 0.66) / 2.0) #* 2 + 1
        if kw%2==0:
            kw+=1

        # kw = kw * 2 + 1
        print("filtering envmaps with kernel of width ", kw)
        for r, d, f in os.walk(envpath):  # r=root, d=directories, f = files
            for file in f:
                if '.exr' in file:
                    self.envfiles.append(file)
                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH),
                                      cv2.COLOR_RGB2BGR)

                    if resample_envmaps:
                        im = cv2.GaussianBlur(im, (kw*5, kw*5), kw)
                        im = torch.from_numpy(im)
                        envsamples = torch.zeros([num_angles, 3])
                        for a in range(num_angles):
                            envsamples[a, 0] = im[int(sample_angles[a,0]), int(sample_angles[a,1]), 0]
                            envsamples[a, 1] = im[int(sample_angles[a,0]), int(sample_angles[a,1]), 1]
                            envsamples[a, 2] = im[int(sample_angles[a,0]), int(sample_angles[a,1]), 2]
                            im[int(sample_angles[a, 0]), int(sample_angles[a, 1]), :] = 0.0
                        # plt.imshow(im)
                        # plt.show()
                        self.envmap.append(envsamples)
                    else:
                        im = torch.from_numpy(im)
                        self.envmap.append(im)

        c = 0
        for r, d, f in os.walk(renderpath):  # r=root, d=directories, f = files
            for file in f:
                if '.exr' in file:
                    if c % 50 == 0:
                        print('Read ', c, ' EXR buffers.')

                    filename = os.path.splitext(os.path.basename(file))[0]
                    fname = filename.split('.')
                    if int(fname[0]) == c:
                        c = c+1

                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(im)

                    if fname[2] == 'posW':
                        im = (im - boundingboxmin.expand(im.shape[0], im.shape[1], -1)) / boundingboxsize.expand(im.shape[0], im.shape[1], -1)
                        im = (im-0.5) * 2
                        self.position.append(im)
                    if fname[2] == 'alpha':
                        self.alpha.append(im)
                    if fname[2] == 'diffuseOpacity':
                        self.diffuse.append(im / np.pi)
                    if fname[2] == 'output':
                        self.gt.append(im)
                    if fname[2] == 'normW':
                        self.normals.append(im)
                    if fname[2] == 'specShading':
                        self.specular.append(im)
                    if fname[2] == 'viewW':
                        self.viewdir.append(im)

    def __len__(self):
        return len(self.gt)

    def __getitem__(self, idx):
        if resample_envmaps:
            env = self.envmap[idx % len(self.envmap)].view(1,-1)
        else:
            env = self.envmap[idx % len(self.envmap)].permute(2,0,1)
            if len(env.shape)==3:
                env = env.unsqueeze(0)
        pos = self.position[idx]
        alpha = self.alpha[idx]
        diff = self.diffuse[idx]
        gt = self.gt[idx]
        normals = self.normals[idx]
        specular = self.specular[idx]
        viewdir = self.viewdir[idx]
        specular = torch.clamp(specular, min = 0.0, max = 10000.0)
        return alpha, pos, normals, viewdir, diff, specular, gt, env

print('Load training data.')
train_dataset = BufferDataset(train_path, envmap_trainpath)
print('Load validation data.')
val_dataset = BufferDataset(test_path, envmap_testpath)
print('Training data loaded.')
train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True) # , num_workers=10) # , pin_memory=True, drop_last=True)
val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=True) # , pin_memory=True, drop_last=True)

if resample_envmaps:
    net = nets.PixelenvNet(netwidth, num_layers, angles=num_angles, useSiren=useSiren, insize=insize).to(device=gpudevice)
else:
    net = nets.CompressedPixelenvNet(netwidth, num_layers, angles=num_angles, useSiren=useSiren, insize=insize).to(device=gpudevice)
print(net)

optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
writer = SummaryWriter(outputpath + '/runs/' + experiment_name)  # logloss-l1-adam-5e-3-testFaceNormals')
test_example = iter(val_dataloader)
iteration = 0


def render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap,
                        gpudevice, net, directions, num_freqs, withnormals, timings=False, encode_normals=False):
    # flatten everything
    mask = (torch.sqrt(torch.sum(normals**2, dim=-1, keepdim=True)).expand(-1,-1,3) > 0.0)  # to make sure, remove background
    gtcpu[~mask] = 0.0
    mask = mask[:,:,0].view(-1)

    pos3d = pos3d.view(-1, 3)
    normals = normals.view(-1, 3)
    diffuse_bsdf_val = diffuse_bsdf_val.view(-1, 3)
    trans_nrj = alpha[:, :, 2].view(-1).unsqueeze(1).expand(-1, 3)
    diffuse_bsdf_val = diffuse_bsdf_val * (1.0 - trans_nrj)
    specular_bsdf_val = specular_bsdf_val.view(-1, 3)
    alpha = alpha[:, :, 0].view(-1)
    wi = wi.view(-1, 3)

    pos3d = pos3d[mask, :]
    pred = torch.zeros(pos3d.shape).to(device=gpudevice)
    normals = normals[mask, :]
    diffuse_bsdf_val = diffuse_bsdf_val[mask, :]
    specular_bsdf_val = specular_bsdf_val[mask, :]
    wi = wi[mask, :]
    alpha = alpha[mask]

    start = torch.cuda.Event(enable_timing=True)
    end = torch.cuda.Event(enable_timing=True)

    envinp = envmap.view(1,-1)
    envinp = envinp.expand(pos3d.shape[0], -1)
    num_directions = directions.shape[0]
    netin = torch.cat((pos3d, normals), dim=1)
    if timings:
        start.record()

    netout = net(envinp.to(device=gpudevice), netin.to(device=gpudevice)).view(-1, num_directions, 3)
    # netout = envinp.view(-1, num_directions, 3)
    inc_light = torch.nn.functional.relu(netout)

    if timings:
        end.record()
        torch.cuda.synchronize()
        print("Time to evaluate network:  ", start.elapsed_time(end))
    assert (inc_light != inc_light).any() == False, "Damn! inc_light contains NaNs"

    if timings:
        start.record()
    # SHADE DIFFUSE
    diffuseim = torch.zeros(pos3d.shape)
    mask_diffuse = (torch.sum(diffuse_bsdf_val * diffuse_bsdf_val, dim=-1, keepdim=False) > 0.0)
    diffnormals = normals[mask_diffuse, :].to(device=gpudevice).detach()
    for dir in range(num_directions):
        with torch.no_grad():
            lightdir = directions[dir, :].view(1, 3).to(device=gpudevice)
            n_dot_l = torch.nn.functional.relu(sgr.dot(diffnormals, lightdir))
        pred[mask_diffuse, :] += n_dot_l * inc_light[mask_diffuse, dir, :]
    pred[mask_diffuse, :] *= diffuse_bsdf_val[mask_diffuse, :].to(device=gpudevice)
    diffuseim[mask_diffuse, :] = diffuseim[mask_diffuse, :] + pred[mask_diffuse, :].cpu()

    # # SAMPLING
    # valid_samples = torch.zeros(pos3d.shape)
    # for dir in range(num_directions):
    #     n_dot_l = torch.nn.functional.relu(sgr.dot(normals, directions[dir, :].view(1, 3)))
    #     valid_samples = valid_samples + (n_dot_l > 0.0)
    # valid_samples = valid_samples / (2.0 * np.pi) # MC uniform integration

    # SHADE Specular
    mask_specular = (torch.sum(specular_bsdf_val * specular_bsdf_val, dim=-1, keepdim=False) > 0.0)
    specim = torch.zeros(pos3d.shape)
    with torch.no_grad():
        n = normals[mask_specular, :].to(device=gpudevice)
        wo = wi[mask_specular, :].to(device=gpudevice)
        # n_dot_v = torch.sum(n * wo, dim=-1, keepdim=True).view(-1, 1)
        a2 = (alpha[mask_specular] * alpha[mask_specular]).view(-1, 1).to(device=gpudevice)
        specshade = specular_bsdf_val[mask_specular, :].to(device=gpudevice)

        # bandlimit the brdf
        a2 = torch.clamp(a2, 0.025)

    for dir in range(num_directions):
        with torch.no_grad():
            lightdir = directions[dir,:].view(1,3).to(device=gpudevice)
            hnorm = torch.sqrt(torch.sum((wo + lightdir)**2, dim=-1, keepdim=True))
            h = (wo + lightdir) / hnorm

            cosTheta = torch.sum(n * h, dim=-1, keepdim=True).view(-1, 1)
            d = ((cosTheta * a2 - cosTheta) * cosTheta + 1.0)
            D = a2 / (d * d * np.pi)

            n_dot_l = torch.sum(n * lightdir, dim=-1, keepdim=True).view(-1, 1)
            # woDotH = torch.sum(wo * h, dim=-1, keepdim=True).view(-1, 1)
            # visible_l = (n_dot_l > 0)
            # tanThetaSqr = torch.nn.functional.relu(n_dot_l)
            # tanThetaSqr[visible_l] = torch.nn.functional.relu((1.0 - n_dot_l[visible_l] * n_dot_l[visible_l]) / (n_dot_l[visible_l] * n_dot_l[visible_l]))
            # lambdaI = 0.5 * (-1 + torch.sqrt(1 + a2 * tanThetaSqr))
            # tanThetaSqr2 = torch.nn.functional.relu((1.0 - n_dot_v * n_dot_v) / (n_dot_v * n_dot_v))
            # lambdaO = 0.5 * (-1 + torch.sqrt(1 + a2 * tanThetaSqr2))
            # G = 1 / ((1 + lambdaI) * (1 + lambdaO))
            #
            # F = specular_bsdf_val[mask_specular, :] + (1.0 - specular_bsdf_val[mask_specular, :]) * torch.nn.functional.relu(1 - woDotH)**5
            #
            # bsdf_spec = F * D * G * 0.25 / n_dot_v
            # bsdf_spec[(n_dot_l <= 0).expand(-1,3)] = 0.0

            bsdf_spec = specshade * D *  torch.nn.functional.relu(n_dot_l)

        specirr = bsdf_spec.to(device=gpudevice) * inc_light[mask_specular, dir, :]
        pred[mask_specular, :] += specirr
        specim[mask_specular, :] = specim[mask_specular, :] + specirr.cpu()

    if timings:
        end.record()
        torch.cuda.synchronize()
        print("Time to shade:  ", start.elapsed_time(end))

    # # SHADE DIFFUSE
    # mask_diffuse = (torch.sum(diffuse_bsdf_val * diffuse_bsdf_val, dim=-1, keepdim=False) > 0.0)
    # num_diffuse = torch.sum(mask_diffuse)
    # lightdir = directions.view(1, num_directions, 3).to(device=gpudevice)
    #
    # n_dot_l = torch.sum(nn[mask_diffuse, :].view(num_diffuse, 1, 3) * lightdir, dim=-1, keepdim=True).expand(-1, -1, 3)
    # diffuse_component = torch.sum(n_dot_l * inc_light[mask_diffuse, :] * diffuse_bsdf_val[mask_diffuse, None, :].to(device=gpudevice), dim=1)
    # pred[mask_diffuse, :] = pred[mask_diffuse, :] + diffuse_component
    #
    # # SHADE Specular
    # mask_specular = (torch.sum(specular_bsdf_val * specular_bsdf_val, dim=-1, keepdim=False) > 0.0)
    # num_specular = torch.sum(mask_specular)
    # n = nn[mask_specular, :].unsqueeze(1).expand(-1, num_directions, -1)
    #
    # wo = wi[mask_specular, :].unsqueeze(1).expand(-1, num_directions, -1).to(device=gpudevice)
    # l = directions.unsqueeze(0).expand(num_specular, -1, 3).to(device=gpudevice)
    # # h = sgr.normalize(wo + l)
    # hnorm = torch.sqrt(torch.sum((wo + l)**2, dim=-1, keepdim=True))
    # h = (wo + l) / hnorm
    #
    # n_dot_l = torch.sum(n * l, dim=-1, keepdim=True).view(num_specular, num_directions, 1)
    # n_dot_v = torch.sum(n * wo, dim=-1, keepdim=True).view(num_specular, num_directions, 1)
    # woDotH = torch.sum(wo * h, dim=-1, keepdim=True).view(num_specular, num_directions, 1)
    #
    # a2 = (alpha[mask_specular] * alpha[mask_specular]).view(-1,1,1).to(device=gpudevice)
    # cosTheta = h[:,:,2].view(num_specular, num_directions, 1)
    # d = ((cosTheta * a2 - cosTheta) * cosTheta + 1.0)
    # D = a2 / (d * d * np.pi)
    #
    # visible_l = (n_dot_l > 0)
    # visible_n_dot_l = n_dot_l[visible_l]
    # tanThetaSqr = torch.nn.functional.relu((1.0 - visible_n_dot_l * visible_n_dot_l) / (visible_n_dot_l * visible_n_dot_l))
    # lambdaI = 0.5 * (-1 + torch.sqrt(1 + a2 * tanThetaSqr))
    # tanThetaSqr2 = torch.nn.functional.relu((1.0 - n_dot_v * n_dot_v) / (n_dot_v * n_dot_v))
    # lambdaO = 0.5 * (-1 + torch.sqrt(1 + a2 * tanThetaSqr2))
    # G = 1 / ((1 + lambdaI) * (1 + lambdaO))
    #
    # F = specular_bsdf_val[mask_specular, None, :].to(device=gpudevice) + (1.0 - specular_bsdf_val[mask_specular, :].to(device=gpudevice)) \
    #        * torch.nn.functional.relu(1 - woDotH)**5
    #
    # bsdf_spec = F * D * G * 0.25 / n_dot_v
    # specular_component = torch.sum(bsdf_spec * inc_light[mask_specular, :], dim=1)
    # pred[mask_specular, :] = pred[mask_specular, :] + specular_component


    return pred * 4.0 * np.pi / num_angles, gtcpu.view(-1, 3)[mask, :], diffuseim* 4.0 * np.pi / num_angles, specim* 4.0 * np.pi / num_angles


# with torch.autograd.detect_anomaly():

for epoch in range(max_epochs):
    epoch_loss = 0.0

    for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in train_dataloader:

        optimizer.zero_grad()
        batch_loss = 0.0
        groundtruth, pred_render = [], []

        for b in range(alpha_batch.shape[0]):
            pred, gtcpu2, _, _ = render_from_buffers(alpha_batch[b], pos3d_batch[b], normals_batch[b], wi_batch[b],
                                                 diffuse_bsdf_val_batch[b], specular_bsdf_val_batch[b], gtcpu_batch[b],
                                                envmap_batch[b],
                                                gpudevice, net, sampling_directions, num_freqs, withnormals,
                                                timings=False, encode_normals=encode_normals)
            groundtruth.append(gtcpu2)
            pred_render.append(pred)

        groundtruth = torch.cat(groundtruth, dim=0).to(gpudevice)
        pred_render = torch.cat(pred_render, dim=0)
        loss = sgr.my_loss(pred_render, groundtruth, tonemap=False, clip=False, log=logloss, L2=L2loss)

        batch_loss += loss.item()
        loss.backward()
        optimizer.step()
        writer.add_scalar('training loss', batch_loss, iteration)
        epoch_loss = epoch_loss + batch_loss / len(train_dataloader)

        with torch.no_grad():
            if iteration % tensorboard_writes == 0:
                try:
                    alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch = test_example.next()
                except StopIteration:
                    test_example = iter(val_dataloader)
                    alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch = test_example.next()

                alpha = alpha_batch[0]  # .to(gpudevice)
                pos3d = pos3d_batch[0]  # .to(gpudevice)
                normals = normals_batch[0]  # .to(gpudevice)
                wi = wi_batch[0]  # .to(gpudevice)
                diffuse_bsdf_val = diffuse_bsdf_val_batch[0]  # .to(gpudevice)
                specular_bsdf_val = specular_bsdf_val_batch[0]  # .to(gpudevice)
                gtcpu = gtcpu_batch[0]  # .to(gpudevice)
                envmap = envmap_batch[0]

                pred, gtcpu2, diffim, specim = render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap,
                                                gpudevice, net, sampling_directions, num_freqs, withnormals,
                                                timings=True, encode_normals=encode_normals)

                alpha = alpha[:, :, 0].view(-1)
                mask = (torch.sqrt(torch.sum(normals ** 2, dim=-1, keepdim=False)) > 0.0).view(-1)
                pr = torch.zeros(gtcpu.size())
                pr = pr.view(-1, 3)
                pr[mask, :] = pred.cpu()
                pr = pr.view(gtcpu.shape)

                gtcpu = torch.zeros(normals.size())
                gtcpu = gtcpu.view(-1, 3)
                gtcpu[mask, :] = gtcpu2.cpu()
                gtcpu = gtcpu.view(normals.shape)

                diff = torch.zeros(normals.size())
                diff = diff.view(-1, 3)
                diff[mask, :] = diffim.cpu()
                diff = diff.view(normals.shape)
                spec = torch.zeros(normals.size())
                spec = spec.view(-1, 3)
                spec[mask, :] = specim.cpu()
                spec = spec.view(normals.shape)

                static_spec = torch.zeros(gtcpu.shape)
                static_spec = static_spec + specular_bsdf_val.cpu()
                static_spec = static_spec.permute(2, 0, 1)
                static_diff = torch.zeros(gtcpu.shape)
                static_diff = static_diff + diffuse_bsdf_val.cpu()
                static_diff = static_diff.permute(2, 0, 1)

                gtcpu = gtcpu.permute(2, 0, 1)
                pr = pr.permute(2, 0, 1)
                diff = diff.permute(2, 0, 1)
                spec = spec.permute(2, 0, 1)
                difference = torch.abs(pr - gtcpu)

                # print(gtcpu.shape, pr.shape, diff.shape)
                print('maxs and mins  ', gtcpu.max(), pr.max(), diff.max(), gtcpu.min(), pr.min(), diff.min())
                print('diffuse minmax : ', diff.min(), diff.mean(), diff.max())
                print('specular minmax : ', spec.min(), spec.mean(), spec.max())
                img_grid = torchvision.utils.make_grid([static_diff / static_diff.mean() * pr.mean(), static_spec / static_diff.mean() * pr.mean(), diff, spec, pr, gtcpu, difference])
                img_grid = img_grid / gtcpu.max()
                img_grid = img_grid ** (1.0 / 2.2)
                img_grid = torch.clip(1.5 * img_grid, 0.0, 1.0)

                writer.add_image('static vs predictions vs. GT', img_grid, global_step=iteration)
        iteration = iteration + 1
    writer.add_scalar('Epoch loss (training)', epoch_loss, epoch)

    with torch.no_grad():  # Loop over the test data:
        val_loss = 0.0
        for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in val_dataloader:
            pred_render, groundtruth, _,_ = render_from_buffers(alpha_batch[0], pos3d_batch[0], normals_batch[0], wi_batch[0],
                                                 diffuse_bsdf_val_batch[0], specular_bsdf_val_batch[0], gtcpu_batch[0],
                                                 envmap_batch[0],
                                                 gpudevice, net, sampling_directions, num_freqs, withnormals,
                                                 timings=False, encode_normals = encode_normals)
            loss = sgr.my_loss(pred_render, groundtruth.to(gpudevice), tonemap=False, clip=False, log=logloss, L2=L2loss)
            val_loss += loss.item() / len(val_dataloader)

        writer.add_scalar('Epoch loss (validation)', val_loss, epoch)

        # TODO : save network and run it on the high-res testing images
        torch.save(net.state_dict(), outputpath + "/" + experiment_name + "-net.pth")

        # TODO : save network and run it on the high-res testing images
        dataset = BufferDataset(highres_test_path, highres_envmap_testpath)
        dataloader = DataLoader(dataset, batch_size=1, shuffle=False)  # , pin_memory=True, drop_last=True)
        imcount = 0
        for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in dataloader:
            pred_render, groundtruth,  _,_ = render_from_buffers(alpha_batch[0], pos3d_batch[0], normals_batch[0], wi_batch[0],
                                                 diffuse_bsdf_val_batch[0], specular_bsdf_val_batch[0], gtcpu_batch[0],
                                                 envmap_batch[0],
                                                 gpudevice, net, sampling_directions, num_freqs, withnormals,
                                                 timings=False, encode_normals = encode_normals)
            alpha = alpha_batch[0]  # .to(gpudevice)
            pos3d = pos3d_batch[0]  # .to(gpudevice)
            normals = normals_batch[0]  # .to(gpudevice)
            wi = wi_batch[0]  # .to(gpudevice)
            diffuse_bsdf_val = diffuse_bsdf_val_batch[0]  # .to(gpudevice)
            specular_bsdf_val = specular_bsdf_val_batch[0]  # .to(gpudevice)
            gtcpu = gtcpu_batch[0]  # .to(gpudevice)
            # envmap = envmap_batch[0].permute(2, 0, 1)
            alpha = alpha[:, :, 0].view(-1)
            mask = (torch.sqrt(torch.sum(normals ** 2, dim=-1, keepdim=False)) > 0.0).view(-1)
            pr = torch.zeros(gtcpu.size())
            pr = pr.view(-1, 3)
            pr[mask, :] = pred_render.cpu()
            pr = pr.view(gtcpu.shape)
            gtcpu = torch.zeros(normals.size())
            gtcpu = gtcpu.view(-1, 3)
            gtcpu[mask, :] = groundtruth.cpu()
            gtcpu = gtcpu.view(normals.shape)
            gtcpu = gtcpu.permute(2, 0, 1)
            pr = pr.permute(2, 0, 1)
            diff = torch.abs(pr - gtcpu)
            img_grid = torchvision.utils.make_grid([pr, gtcpu, diff])
            img_grid = img_grid / gtcpu.max()
            img_grid = img_grid ** (1.0 / 2.2)
            img_grid = torch.clip(1.2 * img_grid, 0.0, 1.0)
            save_image(img_grid, outputpath + "/"+ experiment_name + "-" + str(imcount) + ".jpg")
            imcount = imcount + 1
        print('done rendering highres ')

print('Finished Training')