import os
import numpy as np
import enoki as ek
import mitsuba
mitsuba.set_variant('gpu_autodiff_rgb')
from mitsuba.core import Thread, math, Properties, Frame3f, Float, Vector3f, warp
from mitsuba.core.xml import load_file, load_string
from mitsuba.render import BSDF, BSDFContext, BSDFFlags, BSDFSample3f, SurfaceInteraction3f, register_bsdf, Texture

# TODO: Write the sample method for monte carlo SG rendering
# TODO: write a SG_shade method for SG integration against light
# controls
approximate_with_SG = True # False
monte_carlo_integration = True
filename = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/diffusescene.xml'
savefilename = 'C:/Users/grainer.AD/Documents/GitHub/data/bunny-testscene2/pisa-montecarlo-sgfit-shading.exr'

# lightSG =   [[135.05815, 87.43797, 47.75562, 1515.4696, -0.42767950031390883, 0.04761128078180294, 0.9026756953377894], [50.14956, 47.56903, 41.903664, 82.51134, 0.1188687729982647, 0.1329609662429467, 0.983967273978982], [4.544524, 3.5510426, 2.8634553, 140.34607, 0.9634177431881418, -0.10265880798907884, -0.24756296421825522], [10.713409, 14.290048, 23.320549, 101.34265, 0.14520827386230734, 0.4182330396268862, 0.8966580629015637], [13.391979, 34.240696, 40.864845, 106.25836, -0.19853306335481213, 0.15922916085094505, 0.9670732635584844], [0.33066732, 0.17681901, 0.10012947, 2.1902213, 0.6632462137699505, -0.321697476465291, -0.6757330786306739], [65.358925, 30.235605, 4.7732644, 146.72334, -0.21116821482775677, 0.07267644493469873, 0.9747441302198808], [5.500468, 4.226616, 3.2763305, 127.57822, -0.8985842989334655, -0.09573201783281793, -0.4282308238227481], [20.547792, 23.387177, 33.75418, 30.215794, -0.13977603488310686, 0.38790274926846535, 0.9110401292930639], [94.99076, 90.8476, 79.2127, 1517.9126, -0.42148598674208637, 0.14112080935348353, 0.8957870729964055], [1.413883, 1.1812625, 1.0005144, 39.44321, -0.7848995904317928, -0.3304012571663567, -0.5241828328006317], [1.6185048, 1.3008134, 1.0011877, 11.492917, 0.2280196137131206, -0.4789549201099977, 0.8477082282628529]]
# pisa:
lightSG = [[0.9433718, 0.37715107, 0.030528598, 35.069885, -0.84324276, 0.2121104, 0.4939137], [0.70375603, 0.3655845, 0.15851796, 23.585962, 0.52402025, 0.7886307, -0.32165867], [0.08567178, 0.06427135, 0.044218015, 1.1091332, -0.2356962, -0.66532904, -0.70836747], [0.6558805, 0.31723064, 0.11005787, 21.001013, -0.60743445, 0.73271066, -0.30685267], [0.86364007, 0.35598388, 0.11529839, 22.24271, 0.46149436, 0.2683993, -0.8455677], [1.1314801, 1.2558013, 1.2232834, 41.10061, 0.053204477, 0.256633, 0.9650434], [1.0217043, 0.4285945, 0.13063939, 15.990939, -0.047010783, 0.8666322, -0.49672785], [0.20844299, 0.3255447, 0.49083838, 5.3000503, -0.44954064, 0.51849866, 0.72737354], [1.158383, 1.5155687, 1.7711425, 46.52204, 0.38086146, 0.38595143, 0.84022975], [2.3821747, 2.6763604, 2.6895938, 227.89229, 0.6516599, 0.27348915, 0.7074907], [0.66085684, 0.31836376, 0.1304988, 49.41547, 0.9093498, 0.18544276, -0.3724163], [0.7925673, 1.0842797, 1.393003, 14.961501, 0.7191419, 0.51414, 0.46743447]]


class DiffuseSG(BSDF):
    def __init__(self, props):
        BSDF.__init__(self, props)
        # diffuse_bsdf = load_string("<bsdf version='2.0.0' type='diffuse'></bsdf>")

        self.m_reflectance \
            = load_string('''<spectrum version='2.0.0' type='srgb' name="reflectance">
                                <rgb name="color" value="0.5, 0.5, 0.5"/>
                             </spectrum>''')
        self.m_flags = BSDFFlags.DiffuseReflection | BSDFFlags.FrontSide
        self.m_components = [self.m_flags]

        # cosine lobe SG approximation
        self.sharpness = 2.133
        self.amplitude = 1.17


    def sample(self, ctx, si, sample1, sample2, active):
        cos_theta_i = Frame3f.cos_theta(si.wi)
        active &= cos_theta_i > 0

        bs = BSDFSample3f()
        bs.wo = warp.square_to_cosine_hemisphere(sample2)
        bs.pdf = warp.square_to_cosine_hemisphere_pdf(bs.wo)
        bs.eta = 1.0
        bs.sampled_type = +BSDFFlags.DiffuseReflection
        bs.sampled_component = 0

        value = self.m_reflectance.eval(si, active)
        return ( bs, ek.select(active & (bs.pdf > 0.0), value, Vector3f(0)) )

    def eval_cosineSG(self, cos_theta):
        return self.amplitude * ek.exp((cos_theta - 1.0)* self.sharpness)

    def eval_cosSGinnerProduct(self, normal, amp, sharp, axi):
        um = self.sharpness * normal
        um = um + sharp * Vector3f(axi)
        umLength = ek.norm(um)
        # print(umLength)
        # print('--------------')

        expo = ek.exp(umLength - sharp - self.sharpness)
        expo = expo * Vector3f(amp) * Vector3f(self.amplitude)
        other =  -ek.exp(-2.0 * umLength) + 1.0

        return (2.0 * np.pi * expo * other) / umLength



    def eval(self, ctx, si, wo, active):
        if not ctx.is_enabled(BSDFFlags.DiffuseReflection):
            return Vector3f(0)

        cos_theta_i = Frame3f.cos_theta(si.wi)
        cos_theta_o = Frame3f.cos_theta(wo)
        value = self.m_reflectance.eval(si, active) * math.InvPi

        print(value)

        if monte_carlo_integration:
            if approximate_with_SG:
                value = value * self.eval_cosineSG(cos_theta_o)
            else:
                value = value * cos_theta_o
        else:
            irr = self.eval_cosSGinnerProduct(si.n, lightSG[0][0:3], lightSG[0][3], lightSG[0][4:7])
            for l in range(len(lightSG)-1):
                irr = irr + self.eval_cosSGinnerProduct(si.n, lightSG[l+1][0:3], lightSG[l+1][3], lightSG[l+1][4:7])
            value = value * irr

        return ek.select((cos_theta_i > 0.0) & (cos_theta_o > 0.0), value, Vector3f(0))


    def pdf(self, ctx, si, wo, active):
        if not ctx.is_enabled(BSDFFlags.DiffuseReflection):
            return Vector3f(0)

        cos_theta_i = Frame3f.cos_theta(si.wi)
        cos_theta_o = Frame3f.cos_theta(wo)

        pdf = warp.square_to_cosine_hemisphere_pdf(wo)

        return ek.select((cos_theta_i > 0.0) & (cos_theta_o > 0.0), pdf, 0.0)

    def to_string(self):
        return "DiffuseBSDF[reflectance = %s]".format(self.m_reflectance.to_string())

# Register our BSDF such that the XML file loader can instantiate it when loading a scene
register_bsdf("diffusesg", lambda props: DiffuseSG(props))
Thread.thread().file_resolver().append(os.path.dirname(filename))
scene = load_file(filename)
scene.integrator().render(scene, scene.sensors()[0])
film = scene.sensors()[0].film()
film.set_destination_file(savefilename)
film.develop()