import os
import numpy as np
import enoki as ek
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader
import sys
import os

sys.path += ['C:/Users/grainer.AD/Documents/GitHub/renderers/mitsuba2/build/dist/python']
os.environ['PATH'] += os.pathsep + 'C:/Users/grainer.AD/Documents/GitHub/renderers/mitsuba2/build/dist'

import mitsuba
# Set the desired mitsuba variant (MUST BE THE SAME IN ALL PYTHON FILES !!!)
import nets

mitsuba.set_variant('gpu_rgb')  # 'gpu_autodiff_rgb')
from mitsuba.core import Float, UInt32, Vector2f, Vector3f, Thread, Frame3f, xml, ScalarTransform4f, Bitmap, Struct
from mitsuba.core.xml import load_file, load_dict
from mitsuba.render import (BSDF, BSDFContext, BSDFFlags,
                            BSDFSample3f, SurfaceInteraction3f, register_bsdf,
                            DirectionSample3f, Emitter, ImageBlock,
                            SamplingIntegrator, has_flag,
                            register_integrator, reflect)
from mitsuba.python.util import traverse
import renderhelpers as sgr
import matplotlib.pyplot as plt

torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True

# Input paths and parameters ===========================================================================================
scenepath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/dynamic-envmap-bunny'
outputpath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/results/dynamic-allmaterials/indirect'
envmap_dir = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/envmaps/IndoorHDRDataset2018-256x128'
# envmap_dir = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/render-assets' #
scenename_gt = 'aovs-all.xml'


scenepath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/scenes/living-room'
outputpath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/results/living-room'
envmap_dir = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/envmaps/OutdoorHDR-256x128'
scenename_gt = 'scene-gilles.xml'
# scenename_gt = 'scene-gilles-brighter.xml'
scenename_gt_direct = 'scene-gilles-brighter-direct.xml'

# Training parameters ==================================================================================================
max_epochs = 50000  # 10000
num_freqs = 6  # 0 # 0 means no positional encoding
latentdim = 40 #8
# 72# 72#    128# 72#  350# #128#   512#
numSGs = 25#  25
# 25#  25#      50#
cnndim = 2048  # 96# 512# 2048#  4096#
netwidth = 256#  512  #     64# 400#      250#      64#

batch_size = 3
# 10#0# 60#  5# 5# 24

virtual_batchsize = 10# 10 # number of minibatches per virtual batch

num_layers = 4  # 4# 3#  4#    1#     5# 3#
learning_rate = 0.0001#  0.0001  # 0.0001#   0.005#   0.01# 0.5#
logloss = True  # False#
L2loss = False  # True#

imsize = 100# 100
# 400# 256#
exponentiate = True  # False#  True#

testres = 100

withnormals = True
# False
insize = 3 + 2 * 3 * num_freqs + 3 * withnormals

experiment_name = 'nerfin-reducedviewspace-nobiaslast2'
# Number of parameters in the encoder : 93466
# Number of parameters in the decoder : 237568

alternate_parametrization = False
# False #   True
subtract_direct = False
# False#    True
spp = 128  #

# TODO : try different deparametrizations eg square roughness
# TODO: Try applying mean and std to sharpness too. Try a more agressive remapping than tanh for theta phi
# TODO : try nerf initialisation too, different latent and net sizes
# TODO : why is the predicted envmap not black
# TODO : why are all RGB and sharpness values the same
# TODO: remove RGB augmentation in envmap test set
# TODO : normalize positions by bounding box
# TODO : try max-mean for normalizing sharpness instead of std


experiment_name = experiment_name + '-batchsize-' + str(batch_size) + '-lats-' + str(latentdim) + '-layers-' + str(
    num_layers) + '-width-' + str(netwidth) \
                  + '-numSG-' + str(numSGs) + '-lr-' + str(learning_rate) + '-freq-' + str(num_freqs) \
                  + '-log-' + str(int(logloss)) + '-L2-' + str(int(L2loss)) + '-withnormals-' + str(int(withnormals))
# Other parameters (hardcoded)
num_workers = 0
hybridanis = False  #
anisowarp = True
# False
sharpness_thresh = 5  # for the anisotropic NDF warp
cos_sharpness = 2.13
cos_amplitude = 1.17
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
if not os.path.exists(outputpath):
    os.makedirs(outputpath)
# Load the scene XMLs
Thread.thread().file_resolver().append(scenepath)
scene_gt = load_file(scenepath + '/' + scenename_gt)

if subtract_direct:
    scene_gt_direct = load_file(scenepath + '/' + scenename_gt_direct)

# Load envmaps, DO the train validation data split on the filenames
exr_files = []
for r, d, f in os.walk(envmap_dir):  # r=root, d=directories, f = files
    for file in f:
        if '.exr' in file:
            exr_files.append(os.path.join(r, file))
print(len(exr_files), ' EXR files found')
idxs = np.arange(len(exr_files))
np.random.shuffle(idxs)
cutoff = int(0.8 * len(exr_files))
exr_files = np.array(exr_files)
files_train = exr_files[idxs[0:cutoff]]
files_val = exr_files[idxs[cutoff:]]
print(len(files_train), ' training files')
print(len(files_val), ' testing files')
print('Load training data.')
train_dataset = sgr.EnvmapDataset(files_train, True, False)
print('Load validation data.')
val_dataset = sgr.EnvmapDataset(files_val, False, False)
print('Training data loaded.')
train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=num_workers)
# , pin_memory=True, drop_last=True)
val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=True, num_workers=num_workers)
# , pin_memory=True, drop_last=True)

# Find differentiable scene parameters
params = traverse(scene_gt)
param_res = params['dynamic_envmap.resolution']
param_ref = params['dynamic_envmap.data']
params.keep(['dynamic_envmap.data'])

if subtract_direct:
    params2 = traverse(scene_gt_direct)
    params2.keep(['dynamic_envmap.data'])

# Setup NN, optimizer, tensorboard etc. ===================================================================================
net = nets.RenderingNet(cnndim, latentdim, numSGs, insize, netwidth, num_layers, num_freqs,
                        alternate_parametrization).to(device=gpudevice)
sg_centers = sgr.initialiseSGcenters(numSGs).to(device=gpudevice)
optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
writer = SummaryWriter(outputpath + '/runs/' + experiment_name)  # logloss-l1-adam-5e-3-testFaceNormals')
iteration = 0
envmap_mitsuba = torch.ones(param_res[1], param_res[0], 4).to(gpudevice)

for epoch in range(max_epochs):
    epoch_loss = 0.0
    for envmap, envmap_m, envmap_std in train_dataloader:
        optimizer.zero_grad()
        batch_loss = 0.0

        for b in range(envmap.shape[0]):

            for vind in range(virtual_batchsize):
                groundtruth, pred_render = [], []

                randomsensor, campos = sgr.generate_completely_random_sensor(spp, imsize,
                                                                             imsize)  # fix the camera in the batch

                # envmap_mitsuba[:, :, 0:3] = (torch.exp(envmap[b].squeeze() * envmap_std[b] + envmap_m[b])).permute(1, 2, 0).to(gpudevice)
                envmap_mitsuba[:, :, 0:3] = (torch.exp(envmap[b].squeeze() * envmap_std[b] + envmap_m[b])).permute(1, 2, 0).to(
                        gpudevice)
                # envmap_mitsuba[:, :, 0:3] = ((torch.exp(envmap[b].squeeze()) - 1.0) * envmap_std[b] + envmap_m[b]).permute(1, 2, 0).to(gpudevice)

                # envmap_mitsuba[:, :, 0:3] = (torch.exp(envmap[b].squeeze()*envmap_std[b]+envmap_m[b])).permute(1, 2, 0).to(gpudevice)
                params['dynamic_envmap.data'] = Float(envmap_mitsuba.view(-1))
                params.update()

                gt, pr = sgr.render_gt_and_sg(scene_gt, randomsensor, campos, net, envmap[b],
                                              envmap_m[b], envmap_std[b],
                                              num_freqs, numSGs, withnormals, spp,
                                              sg_centers, exponentiate, imsize, gpudevice, False, alternate_parametrization, filepath =scenepath + '/' + scenename_gt)
                if subtract_direct:
                    params2['dynamic_envmap.data'] = Float(envmap_mitsuba.view(-1))
                    params2.update()
                    scene_gt_direct.integrator().render(scene_gt_direct, randomsensor)
                    components = randomsensor.film().bitmap(raw=False).split()
                    for i in range(len(components)):
                        if 'gt' in components[i][0]:
                            buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
                            directgt = torch.from_numpy(buffer.data_np())
                        # elif 'bsdf' in components[i][0]:
                        #     buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
                        #     bsdf_val = torch.from_numpy(buffer.data_np())
                        #     directgt = directgt * (bsdf_val > 0.0)
                        elif 'alpha' in components[i][0]:
                            buffer = components[i][1].convert(Bitmap.PixelFormat.Y, Struct.Type.Float32,
                                                              srgb_gamma=False)
                            alpha = torch.from_numpy(buffer.data_np())
                    # bsdf_val = bsdf_val.view(-1, 3)
                    # mask = (torch.sum(bsdf_val * bsdf_val, dim=-1, keepdim=False) > 0.0)
                    mask = (alpha.view(-1) > 0.0)
                    gt = torch.clamp(gt - directgt.view(-1, 3)[mask,:], min=0.0)

                groundtruth.append(gt)
                pred_render.append(pr)

            groundtruth = torch.cat(groundtruth, dim=0).to(gpudevice)
            pred_render = torch.cat(pred_render, dim=0)
            loss = sgr.my_loss(pred_render, groundtruth, tonemap=False, clip=False, log=logloss, L2=L2loss) / virtual_batchsize
            batch_loss += loss.item() / virtual_batchsize
            loss.backward()

        optimizer.step()
        writer.add_scalar('training loss', batch_loss, iteration)
        print('loss  ', batch_loss, 'for iteration  ', iteration)
        iteration += 1

        with torch.no_grad():
            if iteration % 2 == 1:
                envmap, envmap_m, envmap_std = next(iter(val_dataloader))
                envmap_mitsuba[:, :, 0:3] = (torch.exp(envmap.squeeze() *
                                                       envmap_std + envmap_m)).permute(
                    1, 2, 0).to(gpudevice)
                # envmap_mitsuba[:, :, 0:3] = (torch.exp(envmap[b].squeeze()*envmap_std[b]+envmap_m[b])).permute(1, 2, 0).to(gpudevice)
                params['dynamic_envmap.data'] = Float(envmap_mitsuba.view(-1))
                params.update()

                torch.save(net.state_dict(), outputpath + '/runs/' + experiment_name + '-net.pth')

                # TODO : render a higher resolution image here
                randomsensor, campos = sgr.generate_completely_random_sensor(spp, testres, testres)  # fix the camera in the batch
                gtcpu, pr, img_grid2 = sgr.render_gt_and_sg(scene_gt, randomsensor, campos, net, envmap[0],
                                                            envmap_m[0], envmap_std[0],
                                                            num_freqs, numSGs, withnormals, spp,
                                                            sg_centers, exponentiate, testres,
                                                            gpudevice, True, alternate_parametrization,
                                                            writer=writer, iteration=iteration, filepath =scenepath + '/' + scenename_gt)

                if subtract_direct:
                    params2['dynamic_envmap.data'] = Float(envmap_mitsuba.view(-1))
                    params2.update()
                    scene_gt_direct.integrator().render(scene_gt_direct, randomsensor)
                    components = randomsensor.film().bitmap(raw=False).split()
                    for i in range(len(components)):
                        if 'gt' in components[i][0]:
                            buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32,
                                                              srgb_gamma=False)
                            directgt = torch.from_numpy(buffer.data_np())
                        elif 'alpha' in components[i][0]:
                            buffer = components[i][1].convert(Bitmap.PixelFormat.Y, Struct.Type.Float32,
                                                              srgb_gamma=False)
                            alpha = torch.from_numpy(buffer.data_np())
                    mask = (alpha > 0.0).expand(-1, -1, 3)
                    directgt = directgt * mask
                    gtcpu = gtcpu - directgt

                # gt = (groundtruth*mask.unsqueeze(1).expand(-1,3)).view(groundtruthshape).cpu()
                gtcpu = gtcpu.permute(2, 0, 1)

                # pred[mask.cpu(),:] = pred_render.cpu()
                # pred = pred.view(groundtruthshape)
                pr = pr.permute(2, 0, 1)
                diff = torch.abs(pr - gtcpu)

                # print(gtcpu.shape, pr.shape, diff.shape)
                print('maxs and mins  ', gtcpu.max(), pr.max(), diff.max(), gtcpu.min(), pr.min(), diff.min())
                img_grid = torchvision.utils.make_grid([pr, gtcpu, diff])
                img_grid = img_grid / gtcpu.max()
                img_grid = img_grid ** (1 / 2.2)

                img_grid = torch.clip(1.2 * img_grid, 0.0, 1.0)
                img_grid2 = torch.clip(img_grid2, 0.0, 1.0)
                img_grid2 = img_grid2 ** (1.0 / 2.2)
                writer.add_image('predictions vs. GT', img_grid, global_step=iteration)
                writer.add_image('envmap at center of render', img_grid2, global_step=iteration)

                if subtract_direct:
                    gtcpu = gtcpu + directgt.permute(2, 0, 1)
                    pr = pr + directgt.permute(2, 0, 1)
                diff = torch.abs(pr - gtcpu)

                img_grid = torchvision.utils.make_grid([pr, gtcpu, diff])
                img_grid = img_grid / gtcpu.max()
                img_grid = img_grid ** (1.0 / 2.2)

                img_grid = torch.clip(1.25 * img_grid, 0.0, 1.0)
                img_grid2 = torch.clip(img_grid2, 0.0, 1.0)
                img_grid2 = img_grid2 ** (1.0 / 2.2)
                writer.add_image('predictions vs. GT - full', img_grid, global_step=iteration)

print('Finished Training')
