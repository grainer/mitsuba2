import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
import imageio
# imageio.plugins.freeimage.download() #Uncomment if the plugin to save EXR is not available on the system
import torch
from torchvision.utils import save_image


data_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/paths/atelier/prt'
out_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/paths/atelier/fxaaprt'

multiplier = 0.5 # 0.5 for atelier, 5 for kitchen, 5 bedroom, 10 for sanmiguel
use_fxaa = True


linear = torch.tensor([0.0, 1.0])
idx = torch.tensor([0.25])
print(linear[idx[:].long()])



if not os.path.exists(out_dir):
    os.makedirs(out_dir)
filenames = []
counter = 0
for r, d, f in os.walk(data_dir):  # r=root, d=directories, f = files
    f.sort()
    for file in f:
        # if 'AccumulatePass.output.0.exr' in file:
        # if 'Denoiser.output.0.exr' in file:
        if '.exr' in file:
            filenames.append(file)
            im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
            im = torch.from_numpy(im)

            xind, yind = torch.meshgrid(torch.arange(im.shape[0]), torch.arange(im.shape[1]))

            pr = im.permute(2, 0, 1)

            if use_fxaa:
                rgbNW = torch.zeros(im.shape)
                rgbNE = torch.zeros(im.shape)
                rgbSW = torch.zeros(im.shape)
                rgbSE = torch.zeros(im.shape)
                rgbNW[1:-1, 1:-1, :] = im[:-2, :-2, :] #texture2D(buf0, texCoords + (vec2(-1.0, -1.0) / frameBufSize)).xyz
                rgbNE[1:-1, 1:-1, :] = im[:-2, 2:, :] # = texture2D(buf0, texCoords + (vec2(1.0, -1.0) / frameBufSize)).xyz
                rgbSW[1:-1, 1:-1, :] = im[2:, :-2, :] # = texture2D(buf0, texCoords + (vec2(-1.0, 1.0) / frameBufSize)).xyz
                rgbSE[1:-1, 1:-1, :] = im[2:, 2:, :] # = texture2D(buf0, texCoords + (vec2(1.0, 1.0) / frameBufSize)).xyz
                rgbM = im

                luma = [0.299, 0.587, 0.114]
                lumaNW = luma[0] * rgbNW[:,:, 0] + luma[1] * rgbNW[:,:, 1] + luma[2] * rgbNW[:,:, 2] #
                lumaNE = luma[0] * rgbNE[:,:, 0] + luma[1] * rgbNE[:,:, 1] + luma[2] * rgbNE[:,:, 2] #dot(rgbNE, luma)
                lumaSW = luma[0] * rgbSW[:,:, 0] + luma[1] * rgbSW[:,:, 1] + luma[2] * rgbSW[:,:, 2] #dot(rgbSW, luma)
                lumaSE = luma[0] * rgbSE[:,:, 0] + luma[1] * rgbSE[:,:, 1] + luma[2] * rgbSE[:,:, 2] #dot(rgbSE, luma)
                lumaM = luma[0] * rgbM[:,:, 0] + luma[1] * rgbM[:,:, 1] + luma[2] * rgbM[:,:, 2] #dot(rgbM, luma)
                lumaMin = torch.minimum(lumaM, torch.minimum(torch.minimum(lumaNW, lumaNE), torch.minimum(lumaSW, lumaSE)))
                lumaMax = torch.maximum(lumaM, torch.maximum(torch.maximum(lumaNW, lumaNE), torch.maximum(lumaSW, lumaSE)))

                dirx = ((lumaNW + lumaNE) - (lumaSW + lumaSE))
                diry = ((lumaNW + lumaSW) - (lumaNE + lumaSE))
                # dirx = -((lumaNW + lumaNE) - (lumaSW + lumaSE))
                # diry = ((lumaNW + lumaSW) - (lumaNE + lumaSE))
                dirReduce = torch.clamp((lumaNW + lumaNE + lumaSW + lumaSE) * (0.25 * 1.0/8.0), max = 1.0/128.0)
                rcpDirMin = 1.0 / (torch.minimum(torch.abs(dirx), torch.abs(diry)) + dirReduce)

                dirx = torch.clamp(dirx * rcpDirMin, min = -8.0, max = 8.0 )
                diry = torch.clamp(diry * rcpDirMin, min = -8.0, max = 8.0 )
                dirx[0,:] = 0.0
                dirx[:,0] = 0.0
                dirx[-1,:] = 0.0
                dirx[:,-1] = 0.0
                diry[0, :] = 0.0
                diry[:, 0] = 0.0
                diry[-1, :] = 0.0
                diry[:, -1] = 0.0
                xoff1 = torch.clamp(xind + dirx * (1.0 / 3.0 - 0.5), min=0, max=511)
                yoff1 = torch.clamp(yind + diry * (1.0 / 3.0 - 0.5), min=0, max=511)
                xoff2 = torch.clamp(xind + dirx * (2.0 / 3.0 - 0.5), min=0, max=511)
                yoff2 = torch.clamp(yind + diry * (2.0 / 3.0 - 0.5), min=0, max=511)
                # plt.imshow(xoff1)
                # plt.show()

                # rgbA = (1.0 / 2.0) * ( im[xoff1[:].long(), yoff1[:].long(), :] + im[xoff2[:].long(), yoff2[:].long(), :] )

                rgbA = torch.zeros(pr.shape)
                rgbA = (1.0 / 2.0) * ( torch.nn.functional.grid_sample(pr.unsqueeze(0), torch.stack([yoff1, xoff1], dim=2).unsqueeze(0) / pr.shape[1] * 2.0 - 1.0)
                                       + torch.nn.functional.grid_sample(pr.unsqueeze(0), torch.stack([yoff2, xoff2], dim=2).unsqueeze(0) / pr.shape[1] * 2.0 - 1.0) )
                # rgbA[:,:, 0] = (1.0 / 2.0) * ( torch.nn.functional.grid_sample(pr[:,:,0], torch.stack([xoff1, yoff1])) + torch.nn.functional.grid_sample(pr[:,:,0], torch.stack([xoff2, yoff2])) )
                # rgbA[:,:, 1] = (1.0 / 2.0) * ( torch.nn.functional.grid_sample(pr[:,:,1], torch.stack([xoff1, yoff1])) + torch.nn.functional.grid_sample(pr[:,:,1], torch.stack([xoff2, yoff2])) )
                # rgbA[:,:, 2] = (1.0 / 2.0) * ( torch.nn.functional.grid_sample(pr[:,:,2], torch.stack([xoff1, yoff1])) + torch.nn.functional.grid_sample(pr[:,:,2], torch.stack([xoff2, yoff2])) )



                xoff1 = torch.clamp(xind + dirx * (0.0 / 3.0 - 0.5), min=0, max=511)
                yoff1 = torch.clamp(yind + diry * (0.0 / 3.0 - 0.5), min=0, max=511)
                xoff2 = torch.clamp(xind + dirx * (0.0 / 3.0 - 0.5), min=0, max=511)
                yoff2 = torch.clamp(yind + diry * (3.0 / 3.0 - 0.5), min=0, max=511)

                # rgbB = rgbA * (1.0 / 2.0) + (1.0 / 4.0) * (im[xoff1[:].long(), yoff1[:].long(), :] + im[xoff2[:].long(), yoff2[:].long(), :] )

                rgbB = torch.zeros(pr.shape)
                rgbB = rgbA * (1.0 / 2.0) + (1.0 / 4.0) * (torch.nn.functional.grid_sample(pr.unsqueeze(0), torch.stack([yoff1,xoff1], dim=2).unsqueeze(0) / pr.shape[1] * 2.0 - 1.0) + torch.nn.functional.grid_sample(
                    pr.unsqueeze(0), torch.stack([yoff2, xoff2], dim=2).unsqueeze(0) / pr.shape[1] * 2.0 - 1.0 ) )
                # rgbB[:, :, 0] = rgbA[:, :, 0] * (1.0 / 2.0) + (1.0 / 4.0) * (torch.nn.functional.grid_sample(pr[:, :, 0], torch.stack(xoff1,yoff1)) + torch.nn.functional.grid_sample(
                #     pr[:, :, 0], torch.stack(xoff2, yoff2)))
                # rgbB[:, :, 1] = rgbA[:, :, 1] * (1.0 / 2.0) + (1.0 / 4.0) * (torch.nn.functional.grid_sample(pr[:, :, 1], torch.stack(xoff1,yoff1)) + torch.nn.functional.grid_sample(
                #     pr[:, :, 1], torch.stack(xoff2, yoff2)))
                # rgbB[:, :, 2] = rgbA[:, :, 2] * (1.0 / 2.0) + (1.0 / 4.0) * (torch.nn.functional.grid_sample(pr[:, :, 2], torch.stack(xoff1,yoff1)) + torch.nn.functional.grid_sample(
                #     pr[:, :, 2], torch.stack(xoff2, yoff2)))

                # rgbA = torch.zeros(im.shape)
                # rgbB = torch.zeros(im.shape)
                # for pxi in range(im.shape[0]):
                #     for pxj in range(im.shape[1]):
                #         rgbA[pxi, pxj, :] = (1.0 / 2.0) * (im[xind[pxi, pxj] + dirx[pxi, pxj] * (1.0 / 3.0 - 0.5),
                #                                            yind[pxi, pxj] + diry[pxi, pxj] * (1.0 / 3.0 - 0.5), :] +
                #                                            im[xind[pxi, pxj] + dirx[pxi, pxj] * (2.0 / 3.0 - 0.5),
                #                                            yind[pxi, pxj] + diry[pxi, pxj] * (2.0 / 3.0 - 0.5), :])
                #
                #         rgbB[pxi, pxj, :] = rgbA * (1.0 / 2.0) + (1.0 / 4.0) * (
                #                 im[xind[pxi, pxj] + dirx[pxi, pxj] * (0.0 / 3.0 - 0.5),
                #                 yind[pxi, pxj] + diry[pxi, pxj] * (0.0 / 3.0 - 0.5), :] +
                #                 im[xind[pxi, pxj] + dirx[pxi, pxj] * (0.0 / 3.0 - 0.5),
                #                 yind[pxi, pxj] + diry[pxi, pxj] * (3.0 / 3.0 - 0.5), :])

                lumaB = luma[0] * rgbB[0, 0,:,:] + luma[1] * rgbB[0, 1,:,:] + luma[2] * rgbB[0, 2,:,:]  #
                mask = (lumaB > lumaMin) * (lumaB < lumaMax)
                pr[:, mask] = rgbB[0, :, mask]
                pr[:,~mask] = rgbA[0, :,~mask]

                # lumaB = luma[0] * rgbB[:, :, 0] + luma[1] * rgbB[:, :, 1] + luma[2] * rgbB[:, :, 2]  #
                # mask = (lumaB > lumaMin) * (lumaB < lumaMax)
                # im[mask, :] = rgbB[mask, :]
                # im[~mask, :] = rgbB[~mask, :]


            pr = torch.clip(multiplier * pr, 0.0, 1.0)
            pr = pr ** (1.0 / 2.2)

            save_image(pr, out_dir + "/" + str(counter).zfill(8) + ".png")
            counter+=1