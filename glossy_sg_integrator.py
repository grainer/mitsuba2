import os
import numpy as np
import enoki as ek
import mitsuba
# Set the desired mitsuba variant
mitsuba.set_variant('gpu_autodiff_rgb')
from mitsuba.core import Float, Vector3f, Thread, Frame3f, xml
from mitsuba.core.xml import load_file
from mitsuba.render import (BSDF, BSDFContext, BSDFFlags,
                            BSDFSample3f, SurfaceInteraction3f, register_bsdf,
                            DirectionSample3f, Emitter, ImageBlock,
                            SamplingIntegrator, has_flag,
                            register_integrator, reflect)


filename = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere-glossy/glossyscene-sg.xml' #test.xml'#

lightSG = [[19.77183, 18.899605, 28.09268, 124.06032, 0.13278571, 0.39354134, 0.90966654], [13.951765, 15.265006, 23.527641, 120.74434, -0.15289421, 0.5411309, 0.8269225], [0.4071942, 0.22668266, 0.10997803, 0.89142746, -0.0812242, -0.6204905, 0.7799963], [64.97454, 42.578075, 16.2094, 154.95787, -0.20876692, 0.07599607, 0.97500813], [5.1314754, 4.024345, 3.1723998, 91.700905, -0.88628745, -0.11917673, -0.44753936], [26.087568, 35.68813, 47.75076, 40.86883, -0.18703045, 0.25262922, 0.94931453], [11.181716, 15.722419, 25.156954, 210.88081, 0.10982957, 0.5572564, 0.82304484], [1.8714151, 1.836281, 1.6746786, 35.524567, 0.34814197, -0.37467614, 0.8593107], [51.333977, 24.03316, 0.3976881, 90.55722, 0.124854945, 0.100686945, 0.98705286], [4.564536, 3.5192788, 2.8387885, 131.00095, 0.9625086, -0.11077466, -0.24760067], [0.1044002, 24.66059, 42.970016, 91.73856, 0.13887106, 0.15844153, 0.97755367], [103.84179, 74.74271, 46.163086, 1047.5914, -0.42761734, 0.07404973, 0.90092176]]
savefile = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere-glossy/sg-ennis-hybridwarp-r02.exr'

hybridanis = True# False#
sharpness_thresh = np.pi  #20#
anisowarp = True#  False#

# # # # pisa:
lightSG = [[0.9433718, 0.37715107, 0.030528598, 35.069885, -0.84324276, 0.2121104, 0.4939137], [0.70375603, 0.3655845, 0.15851796, 23.585962, 0.52402025, 0.7886307, -0.32165867], [0.08567178, 0.06427135, 0.044218015, 1.1091332, -0.2356962, -0.66532904, -0.70836747], [0.6558805, 0.31723064, 0.11005787, 21.001013, -0.60743445, 0.73271066, -0.30685267], [0.86364007, 0.35598388, 0.11529839, 22.24271, 0.46149436, 0.2683993, -0.8455677], [1.1314801, 1.2558013, 1.2232834, 41.10061, 0.053204477, 0.256633, 0.9650434], [1.0217043, 0.4285945, 0.13063939, 15.990939, -0.047010783, 0.8666322, -0.49672785], [0.20844299, 0.3255447, 0.49083838, 5.3000503, -0.44954064, 0.51849866, 0.72737354], [1.158383, 1.5155687, 1.7711425, 46.52204, 0.38086146, 0.38595143, 0.84022975], [2.3821747, 2.6763604, 2.6895938, 227.89229, 0.6516599, 0.27348915, 0.7074907], [0.66085684, 0.31836376, 0.1304988, 49.41547, 0.9093498, 0.18544276, -0.3724163], [0.7925673, 1.0842797, 1.393003, 14.961501, 0.7191419, 0.51414, 0.46743447]]
savefile = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere-glossy/sg-pisa-hybridwarp-r02.exr'


# lightSG = [[1.0, 1.0, 1.0, 1000, 0.0, 1.0, 0.0]]
# savefile = 'C:/Users/grainer.AD/Documents/GitHub/data/testsphere-glossy/sg-lightfromtop.exr'  #test.exr'


def SGinnerProduct(amp1, sharp1, axi1, amp2, sharp2, axi2):
    um = sharp1 * axi1 + sharp2 * axi2
    umLength = ek.norm(um)
    # print(umLength)
    expo = ek.exp(umLength - sharp1 - sharp2)
    expo = expo * amp1 * amp2
    other = -ek.exp(-2.0 * umLength) + 1.0
    return (2.0 * np.pi * expo * other) / umLength

def warpBRDF_SG(amp, sharp, axi, wi):
    w_axi = reflect(wi, axi)
    cosv = ek.select((ek.dot(wi, axi) > 0.00001), ek.dot(wi, axi), Float(0))
    w_sharp = sharp / (4.0 * cosv)
    return amp, w_sharp, w_axi


def evaluate_ASG(amp, x, y, z, sharpX, sharpY, direction):
    sTerm = ek.dot(z, direction)
    active = (sTerm > 0.0)
    lambdaTerm = ek.select(active, sharpX * ek.dot(direction, x) * ek.dot(direction, x), Float(0))
    muTerm = ek.select(active, sharpY * ek.dot(direction, y) * ek.dot(direction, y), Float(0))
    sTerm = ek.select(active, sTerm * ek.exp(-lambdaTerm - muTerm), Float(0.0))
    return amp * sTerm

def convolveASG_SG(aniso_amp, aniso_x, aniso_y, aniso_z, aniso_sharpX, aniso_sharpY, amp, sharp, axi):
    nu = sharp * 0.5 #The ASG paper specifes an isotropic SG as exp(2 * nu * (dot(v, axis) - 1))
    out_sharpX = (nu * aniso_sharpX) / (nu + aniso_sharpX)
    out_sharpY = (nu * aniso_sharpY) / (nu + aniso_sharpY)
    out_amp = Float(np.pi) / ek.sqrt((nu + aniso_sharpX) * (nu + aniso_sharpY))
    return evaluate_ASG(amp * aniso_amp * out_amp, aniso_x, aniso_y, aniso_z, out_sharpX, out_sharpY, axi)

def warpBRDF_ASG(amp, sharp, axi, wi):
    # // Generate any orthonormal basis with Z pointing in the direction of the reflected view vector
    warpZ = reflect(wi, axi)
    warpX = ek.normalize(ek.cross(axi, warpZ)) #ek.cross(axi, warpZ) #
    warpY = ek.normalize(ek.cross(warpZ, warpX)) #ek.cross(warpZ, warpX) #
    # print(ek.cross(warpZ, warpY)[0][1000000:1000009])
    # print(warpX[0][1000000:1000009])
    dotDirO = ek.select((ek.dot(wi, axi)>0.0001), ek.dot(wi, axi), Float(0.0001))
    # // Second derivative of the sharpness with respect to how far we are from basis Axis direction
    sharpX = sharp / (8.0 * dotDirO * dotDirO)
    sharpY = sharp / 8.0
    return amp, warpX, warpY, warpZ, sharpX, sharpY



def integrator_sgs(scene, rays, active=True):
    si = scene.ray_intersect(rays)
    active_b = si.is_valid() & active
    active_b = active_b & (Frame3f.cos_theta(si.wi)>0.0)
    ctx = BSDFContext()
    bsdf = si.bsdf(rays)

    roughness = BSDF.get_alpha_vec(bsdf, si)
    m2 = roughness * roughness
    ndf_amp = Vector3f(1.0) / (m2 * np.pi)
    ndf_sharp = Float(2.0) / m2


    if hybridanis:
        print('Rendering with hybrid iso/anisotropic warping')
        # isotropic rotated SG NDF
        sg_amp, sg_sharp, sg_axi = warpBRDF_SG(ndf_amp, ndf_sharp, si.n, si.to_world(si.wi))

        # anisotropic rotated SG NDF
        amp, warpX, warpY, warpZ, sharpX, sharpY = warpBRDF_ASG(ndf_amp, ndf_sharp, si.n, si.to_world(si.wi))

        print(lightSG[0][3])
        if (lightSG[0][3] > sharpness_thresh):
            ndf_val = ek.select(active_b, convolveASG_SG(amp, warpX, warpY, warpZ, sharpX, sharpY, Vector3f(lightSG[0][0:3]), lightSG[0][3], Vector3f(lightSG[0][4:7])), Vector3f(0))
        else:
            ndf_val = ek.select(active_b, SGinnerProduct(Vector3f(lightSG[0][0:3]), lightSG[0][3], Vector3f(lightSG[0][4:7]), sg_amp, sg_sharp, sg_axi), Vector3f(0))

        for l in range(len(lightSG) - 1):
            print(lightSG[l+1][3])
            if (lightSG[l+1][3] > sharpness_thresh):
                ndf_val += ek.select(active_b, convolveASG_SG(amp, warpX, warpY, warpZ, sharpX, sharpY,
                                                              Vector3f(lightSG[l + 1][0:3]), lightSG[l + 1][3],
                                                              Vector3f(lightSG[l + 1][4:7])), Vector3f(0))
            else:
                ndf_val += ek.select(active_b, SGinnerProduct(Vector3f(lightSG[l+1][0:3]), lightSG[l+1][3], Vector3f(lightSG[l+1][4:7]), sg_amp, sg_sharp, sg_axi), Vector3f(0))



    else:
        if anisowarp:
            # warp NDF to anisotropic SG
            amp, warpX, warpY, warpZ, sharpX, sharpY = warpBRDF_ASG(ndf_amp, ndf_sharp, si.n, si.to_world(si.wi))

            ndf_val = ek.select(active_b, convolveASG_SG(amp, warpX, warpY, warpZ, sharpX, sharpY, Vector3f(lightSG[0][0:3]), lightSG[0][3], Vector3f(lightSG[0][4:7])), Vector3f(0))
            for l in range(len(lightSG) - 1):
                ndf_val += ek.select(active_b, convolveASG_SG(amp, warpX, warpY, warpZ, sharpX, sharpY, Vector3f(lightSG[l+1][0:3]), lightSG[l+1][3], Vector3f(lightSG[l+1][4:7])), Vector3f(0))

        else:
            # rotate the BRDF SGs
            sg_amp, sg_sharp, sg_axi = warpBRDF_SG(ndf_amp, ndf_sharp, si.n, si.to_world(si.wi))

            ndf_val = ek.select(active_b, SGinnerProduct(Vector3f(lightSG[0][0:3]), lightSG[0][3], Vector3f(lightSG[0][4:7]), sg_amp, sg_sharp, sg_axi), Vector3f(0))
            for l in range(len(lightSG) - 1):
                ndf_val = ndf_val + ek.select(active_b, SGinnerProduct(Vector3f(lightSG[l+1][0:3]), lightSG[l+1][3], Vector3f(lightSG[l+1][4:7]), sg_amp, sg_sharp, sg_axi), Vector3f(0))


    # # # BSDF without NDF term, only terms out of the integral. Evaluate in reflected direction
    result = ek.select(active_b, BSDF.eval_vec(bsdf, ctx, si, reflect(si.wi), active_b) * ndf_val, Vector3f(0))
    return result, si.is_valid()

def integrator_sample(scene, sampler, rays, medium, active=True):
    si = scene.ray_intersect(rays)
    active = si.is_valid() & active

    # Visible emitters
    emitter_vis = si.emitter(scene, active)
    result = ek.select(active, Emitter.eval_vec(emitter_vis, si, active), Vector3f(0.0))

    ctx = BSDFContext()
    bsdf = si.bsdf(rays)

    # Emitter sampling
    sample_emitter = active & has_flag(BSDF.flags_vec(bsdf), BSDFFlags.Smooth)
    ds, emitter_val = scene.sample_emitter_direction(si, sampler.next_2d(sample_emitter), True, sample_emitter)
    active_e = sample_emitter & ek.neq(ds.pdf, 0.0)
    wo = si.to_local(ds.d)
    bsdf_val = BSDF.eval_vec(bsdf, ctx, si, wo, active_e)
    bsdf_pdf = BSDF.pdf_vec(bsdf, ctx, si, wo, active_e)
    mis = ek.select(ds.delta, Float(1), mis_weight(ds.pdf, bsdf_pdf))
    result += ek.select(active_e, emitter_val * bsdf_val * mis, Vector3f(0))

    # BSDF sampling
    active_b = active
    bs, bsdf_val = BSDF.sample_vec(bsdf, ctx, si, sampler.next_1d(active), sampler.next_2d(active), active_b)
    si_bsdf = scene.ray_intersect(si.spawn_ray(si.to_world(bs.wo)), active_b)
    emitter = si_bsdf.emitter(scene, active_b)
    active_b &= ek.neq(emitter, 0)
    emitter_val = Emitter.eval_vec(emitter, si_bsdf, active_b)
    delta = has_flag(bs.sampled_type, BSDFFlags.Delta)
    ds = DirectionSample3f(si_bsdf, si)
    ds.object = emitter
    emitter_pdf = ek.select(delta, Float(0), scene.pdf_emitter_direction(si, ds, active_b))
    result += ek.select(active_b, bsdf_val * emitter_val * mis_weight(bs.pdf, emitter_pdf), Vector3f(0))
    return result, si.is_valid()
def mis_weight(pdf_a, pdf_b):
    pdf_a *= pdf_a
    pdf_b *= pdf_b
    return ek.select(pdf_a > 0.0, pdf_a / (pdf_a + pdf_b), Float(0.0))


class SGIntegrator(SamplingIntegrator):
    def __init__(self, props):
        SamplingIntegrator.__init__(self, props)

    def sample(self, scene, sampler, ray, medium, active):
        result, is_valid = integrator_sgs(scene, ray, active)
        # result, is_valid = integrator_sample(scene, sampler, ray, medium, active)
        return result, is_valid, []

# Register our integrator such that the XML file loader can instantiate it when loading a scene
register_integrator("sgintegrator", lambda props: SGIntegrator(props))

# Load an XML file which specifies "glossysg" as the scene's integrator
Thread.thread().file_resolver().append(os.path.dirname(filename))
scene = load_file(filename)
scene.integrator().render(scene, scene.sensors()[0])
film = scene.sensors()[0].film()
film.set_destination_file(savefile)
film.develop()



# class SGBSDF(BSDF):
#     # spectral IOR is obtained using include/render/ior.h which fetches from resources/data/ior/<materials>.eta.spd and .k.spd
#     def __init__(self, props):
#         BSDF.__init__(self, props)
#         # print(props)
#         self.roughness = props["alpha"]
#         self.material = props["material"]
#         self.specular = props["specular_reflectance"]
#         # print(props["distribution"])
#         # self.m_flags = BSDFFlags.DiffuseReflection | BSDFFlags.FrontSide
#         # self.m_components = [self.m_flags]
#
#         # // / Anisotropic roughness values  # ref < Texture > m_alpha_u, m_alpha_v
#         # // / Relative refractive index(real component)  # ref < Texture > m_eta
#         # // / Relative refractive index(imaginary component).  # ref < Texture > m_k
#         # // / Specular reflectance component  # ref < Texture > m_specular_reflectance
#
#     def sample(self, ctx, si, sample1, sample2, active):
#         wo  = warp.square_to_cosine_hemisphere(sample2)
#         return eval(self, ctx, si, wo, active)
#
#     def eval(self, ctx, si, wo, active):
#         cos_theta_i = Frame3f.cos_theta(si.wi)
#         cos_theta_o = Frame3f.cos_theta(wo)
#
#
#
#         # active &= cos_theta_i > 0.0 & & cos_theta_o > 0.0
#         # H = normalize(wo + si.wi);
#         # MicrofacetDistribution distr(m_type, m_alpha_u->eval_1(si, active), m_alpha_v->eval_1(si, active), m_sample_visible)
#         # D = distr.eval(H)
#         # active &= neq(D, 0.0)
#         #
#         # G = distr.G(si.wi, wo, H)
#         # UnpolarizedSpectrum result = D * G / (4.f * Frame3f::cos_theta(si.wi))
#         # Complex < UnpolarizedSpectrum > eta_c(m_eta->eval(si, active), m_k->eval(si, active))
#         # F = fresnel_conductor(UnpolarizedSpectrum(dot(si.wi, H)), eta_c)
#         # if (m_specular_reflectance)
#         #     result *= m_specular_reflectance->eval(si, active)
#         # return (F * result) & active
#
#
#
#
#
#         value = self.m_reflectance.eval(si, active) * math.InvPi * cos_theta_o
#
#         return ek.select((cos_theta_i > 0.0) & (cos_theta_o > 0.0), value, Vector3f(0))
#
#     def pdf(self, ctx, si, wo, active):
#         return warp.square_to_cosine_hemisphere_pdf(wo)
#
#     def to_string(self):
#         return "SGBSDF[reflectance = %s]".format(self.roughness.to_string())
