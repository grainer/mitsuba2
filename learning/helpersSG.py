import numpy as np
import torch
import matplotlib.pyplot as plt

# The shaders assume all the vector quantities are described by 2-dimensional tensors where the last dimension has 3 elements
# The SG lights are described in a 2D tensor where the first dim is the number of the light and the second dim contains:
# 7 elements: [amplitude_R, amplitude_G, amplitude_B, sharpness, axis_x, axis_y, axis_z]
# You can visualize the spherical gaussian envmap with:
# displaySGEnvmap(sglights)

# With the helper functions you can compute approximations for the diffuse component and the D term of a specular BRDF.
# In both cases, the cosine or the Beckmann term is approximated by a SG and there is an analytic approximation for the convolution with the SG light.
# For the normal distribution D, call the computeSG_Dterm function with:
# ndf_amp = 1.0 / roughness**2
# ndf_sharp = 2.0 / roughness**2

# The functions are invoked as:
# computeSG_diffuse(shading_normal, light_sg_amplitude, light_sg_sharpness, light_sg_axis)
# computeSG_Dterm(sg_lights, shading_normal, wi, ndf_amp, ndf_sharp)

# Usage example at the end of the file.


def dot(a,b):
    return torch.sum(a*b, dim=-1, keepdim=True).expand(-1,3)
def norm(v):
    return torch.sqrt(torch.sum(v**2, dim=-1, keepdim=True)).expand(-1,3)
def normalize(v):
    return v/norm(v)

def evaluate_ASG(amp, x, y, z, sharpX, sharpY, direction):
    sTerm = torch.nn.functional.relu(dot(z, direction))
    lambdaTerm = sharpX * dot(direction, x) * dot(direction, x)
    muTerm = sharpY * dot(direction, y) * dot(direction, y)
    sTerm = sTerm * torch.exp(-lambdaTerm - muTerm)
    return amp * sTerm

def convolveASG_SG(aniso_amp, aniso_x, aniso_y, aniso_z, aniso_sharpX, aniso_sharpY, amp, sharp, axi):
    nu = sharp * 0.5 #The ASG paper specifes an isotropic SG as exp(2 * nu * (dot(v, axis) - 1))
    out_sharpX = (nu * aniso_sharpX) / (nu + aniso_sharpX)
    out_sharpY = (nu * aniso_sharpY) / (nu + aniso_sharpY)
    out_amp = np.pi / torch.sqrt((nu + aniso_sharpX) * (nu + aniso_sharpY))
    return evaluate_ASG(amp * aniso_amp * out_amp, aniso_x, aniso_y, aniso_z, out_sharpX, out_sharpY, axi)

def warpBRDF_ASG(amp, sharp, axi, wi, device='cuda:0'):
    # // Generate any orthonormal basis with Z pointing in the direction of the reflected view vector
    warpZ = normalize(-wi + 2.0 * dot(wi, axi) * axi)
    warpX = normalize(torch.cross(axi, warpZ, dim=-1))
    warpY = normalize(torch.cross(warpZ, warpX, dim=-1))
    mask = (dot(wi, axi)>0.0001).bool()
    # // Second derivative of the sharpness with respect to how far we are from basis Axis direction
    sharpX = torch.zeros(mask.shape, device=device)
    sharpY = torch.zeros(mask.shape, device=device)
    sharpX[mask] = sharp[mask] / (8.0 * dot(wi, axi)[mask] * dot(wi, axi)[mask])
    sharpY[mask] = sharp[mask] / 8.0
    return amp, warpX, warpY, warpZ, sharpX, sharpY

def computeSG_diffuse(normal, amp, sharp, axi):
    cos_sharpness = 2.133
    cos_amplitude = 1.17
    umLength = norm(cos_sharpness * normal + sharp * axi)
    lambdam = sharp + cos_sharpness
    sinh_term = torch.exp(umLength-lambdam) - torch.exp(-umLength-lambdam)
    return (2.0 * np.pi * amp * cos_amplitude * sinh_term) / umLength

def computeSG_Dterm(sglights, n, wi, ndf_amp, ndf_sharp, device='cuda:0'):
    amp, warpX, warpY, warpZ, sharpX, sharpY = warpBRDF_ASG(ndf_amp, ndf_sharp, n, wi, device=device)
    ndf_val = torch.zeros(wi.shape, device=device)
    for l in range(sglights.size()[1]):
        ndf_val = ndf_val + convolveASG_SG(amp, warpX, warpY, warpZ, sharpX, sharpY, sglights[:, l, 0:3],
                                           sglights[:, l, 3].unsqueeze(1).expand(-1, 3), sglights[:, l, 4:7])
    assert (ndf_val != ndf_val).any() == False, "Damn! ndf_val contains NaNs"
    assert torch.isinf(ndf_val).any() == False, "Damn! ndf_val contains Infs"
    return ndf_val


def thetaphi_to_xyz(theta, phi): #angles in radians
    x = -torch.cos(theta) * torch.sin(phi)
    y = torch.sin(theta)
    z = torch.cos(theta) * torch.cos(phi)
    return torch.stack([x, y, z], dim=-1)

def create_latlong_grid(imheight, imwidth):
    # return the environment grid as (teta,phi): elevation [-90,90], azimuth [-180,180]
    grid = torch.stack(torch.meshgrid([-torch.linspace(-89.5, 89.5, steps=imheight), torch.linspace(-179.5, 179.5, steps=imwidth)]), dim=-1)
    return grid.reshape(imheight, imwidth, 2) * np.pi / 180.0

def create_envmap_grid(imheight, imwidth):
    latlong = create_latlong_grid(imheight, imwidth)
    return thetaphi_to_xyz(latlong.select(-1, 0), latlong.select(-1, 1))

def displaySGEnvmap(sglights, device='cuda:0'):
    xyzgrid = create_envmap_grid(128,256).to(device=device)
    val = torch.zeros(xyzgrid.size()).to(device=device)
    for l in range(sglights.size()[0]):
        cosangle = (xyzgrid[:,:,0]*sglights[l,4] + xyzgrid[:,:,1]*sglights[l,5] + xyzgrid[:,:,2]*sglights[l,6]).unsqueeze(2).expand(xyzgrid.size())
        col = sglights[l,0:3].unsqueeze(0).unsqueeze(0).expand(xyzgrid.size())
        sg = col * torch.exp(sglights[l,3] * (cosangle - 1.0))
        val = val + sg
    im = val / val.max()
    return im**(1.0/2.2)



# Usage example:
# Make an envmap with a red light from the top and blue light from the front. Y-axis up here
sglights = torch.tensor([[10.0,0.0,0.0,1000.0,0.0,1.0,0.0], [0.0,0.0,2.0,100.0,0.0,0.0,1.0]]).to(device='cpu')
envmap = displaySGEnvmap(sglights, device='cpu')
plt.imshow(envmap)
plt.show()

# create an image of normals of a sphere
sphere_normals = torch.zeros(256,256,3)
sphere_normals[:,:,2], sphere_normals[:,:,0] = torch.meshgrid([-torch.linspace(-1.0,1.0, steps=256), torch.linspace(-1.0,1.0, steps=256)])
circle = sphere_normals[:,:,0]**2 + sphere_normals[:,:,2]**2
sphere_normals[:,:,1] = torch.sqrt(torch.nn.functional.relu(1.0 - circle))
sphere_normals[circle>1.0] = norm(sphere_normals[circle>1.0])
plt.imshow(sphere_normals)
plt.show()

# define the roughness for the specular component
roughness = torch.ones(256*256,3) * 0.05
ndf_amp = 1.0 / roughness**2
ndf_sharp = 2.0 / roughness**2
sphere_normals = sphere_normals.view(-1,3) #all components need to be in this format :(pixels,3)

# visualise the diffuse shading component
diffuse = torch.zeros(256*256,3)
for i in range(sglights.shape[0]):
    amp = sglights[i,0:3].unsqueeze(0).expand(diffuse.shape)
    sharpness = sglights[i,3].unsqueeze(0).unsqueeze(1).expand(diffuse.shape)
    axis = sglights[i,4:7].unsqueeze(0).expand(diffuse.shape)
    diffuse += computeSG_diffuse(sphere_normals, amp, sharpness, axis)
plt.imshow(diffuse.view(256,256,3))
plt.show()

# visualise the specular normal distribution term
wi = torch.zeros(256*256,3)
wi[:,1] = 1.0
sglights = sglights.unsqueeze(0).expand(wi.shape[0], -1, -1)
D = computeSG_Dterm(sglights, sphere_normals, wi, ndf_amp, ndf_sharp, device='cpu')
plt.imshow(D.view(256,256,3) / D.max())
plt.show()
