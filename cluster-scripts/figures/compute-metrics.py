import matplotlib.pyplot as plt
import csv
import numpy as np
# import configargparse
import cv2
import os
import torch

import pytorchssim as pytorch_ssim
# import lpips_pytorch


data_dirs = ['F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/bedroom/realtime',
             'F:/gilles/paperresults/ios-denoised/bedroom/path-denoised',
             'F:/gilles/paperresults/results/results/bedroom/nrc_spp_01_accum_05',
             'F:/gilles/paperresults/results/results/bedroom/dir_spp_01_accum_05',
             'F:/gilles/paperresults/bedroom/0499-splitnets-withbuffers--bats-3-lat-64-layer-2-width-172-lr-0.0001']

reference_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/refpaths/bedroom/path'



data_dirs = ['F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/loss-bedroom/prt3',
    'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/loss-bedroom/prt']
reference_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/loss-bedroom/ref'

data_dirs = ['F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/loss-atelier/prt3',
    'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/loss-atelier/prt']
reference_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/figures/comparisons/prt/loss-atelier/ref'



data_dirs = [ 'F:/gilles/paperresults/deepshading/atelier']
reference_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/refpaths/atelier/path'
#
# data_dirs = [ 'F:/gilles/paperresults/deepshading/bedroom',
#               'F:/gilles/paperresults/bedroom/0499-splitnets-withbuffers--bats-3-lat-64-layer-2-width-172-lr-0.0001']
# reference_dir = 'F:/gilles/Dropbox/INRIA/projects/nprt/paper/refpaths/bedroom/path'
#


refs = []
datas = []
class AllMetrics(torch.nn.Module):

    def __init__(self):
        super(AllMetrics, self).__init__()

        self.l1 = 0
        self.l2 = 0
        self.lpips = 0
        self.dssim = 0
        self.mape = 0
        self.smape = 0
        self.mrse = 0

        # self.lpips_network = lpips_pytorch.lpips.LPIPS().to('cuda')
        self.loss_map = torch.zeros([0])

    def reset(self):
        self.l1 = 0
        self.l2 = 0
        self.lpips = 0
        self.dssim = 0
        self.mape = 0
        self.smape = 0
        self.mrse = 0

    def forward(self, pred, gt):

        diff = gt - pred
        eps = 1e-2

        self.l1 = (torch.abs(diff)).mean()
        self.l2 = (diff*diff).mean()
        self.mrse = (diff*diff/(gt*gt+eps)).mean()
        self.mape = (torch.abs(diff)/(gt+eps)).mean()
        self.smape = (2 * torch.abs(diff)/(gt+pred+eps)).mean()
        self.dssim = 1.0 - (pytorch_ssim.ssim(pred, gt)).mean()
        # self.lpips = self.lpips_network(pred.permute(0, 3, 2, 1), gt.permute(0, 3, 2, 1)).mean()

        # Choose which loss to show on preview
        self.loss_map = (diff*diff)

        return self

    def __add__(self, other):
        self.l1 = self.l1 + other.l1
        self.l2 = self.l2 + other.l2
        # self.lpips = self.lpips + other.lpips
        self.dssim = self.dssim + other.dssim
        self.mape = self.mape + other.mape
        self.smape = self.smape + other.smape
        self.mrse = self.mrse + other.mrse
        # self.lpips = self.lpips + other.lpips
        return self

    def __truediv__(self, other):
        self.l1 = self.l1 / other
        self.l2 = self.l2 / other
        # self.lpips = self.lpips / other
        self.dssim = self.dssim / other
        self.mape = self.mape / other
        self.smape = self.smape / other
        self.mrse = self.mrse / other
        # self.lpips = self.lpips / other
        return self


for r, d, f in os.walk(reference_dir):  # r=root, d=directories, f = files
    f.sort()
    for file in f:
        if 'AccumulatePass.output.0.exr' in file:
            im = torch.from_numpy( cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)).permute(2,0,1).unsqueeze(0)
            refs.append(im)

metrics = []
summedmetrics = []
for datadir in data_dirs:
    counter = 0
    dataims = []
    metric = AllMetrics()
    for r, d, f in os.walk(datadir):  # r=root, d=directories, f = files
        f.sort()
        for file in f:
            if 'denoised' in datadir:
                name = 'Denoiser.output.0.exr'
            else:
                name = 'AccumulatePass.output.0.exr'

            if name in file or str(counter).zfill(8)+'.exr' in file:
                # print(file)
                im = torch.from_numpy( cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)).permute(2,0,1).unsqueeze(0)
                # dataims.append(im)
                loss = AllMetrics()
                metric += loss(im, refs[counter])
                counter += 1

    # datas.append(dataims)
    print("computed on ", counter, " images from ", datadir)
    summedmetrics.append(metric)
    # metrics.append(metric/counter)
    # m = metric/counter
    print(metric.l1, metric.l2, metric.lpips, metric.dssim, metric.mape, metric.smape, metric.mrse)

# print("  ")
# for m in metrics:
#     print("Metrics: ", m.l1, m.l2, m.lpips, m.dssim, m.mape, m.smape, m.mrse)


print("  ")
for m in summedmetrics:
    print("Summed Metrics: ", m.l1, m.l2, m.lpips, m.dssim, m.mape, m.smape, m.mrse)

for m in summedmetrics:
    print("Summed Metrics /600: ", m.l1/0.6, m.l2/0.6, m.dssim/0.6, m.mape/0.6, m.mrse/0.6)
    # print("Summed Metrics /600: ", m.l1.item()/0.6, m.l2.item()/0.6, m.lpips.item()/0.6, m.dssim.item()/0.6, m.mape.item()/0.6, m.smape.item()/0.6, m.mrse.item()/0.6)


