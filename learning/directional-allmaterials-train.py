import os
import numpy as np
import enoki as ek
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader

import sys
import os

sys.path += [ 'C:/Users/grainer.AD/Documents/GitHub/renderers/mitsuba2/build/dist/python' ]
os.environ['PATH'] += os.pathsep + 'C:/Users/grainer.AD/Documents/GitHub/renderers/mitsuba2/build/dist'

import mitsuba
# Set the desired mitsuba variant (MUST BE THE SAME IN ALL PYTHON FILES !!!)
import nets
mitsuba.set_variant('gpu_rgb') #'gpu_autodiff_rgb')
from mitsuba.core import Float, UInt32, Vector2f, Vector3f, Thread, Frame3f, xml, ScalarTransform4f, Bitmap, Struct
from mitsuba.core.xml import load_file, load_dict
from mitsuba.render import (BSDF, BSDFContext, BSDFFlags,
                            BSDFSample3f, SurfaceInteraction3f, register_bsdf,
                            DirectionSample3f, Emitter, ImageBlock,
                            SamplingIntegrator, has_flag,
                            register_integrator, reflect)
from mitsuba.python.util import traverse
import renderhelpers as sgr
import matplotlib.pyplot as plt
torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True
import time

# Input paths and parameters ===========================================================================================
# scenepath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/AOVscene'
scenepath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/dynamic-envmap-bunny'# 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/AOVscene'
outputpath = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/results/directionallight/allmaterials'
scenename_train = 'diffuse-gt.xml'
scenename_gt = 'aovs-all.xml'#  'gloss.xml' #

scenepath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/dynamic-envmap-bunny'
outputpath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/results/dynamic-allmaterials/alien'
envmap_dir = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/envmaps/IndoorHDRDataset2018-256x128'
# envmap_dir = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/render-assets' #
scenename_gt = 'aovs-all.xml'

scenepath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/dynamic-envmap-bunny'
outputpath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/results/dynamic-allmaterials/alien'
envmap_dir = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/envmaps/IndoorHDRDataset2018-256x128'
# envmap_dir = 'F:/gilles/Dropbox/INRIA/projects/learnedtransport/data/render-assets' #
scenename_gt = 'aovs-all.xml'

scenepath = 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/data/scenes/living-room'
scenename_gt = 'scene-gilles.xml'

scenepath = 'C:/Users/grainer.AD/Desktop/projects/data/mitsuba-falcor-conversion/stavrosscenes/bathroom2-falcor'
scenename_gt = 'scene.xml'

# # Training parameters ==================================================================================================
max_epochs = 50000# 10000
num_freqs = 5#  5# 0 # 0 means no positional encoding

# TODO : check the BSDF IDs, can we do something with that? otherwise ask Stavros
# TODO : check whether there is any autoexposure anywhere. Render 2 envmaps with a factor multiplier and verify

# TODO : make a scene with glossy bunny on diffuse ground and plug envmap autoencoder onto the pipeline


numSGs = 5# 12#   12#  25#      50#

netwidth = 256#  128#  64# 400#      250#      64#
batch_size = 50# 60#  5# 5# 24

num_layers = 3# 4# 3#  4#    1#     5# 3#
learning_rate = 0.002#   0.0001#   0.005#   0.01# 0.5#
logloss = True# False#
L2loss = False# True#

imsize = 100#  400# 256#
exponentiate = False#

withnormals = True#  False
withlight = True#  False#

experiment_name = 'normalizeddir-'

experiment_name = experiment_name +'-batchsize-' +str(batch_size) +'-layers-' +str(num_layers) +'-width-' +str(netwidth) \
                  +'-numSG-'+str(numSGs) +'-lr-'+str(learning_rate) +'-freq-' + str(num_freqs)\
                  +'-log-'+str(int(logloss)) +'-L2-'+str(int(L2loss))+'-withnormals-' + str(int(withnormals))
# Other parameters (hardcoded)
num_workers = 0
spp = 32 # 32#
hybridanis = False#
anisowarp = True#   False
sharpness_thresh = 5 #for the anisotropic NDF warp
cos_sharpness = 2.13
cos_amplitude = 1.17
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
if not os.path.exists(outputpath):
    os.makedirs(outputpath)
# Load the scene XMLs
Thread.thread().file_resolver().append(scenepath)
scene_gt = load_file(scenepath+'/'+scenename_gt)
# scene_train = load_file(scenepath+'/'+scenename_train, spp=spp)

start = time.time()
print("Start rendering")
for i in range(20):
    scene_gt.integrator().render(scene_gt, scene_gt.sensors()[0])
end = time.time()
print("Rendering time:  ", (end - start)/20)



scene_gt.integrator().render(scene_gt, scene_gt.sensors()[0])
scene_gt.sensors()[0].film().set_destination_file(scenepath+'/aovs.exr')
scene_gt.sensors()[0].film().develop()


components = scene_gt.sensors()[0].film().bitmap(raw=False).split()
for i in range(len(components)):
    if 'position' in components[i][0]:
        buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
        pos3d = buffer.data_np()
        print('position: ', pos3d.min(), pos3d.mean(), pos3d.max())
        plt.imshow(pos3d)
        plt.show()
    elif 'normals' in components[i][0]:
        buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
        normals = buffer.data_np()
        print('normals: ', normals.min(), normals.mean(), normals.max())
        plt.imshow(normals)
        plt.show()
    elif 'diffuse' in components[i][0]:
        buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
        bsdf_val = buffer.data_np()
        print('diffuse: ', bsdf_val.min(), bsdf_val.mean(), bsdf_val.max())
        plt.imshow( (bsdf_val / np.max(bsdf_val) )**(1.0/2.2))
        plt.show()
    elif 'specular' in components[i][0]:
        buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
        bsdf_val = buffer.data_np()
        print('specular: ', bsdf_val.min(), bsdf_val.mean(), bsdf_val.max())
        plt.imshow( (bsdf_val / np.max(bsdf_val) )**(1.0/2.2))
        plt.show()
    elif 'gt' in components[i][0]:
        buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
        gtcpu = buffer.data_np()
        print('gtcpu: ', gtcpu.min(), gtcpu.mean(), gtcpu.max())
        plt.imshow(gtcpu)
        plt.show()
    elif 'alpha' in components[i][0]:
        buffer = components[i][1].convert(Bitmap.PixelFormat.Y, Struct.Type.Float32, srgb_gamma=False)
        alpha = buffer.data_np()
        print('alpha: ', alpha.min(), alpha.mean(), alpha.max())
        plt.imshow(alpha / np.max(alpha) )
        plt.show()





# # Setup NN, optimizer, tensorboard etc. ===================================================================================
# inputsize = 3 + 3*int(withlight) + 3*int(withnormals)
# print(inputsize)
# net = nets.DirectionalNet(inputsize, numSGs, netwidth, num_layers, num_freqs).to(device=gpudevice)
#
# sg_centers = sgr.initialiseSGcenters(numSGs).to(device=gpudevice)
# optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
# writer = SummaryWriter(outputpath +'/runs/' + experiment_name)
#
#
# iteration = 0
# for epoch in range(max_epochs):
#     optimizer.zero_grad()
#     epoch_loss = 0.0
#     groundtruth, mask, pos3d, normals, bsdf_val, lightdir = [],[],[],[],[],[]
#
#     for b in range(batch_size):
#         iteration += 1
#         # Generate light direction and ground truth
#         randomsensor = sgr.generate_random_sensor(spp, imsize, imsize)
#         randomdir = np.float32(2.0 * np.random.rand(3) - 1.0)
#         randomdir[1] = np.abs(randomdir[1]) * (-1.0)
#         randomnorm = np.sqrt(randomdir[0]*randomdir[0] + randomdir[1]*randomdir[1] + randomdir[2]*randomdir[2])
#         randomdir = randomdir / randomnorm
#         print('Light direction: ', randomdir)
#
#         if iteration % 10 == 0:
#             with torch.no_grad():
#                 sgr.write_intermediate_results(iteration, writer, scene_train, scene_gt, randomsensor, net, randomdir,
#                                                withlight, numSGs, sg_centers, groundtruthshape, withnormals, gpudevice,
#                                                exponentiate)
#
#         m, p, n, b = sgr.get_buffers(scene_train, randomsensor)
#         # print(m.shape, p.shape, n.shape, b.shape)
#         mask.append(m)
#         pos3d.append(p)
#         normals.append(n)
#         bsdf_val.append(b)
#         lightdir.append(torch.from_numpy(-randomdir).unsqueeze(0).expand(p.shape[0],-1).to(device=gpudevice))
#
#         scene_gt = load_file(scenepath + '/' + scenename_gt, spp=spp, lx=randomdir[0], ly=randomdir[1], lz=randomdir[2])
#         scene_gt.integrator().render(scene_gt, randomsensor)
#         gtcpu = torch.from_numpy(np.array(
#             randomsensor.film().bitmap(raw=True).convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)))
#         groundtruthshape = gtcpu.shape
#         groundtruth.append(gtcpu.view(1, groundtruthshape[0] * groundtruthshape[1], 3).squeeze()[m, :].to(device=gpudevice))
#
#     pos3d = torch.cat(pos3d, dim=0)
#     normals = torch.cat(normals, dim=0)
#     bsdf_val = torch.cat(bsdf_val, dim=0)
#     lightdir = torch.cat(lightdir, dim=0)
#     if withlight:
#         pos3d = torch.cat((lightdir, pos3d), dim=1)
#     if withnormals:
#         pos3d = torch.cat((pos3d, normals), dim=1)
#     net_out = net(pos3d)
#
#     sglights = sgr.deparametrize_torch(net_out.unsqueeze(2).view(-1,numSGs,6), sg_centers, exponentiate)
#
#
#
#     # Separate diffuse from glossy BSDFs
#
#     pred_render = sgr.render_SGs(numSGs, sglights, glossymask, normals, gpudevice)
#
#     # pred_render = bsdf_val * irr
#     groundtruth = torch.cat(groundtruth, dim=0)
#     loss = sgr.my_loss(pred_render, groundtruth, tonemap=False, clip=False, log=logloss, L2=L2loss)
#     epoch_loss = loss.item()
#     loss.backward()
#     optimizer.step()
#     writer.add_scalar('training loss', epoch_loss, epoch)
#     print('loss  ', epoch_loss, 'for epoch  ', epoch)
#
#
#
#     if epoch % 50 == 0:
#         with torch.no_grad():
#             randomdir = np.array([0, -1, 0]).astype(dtype=np.float32)
#             scene_gt = load_file(scenepath + '/' + scenename_gt, spp=spp, lx=randomdir[0], ly=randomdir[1],
#                                  lz=randomdir[2])
#             sgr.write_intermediate_results(iteration, writer, scene_train, scene_gt, scene_gt.sensors()[0], net,
#                                            randomdir,
#                                            withlight, numSGs, sg_centers, groundtruthshape, withnormals, gpudevice,
#                                            exponentiate, tensorboard_tag='Canonic view top light')
#             randomdir = np.array([-0.9949874, -0.1, 0]).astype(dtype=np.float32)
#             scene_gt = load_file(scenepath + '/' + scenename_gt, spp=spp, lx=randomdir[0], ly=randomdir[1],
#                                  lz=randomdir[2])
#             sgr.write_intermediate_results(iteration, writer, scene_train, scene_gt, scene_gt.sensors()[0], net,
#                                            randomdir,
#                                            withlight, numSGs, sg_centers, groundtruthshape, withnormals, gpudevice,
#                                            exponentiate, tensorboard_tag='Canonic view light X')
#             randomdir = np.array([0, -0.1, -0.9949874]).astype(dtype=np.float32)
#             scene_gt = load_file(scenepath + '/' + scenename_gt, spp=spp, lx=randomdir[0], ly=randomdir[1],
#                                  lz=randomdir[2])
#             sgr.write_intermediate_results(iteration, writer, scene_train, scene_gt, scene_gt.sensors()[0], net,
#                                            randomdir,
#                                            withlight, numSGs, sg_centers, groundtruthshape, withnormals, gpudevice,
#                                            exponentiate, tensorboard_tag='Canonic view light Y')
#
# print('Finished Training')


