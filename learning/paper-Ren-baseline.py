import os
import numpy as np
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader
import sys
import os
import cv2
import nets
from torchvision.utils import save_image

import renderhelpers as sgr
import matplotlib.pyplot as plt

import einops
import siren_pytorch as siren

torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True

# Input paths and parameters ===========================================================================================
# dropbox_path = 'C:/Users/grainer/Dropbox/'
dropbox_path = 'F:/gilles/Dropbox/'

train_path = dropbox_path + 'INRIA/projects/nprt/data/scenes/atelier/train'
test_path = dropbox_path + 'INRIA/projects/nprt/data/scenes/atelier/test'

aabb = [[-146.994873, -59.336391, -170.382034], [146.993851, 85.836891, 113.362228]] # for atelier

envmap_trainpath = dropbox_path + 'INRIA/projects/nprt/data/scenes/indoorenvs/train'
envmap_testpath = dropbox_path + 'INRIA/projects/nprt/data/scenes/indoorenvs/test'

pretrain_enc_path = None # 'C:/Users/grainer.AD/Dropbox/INRIA/projects/nprt/res/atelier/pretrained.pth' #

outputpath = dropbox_path + 'INRIA/projects/nprt/res/atelier/viz'

# TODO: pre-train the encoder? take a trained encoder from some other network? (load the other network and go net.sgencoder = net2.encoder and freeze it)
experiment_name = 'Ren-linearout2-' #
# Training parameters ==================================================================================================
insize = 20

latentdim = 64 # 128 #8# 72# 72#    128# 72#  350# #128#   512#

netwidth = 256# 512#  64# 400#      250#      64#
batch_size = 3# 10#0# 60#  5# 5# 24
num_layers = 2#  4  # 4# 3#  4#    1#     5# 3#

learning_rate = 0.0001#  0.0001  # 0.0001#   0.005#   0.01# 0.5#
logloss = True# True  # False#
L2loss = False  # True#
max_epochs = 500000  # 10000
cnndim = 2048  # 96# 512# 2048#  4096#
withnormals = True # True  # False
encode_normals = False # False # True

tensorboard_writes = 250
experiment_name = experiment_name + '-bats-' + str(batch_size) + '-lat-' + str(latentdim) + '-layer-' + str(
                    num_layers) + '-width-' + str(netwidth) + '-lr-' + str(learning_rate)
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
if not os.path.exists(outputpath):
    os.makedirs(outputpath)
boundingboxmin = torch.tensor(aabb[0]).unsqueeze(0).unsqueeze(0)
boundingboxsize = torch.tensor(aabb[1]).unsqueeze(0).unsqueeze(0) - boundingboxmin

class BufferDataset(Dataset):
    def __init__(self, renderpath, envpath):
        self.alpha = []
        self.diffuse = []
        self.specular = []
        self.envmap = []
        self.gt = []
        self.normals = []
        self.position = []
        self.viewdir = []

        self.envfiles = []
        for r, d, f in os.walk(envpath):  # r=root, d=directories, f = files
            for file in f:
                if '.exr' in file:
                    self.envfiles.append(file)
                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH),
                                      cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(im)
                    self.envmap.append(im)

        c = 0
        for r, d, f in os.walk(renderpath):  # r=root, d=directories, f = files
            for file in f:
                if '.exr' in file:
                    if c % 50 == 0:
                        print('Read ', c, ' EXR buffers.')

                    filename = os.path.splitext(os.path.basename(file))[0]
                    fname = filename.split('.')
                    if int(fname[0]) == c:
                        c = c+1

                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(im)

                    if fname[2] == 'posW':
                        im = (im - boundingboxmin.expand(im.shape[0], im.shape[1], -1)) / boundingboxsize.expand(im.shape[0], im.shape[1], -1)
                        im = (im-0.5) * 2
                        self.position.append(im)
                    if fname[2] == 'alpha':
                        self.alpha.append(im)
                    if fname[2] == 'diffuseOpacity':
                        self.diffuse.append(im / np.pi)
                    if fname[2] == 'output':
                        self.gt.append(im)
                    if fname[2] == 'normW':
                        self.normals.append(im)
                    if fname[2] == 'specRough':
                        self.specular.append(im)
                    if fname[2] == 'viewW':
                        self.viewdir.append(im)

    def __len__(self):
        return len(self.gt)

    def __getitem__(self, idx):
        env = torch.log(1.0 + self.envmap[idx % len(self.envmap)])
        # env = self.envmap[idx % len(self.envmap)]

        pos = self.position[idx]
        # alpha = self.alpha[idx]
        alpha = torch.sqrt(self.alpha[idx])

        diff = self.diffuse[idx]
        gt = self.gt[idx]
        normals = self.normals[idx]
        specular = self.specular[idx]
        viewdir = self.viewdir[idx]
        specular = torch.clamp(specular, min = 0.0, max = 10000.0)
        return alpha, pos, normals, viewdir, diff, specular, gt, env

print('Load training data.')
train_dataset = BufferDataset(train_path, envmap_trainpath)
print('Load validation data.')
val_dataset = BufferDataset(test_path, envmap_testpath)
print('Training data loaded.')
train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True) # , num_workers=10) # , pin_memory=True, drop_last=True)
val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=True) # , pin_memory=True, drop_last=True)

class RenNet(nn.Module):
    def __init__(self, inpsize):
        super(RenNet, self).__init__()
        layers = [nn.Linear(inpsize, 20), nn.Tanh(), nn.Linear(20, 10), nn.Tanh(), nn.Linear(10, 3)]
        self.module_list = nn.ModuleList(layers)
    def forward(self, x):
        for f in self.module_list:
            x = f(x)
        return x

class Nets(nn.Module):
    def __init__(self, numberofnets):
        super(Nets, self).__init__()
        posnets = []
        for kd in range(numberofnets):
            net = RenNet(latentdim + 16)
            posnets.append(net)
        self.nets = nn.ModuleList(posnets)

    def forward(self, env, pos, netidx):
        x = torch.cat((env, pos), dim=1)
        return self.nets[netidx](x)

envmapnet = nets.EnvmapSGencoder(4096, latentdim).to(device=gpudevice)
spatialnets = Nets(1000).to(device=gpudevice)
print(spatialnets)
print("SPATIALNET: number of parameters ", sum(p.numel() for p in spatialnets.parameters() if p.requires_grad))
print("envmapnet: number of parameters ", sum(p.numel() for p in envmapnet.parameters() if p.requires_grad))

optimizer = torch.optim.Adam(list(spatialnets.parameters()) + list(envmapnet.parameters()), lr=learning_rate)
writer = SummaryWriter(outputpath + '/runs/' + experiment_name)  # logloss-l1-adam-5e-3-testFaceNormals')
test_example = iter(val_dataloader)
iteration = 0

def render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap, gpudevice, envmapnetwork, spatialnetworks, timings=False):
    # flatten everything
    mask = (torch.sqrt(torch.sum(normals**2, dim=-1, keepdim=True)).expand(-1,-1,3) > 0.0)  # to make sure, remove background
    gtcpu[~mask] = 0.0
    mask = mask[:,:,0].view(-1)

    pos3d = pos3d.view(-1, 3)
    normals = normals.view(-1, 3)
    diffuse_bsdf_val = diffuse_bsdf_val.view(-1, 3)
    specular_bsdf_val = specular_bsdf_val.view(-1, 3)
    alpha = alpha[:, :, 0].view(-1)
    wi = wi.view(-1, 3)
    pos3d = pos3d[mask, :]
    pred = torch.zeros(pos3d.shape).to(device=gpudevice)
    normals = normals[mask, :]
    diffuse_bsdf_val = diffuse_bsdf_val[mask, :]
    specular_bsdf_val = specular_bsdf_val[mask, :]
    wi = wi[mask, :]
    alpha = alpha[mask]

    start = torch.cuda.Event(enable_timing=True)
    end = torch.cuda.Event(enable_timing=True)
    if timings:
        start.record()

    envinp = envmap.unsqueeze(0).to(device=gpudevice)
    latentenv = envmapnet(envinp)
    # print(latentenv.shape)

    for xd in range(10):
        for yd in range(10):
            for zd in range(10):
                xdmin = 2.0 * xd / 10.0 - 1.0
                ydmin = 2.0 * yd / 10.0 - 1.0
                zdmin = 2.0 * zd / 10.0 - 1.0

                pxmask = (pos3d[:,0]>=xdmin) * (pos3d[:,0]<xdmin+0.2) * (pos3d[:,1]>=ydmin) * (pos3d[:,1]<ydmin+0.2) * (pos3d[:,2]>=zdmin) * (pos3d[:,2]<zdmin+0.2)

                if torch.sum(pxmask)>1:
                    # TODO : scale the position within the small voxel
                    scaledpos = pos3d[pxmask, :]
                    scaledpos[:,0] = 1.6 * (scaledpos[:,0] - xdmin) / 0.2 - 0.8
                    scaledpos[:,1] = 1.6 * (scaledpos[:,1] - ydmin) / 0.2 - 0.8
                    scaledpos[:,2] = 1.6 * (scaledpos[:,2] - zdmin) / 0.2 - 0.8

                    netin = torch.cat((pos3d[pxmask, :], scaledpos), dim=1)
                    netin = torch.cat((netin, alpha[pxmask].unsqueeze(1), specular_bsdf_val[pxmask, :], diffuse_bsdf_val[pxmask, :], wi[pxmask, :]), dim=1)
                    netin = netin.to(device=gpudevice)

                    netidx = 100 * xd + 10 * yd + zd
                    lightin = latentenv.expand(netin.shape[0], -1)
                    out = torch.nn.functional.relu(spatialnets(lightin, netin, netidx))
                    pred[pxmask,:] += out
    if timings:
        end.record()
        torch.cuda.synchronize()
        print("Time to shade:  ", start.elapsed_time(end))
    return pred, gtcpu.view(-1, 3)[mask, :], pred, pred


for epoch in range(max_epochs):
    epoch_loss = 0.0

    for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in train_dataloader:
        iteration = iteration + 1
        optimizer.zero_grad()
        batch_loss = 0.0
        groundtruth, pred_render = [], []

        for b in range(alpha_batch.shape[0]):
            pred, gtcpu2, _, _ = render_from_buffers(alpha_batch[b], pos3d_batch[b], normals_batch[b], wi_batch[b],
                                                 diffuse_bsdf_val_batch[b], specular_bsdf_val_batch[b], gtcpu_batch[b],
                                                envmap_batch[b].permute(2,0,1),
                                                gpudevice, envmapnet, spatialnets, timings=False)

            groundtruth.append(gtcpu2)
            pred_render.append(pred)

        groundtruth = torch.cat(groundtruth, dim=0).to(gpudevice)
        pred_render = torch.cat(pred_render, dim=0)
        loss = sgr.my_loss(pred_render, groundtruth, tonemap=False, clip=False, log=logloss, L2=L2loss)

        batch_loss += loss.item()
        loss.backward()
        optimizer.step()
        writer.add_scalar('training loss', batch_loss, iteration)
        epoch_loss = epoch_loss + batch_loss / len(train_dataloader)

        with torch.no_grad():
            if iteration % tensorboard_writes == 1:
                try:
                    alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch = test_example.next()
                except StopIteration:
                    test_example = iter(val_dataloader)
                    alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch = test_example.next()

                alpha = alpha_batch[0]  # .to(gpudevice)
                pos3d = pos3d_batch[0]  # .to(gpudevice)
                normals = normals_batch[0]  # .to(gpudevice)
                wi = wi_batch[0]  # .to(gpudevice)
                diffuse_bsdf_val = diffuse_bsdf_val_batch[0]  # .to(gpudevice)
                specular_bsdf_val = specular_bsdf_val_batch[0]  # .to(gpudevice)
                gtcpu = gtcpu_batch[0]  # .to(gpudevice)
                envmap = envmap_batch[0].permute(2, 0, 1)  # .to(gpudevice)

                pred, gtcpu2, diffim, specim = render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap,
                        gpudevice, envmapnet, spatialnets, timings=True)

                alpha = alpha[:, :, 0].view(-1)
                mask = (torch.sqrt(torch.sum(normals ** 2, dim=-1, keepdim=False)) > 0.0).view(-1)
                pr = torch.zeros(gtcpu.size())
                pr = pr.view(-1, 3)
                pr[mask, :] = pred.cpu()
                pr = pr.view(gtcpu.shape)

                gtcpu = torch.zeros(normals.size())
                gtcpu = gtcpu.view(-1, 3)
                gtcpu[mask, :] = gtcpu2.cpu()
                gtcpu = gtcpu.view(normals.shape)

                diff = torch.zeros(normals.size())
                diff = diff.view(-1, 3)
                diff[mask, :] = diffim.cpu()
                diff = diff.view(normals.shape)
                spec = torch.zeros(normals.size())
                spec = spec.view(-1, 3)
                spec[mask, :] = specim.cpu()
                spec = spec.view(normals.shape)

                static_spec = torch.zeros(gtcpu.shape)
                static_spec = static_spec + specular_bsdf_val.cpu()
                static_spec = static_spec.permute(2, 0, 1)
                static_diff = torch.zeros(gtcpu.shape)
                static_diff = static_diff + diffuse_bsdf_val.cpu()
                static_diff = static_diff.permute(2, 0, 1)

                gtcpu = gtcpu.permute(2, 0, 1)
                pr = pr.permute(2, 0, 1)
                diff = diff.permute(2, 0, 1)
                spec = spec.permute(2, 0, 1)
                difference = torch.abs(pr - gtcpu)

                # print(gtcpu.shape, pr.shape, diff.shape)
                print('maxs and mins  ', gtcpu.max(), pr.max(), diff.max(), gtcpu.min(), pr.min(), diff.min())
                print('diffuse minmax : ', diff.min(), diff.mean(), diff.max())
                print('specular minmax : ', spec.min(), spec.mean(), spec.max())
                img_grid = torchvision.utils.make_grid([static_diff / static_diff.mean() * pr.mean(), static_spec / static_diff.mean() * pr.mean(), diff, spec, pr, gtcpu, difference])
                img_grid = img_grid / gtcpu.max()
                img_grid = img_grid ** (1.0 / 2.2)
                img_grid = torch.clip(1.2 * img_grid, 0.0, 1.0)

                writer.add_image('static vs predictions vs. GT', img_grid, global_step=iteration)

    writer.add_scalar('Epoch loss (training)', epoch_loss, epoch)

    with torch.no_grad():  # Loop over the test data:
        val_loss = 0.0
        for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in val_dataloader:
            pred_render, groundtruth, sharpprior,_ = render_from_buffers(alpha_batch[0], pos3d_batch[0], normals_batch[0], wi_batch[0],
                                                 diffuse_bsdf_val_batch[0], specular_bsdf_val_batch[0], gtcpu_batch[0],
                                                 envmap_batch[0].permute(2, 0, 1),
                                                 gpudevice, envmapnet, spatialnets,
                                                 timings=False)
            loss = sgr.my_loss(pred_render, groundtruth.to(gpudevice), tonemap=False, clip=False, log=logloss, L2=L2loss)
            val_loss += loss.item() / len(val_dataloader)

        writer.add_scalar('Epoch loss (validation)', val_loss, epoch)

        # TODO : save network and run it on the high-res testing images
        torch.save(envmapnet.state_dict(), outputpath + "/"+ experiment_name + "-envmapnet.pth")
        torch.save(spatialnets.state_dict(), outputpath + "/"+ experiment_name + "-spatialnet.pth")

print('Finished Training')